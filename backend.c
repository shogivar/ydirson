/**************************************************************/
/* ShogiVar business logic hand-converted to C by H.G. Muller */
/**************************************************************/

#include "config.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "common.h"
#include "backend.h"
#include "frontend.h"

#define NOCOLOR 0

#ifdef MSVC
#   define MOVE(N, OBJ, X, Y) OBJ.Move(X, Y)
#   define DRAG(OBJ, X)       OBJ.Drag(X)
#   define CR "\r"
#   define LF
#   define DATADIR "./Data"
#   define CONFIGFILE "%s/Shogi.cfg", Direct
#else
#   define MOVE(N, OBJ, X, Y) MoveObject(N, &OBJ, X, Y)
#   define DRAG(OBJ, X)       OBJ.dragged = X
#   define CR
#   define LF "\n"
#   define CONFIGFILE "./.shogivarrc"
#endif

// integer globals;

INT FirstInitFile, FirstInitRank, LionAttack, FirstSeeMove, Reload, PawnMate, AA, Suggest, EndMove, Eval, CheckLooked, NoLionCapture, Checked, OriginalPiece, RightClick, OriginalFile, OriginalRank, FirstFile, FirstRank, Influence, CheckTest;
INT CompLionTest, OldNewFile, OldNewRank, ChuLionTest, ProtectLion, XXX, YYY, BlackKingX, BlackKingY, WhiteKingX, WhiteKingY, Changed, FirstTime, MakeMove, CompFile, CompRank, CompMove, LegalMoves, Evaluate, DropTest, OldSeeMove, BestMove;
INT Replaying, OldInitRank, OldInitFile, EmperorTest, WhiteEmperor, BlackEmperor, Boardsize, Notate, Graphnum, Pixels, XCorner, SeeFile, SeeRank, PromGraf, Reverse, CapturedPiece;
INT Forwards, Tabbing, PieceSizes, ClickPiece, OldLast, OldRank, OldFile, YCorner, Totpiece, PieceNum, J, I, Demon, XStart, LookMate, GameOver;
INT Tilde, Blink, Blocked, MicroCap, BoardSizeX, BoardSizeY, Setup, Reduce, WhiteKing, BlackKing, WhitePrince, BlackPrince, LionPro;
INT ForceProm, AutoPromote, Notice, Backwards, TurnCount, Handicap, Display, NewButton, PromDotY, NewIndex, Rank, File, TotGraph, InitRank, InitFile, Prom;
INT Testing123, Timing, NewGraf, HandGame, Loading, NoPro, GameNo, MoveCount, NewRank, NewFile, Weaker, TeachVer, Teach, CaptPiece;
INT Other, LionTest, NewGame, GeneralInfo, LastWhite, LastBlack, Hook, Range, N, R, F, Area, M, Drop, Dropped, C, D, Capture, RealLion, LionPiece;
INT EndTurn, MovePiece, Selection, WhiteLion, BlackLion, Mate, Taken, P, MoveTest, MoveData, FileInc, RankInc, Last, SeeMove, LionHawkVer;
INT AllBack, Ligui, XA, LastPieceX, LastPieceY, CCC, ShowLast, BlackInfluence, WhiteInfluence, WhiteEmpX, WhiteEmpY, BlackEmpX, BlackEmpY, Level, RealLevel, Depth, TestDepth, FirstLegal;
INT YStart;

// string globals;

STRING OldComputer, Threat = "Off", FirstScore, SecondScore;
STRING OldThreat, ScoreFormat, PromPieceSTR, OldChoice, GameName, LionNameSTR, CMoveSTR, NxtSTR, Turn = "Black", Turn2, Choice;
STRING Computer = "White", SaveTitleSTR, NewTurn, ExtraPiece, ElapsedSTR, SpecPowerSTR, LoadedSTR, SavedSTR, Datafile, Direct = DATADIR, Cap, Boardbmp, Grade = "Weak";

// float globals;

SINGLE Count;
SINGLE NewX, NewY;
#define COUNT ((int)Count)

// long globals;

LONG BestTally[26];
LONG BestScore, WhiteTally, BlackTally;
LONG FinalTally[26];

static FILE *f2; // global, because LoadGame opens it, and SetPieces then reads from it.
static STRING StringTmp;

FORM Board;
FORM Start; // FIXME: what is this?
FORM RulesHelp;
ADDPIECES AddPieces; // created by Load ?
// FORM TaiPieces;

char *Mid(char *s, int start, int len)
{
  static char buf[80];
  *buf = 0;
  if(start < 1 || start > strlen(s) || len > 79) return buf;
  strncpy(buf, s + start - 1, 79); buf[len] = 0;
  return buf;
}
char *TrimSTR (char *s)
{
  char *p; static STRING buf;
  strncpy(buf, s, 79);   while(*s == ' ') s++;
  p = s + strlen(s) - 1; while(*p == ' ') p--; p[1] = '\0';
  return buf;
}
char *NumSTR (int n) { static char buf[20]; snprintf(buf, 20, "% d", n); return buf; }
char *CharSTR (int n) { static char buf[2]; buf[0] = n; return buf; }
INT Int(double x) { return (INT) x; }
INT Sgn(INT n) { if(n < 0) return -1; else return n > 0; }
char Asc(STRING s) { return *s; }
INT Rnd() { return rand(); }
void Randomize() { srand(StartTime()); }

struct {
  int Height, Width;
  int TwipsPerPixelY;
} screen;

INT Err; // FIXME: this is apparently a variable set by the system, like errno in C
INT Dropping; STRING Game; // FIXME: added to prevent compile error, read-only variables!

// structures;

typedef struct { 
    INT StartFile;
    INT StartRank;
    INT EndFile;
    INT EndRank;
    INT StartPiece;
    INT EndPiece;
} LegalList;
typedef struct { 
    STRING Caption;
    INT IDStart;
    INT IDEnd;
    INT PosStart;
    INT PosEnd;
} MoveRecord;
typedef struct { 
    INT number;
    INT Positions[13];
    INT PieceNum[13];
} CaptiveRecord;
typedef struct { 
    INT File;
    INT Rank;
    INT Piece;
} Layout;
typedef struct { 
    INT File;
    INT Rank;
} Empty;
typedef struct { 
    INT WhiteNum;
    INT BlackNum;
    INT WhiteValue;
    INT BlackValue;
    Layout Info[513];
} Map;

// Tables;

#define SZX 26
#define SZY 26
#define PIXELS 61
#define CAPTURE 30

LegalList MoveList[27]; 
Empty Clearing[201];
STRING ShortScore[4001];
STRING BlackDrop[3];
STRING WhiteDrop[3];
LegalList CompLegal[2001];
Layout ExtraCapture[2001]; 
Layout ECapture[22];
Layout LionVictim;
Piece Pieces[1000]; // TODO how many really?
INT LowBlack[SZX][SZY];
INT LowWhite[SZX][SZY];
INT PieceMask[7][PIXELS];
INT Squares[SZX][SZY];
INT Comp[SZX][SZY];
INT CompHeld[2*CAPTURE];
INT Legal[SZX][SZY];
INT OldLegal[SZX][SZY];
INT Grafix[SZX][SZY];
INT InHand[2*CAPTURE];
INT TempHand[2*CAPTURE];
INT CapRef[2*CAPTURE];
INT AreaOK[SZX][SZY];
INT Camps[SZX][SZY];
INT Attacker[SZX][SZY];
INT OldAttack[SZX][SZY];
Map BanMap[SZX][SZY]; 
Map BackMap[SZX][SZY];
MoveRecord Score[4001]; 
CaptiveRecord Captures[4001];
LONG KingTally[2001];
LONG OldKingTally[27];

INT OldHand[2*CAPTURE];
INT TestBoard[SZX][SZY];

// Prototypes

void AreaMove2();
void CheckBurn();
void CheckAdd();
void CompPromote();
void CompPromote2();
void ConfigLoad2();
void ConsiderMate();
void ConsiderMove();
void EmperorInfluence();
void FindEmperorMove();
void FindInfluence();
void FireDemon();
void FormDrop();
void GetSquare();
void GetSquare2();
void HeldDrop();
void HeldValid();
void KingSafety();
void Lion2();
void LionBurn();
void LionBurn2();
void LookAhead();
void LookCheck2();
void LookComp2();
void LookMove();
void Main();
void MakeDrop();
void MakeMap();
void MicroAdd();
void MicroDrop();
void MoveFormDrop();
void NextTurn();
void NextTurn2();
void PawnMates();
void PicDrop();
void Promessage();
void Promote();
void RemovePiece();
void ReorderMoves();
void ResetHand();
void ResetLegal();
void RestoreGrafix();
void RestoreMap();
void SetClock();
void SetDifficulty();
void SetGrade();
void SetGrafix();
void SetKings();
void SetPieces();
void SetThreat();
void ShowMove();
void SingleStep();
void SquareReplace();
void StartUp();
void SuggestMove();
void TakeBack();
void Territory();
void TestAhead();
void TestDrop();
void TestOther();
void TestStrength();
void Tetrarchs();
void TooSmall();
void UnPromote();
void Validate();
void WaDrop();
void TwoKings();


void ActingPieces ()
{
  int AB, CD;
  if(InitFile > 0) {
    BlackInfluence = 0; WhiteInfluence = 0;
    OriginalPiece = Squares[InitFile][InitRank];
    OriginalFile = InitFile; OriginalRank = InitRank;
    Influence = 1;
    for(AB = 1; AB <= BoardSizeY; AB++) {
	for(CD = 1; CD <= BoardSizeX; CD++) {
	    if(Squares[CD][AB] != 0) {
		if(OriginalPiece != 0 && Sgn(Squares[CD][AB]) != Sgn(OriginalPiece)) {
		    Squares[OriginalFile][OriginalRank] = OriginalPiece;
		} else {
		    if(CheckTest != 1) Squares[OriginalFile][OriginalRank] = 0;
		}
		InitFile = CD; InitRank = AB;
		if(CheckTest != 1 || (Sgn(OriginalPiece) != Sgn(Squares[CD][AB])) ) Validate();
	    }
	} // Next CD;
    } // Next AB;
    if(CheckTest != 1 && (!strcmp(Choice, "Maka") || !strcmp(Choice, "Tai")) ) {
	if(OriginalPiece == 0) {
	    if(WhiteEmperor == 1 ) { InitFile = WhiteEmpX; InitRank = WhiteEmpY; EmperorInfluence(); }
	    if(BlackEmperor == 1 ) { InitFile = BlackEmpX; InitRank = BlackEmpY; EmperorInfluence(); }
	} else {
	    if(OriginalPiece < 0 && WhiteEmperor == 1 && strcmp(Pieces[abs(OriginalPiece)].Name, "Emperor") ) {
		InitFile = WhiteEmpX; InitRank = WhiteEmpY; EmperorInfluence();
	    }
	    if(OriginalPiece > 0 && WhiteEmperor == 1 && BlackEmperor == 0 && BlackInfluence == 0) {
		InitFile = WhiteEmpX; InitRank = WhiteEmpY; EmperorInfluence();
	    }
	    if(OriginalPiece > 0 && BlackEmperor == 1 && strcmp(Pieces[abs(OriginalPiece)].Name, "Emperor") ) {
		InitFile = BlackEmpX; InitRank = BlackEmpY; EmperorInfluence();
	    }
	    if(OriginalPiece < 0 && BlackEmperor == 1 && WhiteEmperor == 0 && WhiteInfluence == 0) {
		InitFile = BlackEmpX; InitRank = BlackEmpY; EmperorInfluence();
	    }
	}
    }
    Influence = 0;
    Squares[OriginalFile][OriginalRank] = OriginalPiece;
  }
}

void AddEmperorAttack ()
{
  LegalMoves = LegalMoves + 1;
  if(!strcmp(Turn, "White") ) {
    CompLegal[LegalMoves].StartFile = WhiteEmpX;
    CompLegal[LegalMoves].StartRank = WhiteEmpY;
    CompLegal[LegalMoves].EndFile = BlackEmpX;
    CompLegal[LegalMoves].EndRank = BlackEmpY;
    CompLegal[LegalMoves].StartPiece = Squares[WhiteEmpX][WhiteEmpY];
    CompLegal[LegalMoves].EndPiece = Squares[WhiteEmpX][WhiteEmpY];
  } else {
    CompLegal[LegalMoves].StartFile = BlackEmpX;
    CompLegal[LegalMoves].StartRank = BlackEmpY;
    CompLegal[LegalMoves].EndFile = WhiteEmpX;
    CompLegal[LegalMoves].EndRank = WhiteEmpY;
    CompLegal[LegalMoves].StartPiece = Squares[BlackEmpX][BlackEmpY];
    CompLegal[LegalMoves].EndPiece = Squares[BlackEmpX][BlackEmpY];
  }
}

void AddEmperorMove ()
{
LegalMoves = LegalMoves + 1;
CompLegal[LegalMoves].StartFile = InitFile;
CompLegal[LegalMoves].StartRank = InitRank;
CompLegal[LegalMoves].EndFile = SeeFile;
CompLegal[LegalMoves].EndRank = SeeRank;
CompLegal[LegalMoves].StartPiece = Squares[InitFile][InitRank];
CompLegal[LegalMoves].EndPiece = Squares[InitFile][InitRank];
}

void AddHand ()
{
  int X;
  for(X = 1; X <= Capture; X++) {
    if(CapRef[X] == CaptPiece) {
	InHand[X] = InHand[X] + 1;
	Board.HandPic[X].Visible = True;
	if(InHand[X] > 1) sprintf(Board.Held[X].Caption, "%d", InHand[X]);
	if(!strcmp(Choice, "Micro")) {
	    MicroCap = X;
	    MicroAdd();
	}
    } else {
	if(CapRef[X] == 0 - CaptPiece) {
	    InHand[Capture + X] = InHand[Capture + X] + 1;
	    Board.HandPic[Capture + X].Visible = True;
	    if(InHand[Capture + X] > 1) sprintf(Board.Held[Capture + X].Caption, "%d", InHand[Capture + X]);
	    if(!strcmp(Choice, "Micro") ) {
		MicroCap = Capture + X;
		MicroAdd();
	    }
	}
    }
  } // } // Next X;
}

void AddHand2 ()
{
  int X;
  if(MovePiece != 1 ) {
    for(X = 1; X <= Capture; X++) {
	if(CapRef[X] == Selection ) {
	    InHand[X] = InHand[X] + 1;
	    sprintf(Board.Held[X].Caption, "%d", InHand[X]);
	    if(!strcmp(Choice, "Micro") ) {
		MicroCap = X;
		MicroAdd();
	    }
	} else {
	    if(CapRef[X] == 0 - Selection ) {
		InHand[Capture + X] = InHand[Capture + X] + 1;
		sprintf(Board.Held[Capture + X].Caption, "%d", InHand[Capture + X]);
		if(!strcmp(Choice, "Micro") ) {
		    MicroCap = Capture + X;
		    MicroAdd();
		}
	    }
	}
    } // Next X;
    CheckAdd();
  }
}

void AddHand3 ()
{
  int X;
  for(X = 1; X <= Capture; X++) {
    if(CapRef[X] == CaptPiece ) {
	InHand[X] = InHand[X] + 1;
	if(!strcmp(Choice, "Micro") ) {
	    MicroCap = X;
	    if((MicroCap > 4 && MicroCap < 9) || MicroCap > 12)
                 InHand[MicroCap - 4] = InHand[MicroCap - 4] + 1;
            else InHand[MicroCap + 4] = InHand[MicroCap + 4] + 1;
	}
    } else {
	if(CapRef[X] == 0 - CaptPiece ) {
	    InHand[Capture + X] = InHand[Capture + X] + 1;
	    if(!strcmp(Choice, "Micro") ) {
		MicroCap = Capture + X;
		if((MicroCap > 4 && MicroCap < 9) || MicroCap > 12)
                     InHand[MicroCap - 4] = InHand[MicroCap - 4] + 1;
                else InHand[MicroCap + 4] = InHand[MicroCap + 4] + 1;
	    }
	}
    }
  } // Next X;
}

void AddLegalMove ()
{
  LegalMoves = LegalMoves + 1;
  CompLegal[LegalMoves].StartFile = InitFile;
  CompLegal[LegalMoves].StartRank = InitRank;
  CompLegal[LegalMoves].EndFile = SeeFile;
  CompLegal[LegalMoves].EndRank = SeeRank;
  if(InitFile == 0 ) {
    CompLegal[LegalMoves].StartPiece = CapRef[I];
    CompLegal[LegalMoves].EndPiece = CapRef[I];
  } else {
    CompLegal[LegalMoves].StartPiece = Squares[InitFile][InitRank];
    CompLegal[LegalMoves].EndPiece = Squares[InitFile][InitRank];
    CompPromote();
  }
}

void AddLionMove ()
{
  LegalMoves = LegalMoves + 1;
  CompLegal[LegalMoves].StartFile = InitFile;
  CompLegal[LegalMoves].StartRank = InitRank;
  CompLegal[LegalMoves].EndFile = XXX;
  CompLegal[LegalMoves].EndRank = YYY;
  CompLegal[LegalMoves].StartPiece = Squares[NewFile][NewRank];
  CompLegal[LegalMoves].EndPiece = Squares[NewFile][NewRank];
}

void AddSomePieces ()
{
  int X;
#if 0
  int K, i, j;
  char spaces[50];
  STRING ASTR, HelpSTR;
  for(K = PieceNum / 2; K >= 1; K--) {
    sprintf(ASTR, "%s %s %d", Pieces[K].Name, Pieces[K].sname, Pieces[K].number);
    for(i=strlen(ASTR), j=0; i<38; i++) spaces[j++] = ' '; spaces[j] = 0;
    sprintf(HelpSTR, "%s %s %s %s %d", ExtraPiece, Pieces[K].Name, Pieces[K].sname, spaces, Pieces[K].number);
     if(!strcmp(AddPieces.NewPiece.List[AddPieces.NewPiece.ListIndex], HelpSTR) ) { // FIXME: only dummy now
	Selection = Pieces[K].number;
	if(!strcmp(ExtraPiece, "White")) Selection = 0 - Selection;
    }
  } // Next K;
#else
    // [HGM] Selection set by caller by requesting selected item from listbox
    Selection = Pieces[Selection].number;
    if(!strcmp(ExtraPiece, "White")) Selection = 0 - Selection;
#endif
  AddPieces.Visible = False;
  sprintf(Board.Caption, "Adding %s %ss - [Press Right Button When Finished]", ExtraPiece, Pieces[abs(Selection)].Name);
  strcpy(Board.BlackClock.Caption, "00:00:00");
  strcpy(Board.WhiteClock.Caption, "00:00:00");
  Board.Timer1.Enabled = False;
  strcpy(Board.LastMove.Caption, "");
  if(Drop == 1 ) {
    for(X = 1; X <= Capture; X++) {
      if(!strcmp(ExtraPiece, "Black") ) {
	if(CapRef[X] == Selection ) {
	    Board.HandPic[X].Visible = True;
	    sprintf(Board.Held[X].Caption, "%d", InHand[X]);
	}
      } else {
	if(CapRef[X] == 0 - Selection ) {
	    Board.HandPic[Capture + X].Visible = True;
	    sprintf(Board.Held[Capture + X].Caption, "%d", InHand[Capture + X]);
	}
      }
    } // Next X;
  }
  UnloadAddPieces();
}

void AddTally ()
{
    INT DD, EE, FF, QQ, BlackLoss, WhiteLoss, HighBlackLoss, HighWhiteLoss;
    BestTally[1] = -999999; CompMove = 0; Influence = 3; Evaluate = 1;
    WhiteTally = 0; BlackTally = 0;
    MakeMap();
    for(DD = 1;DD <= BoardSizeY; DD++) {
	for(EE = 1; EE <= BoardSizeX; EE++) {
	    if(Squares[EE][DD] < 0) WhiteTally = WhiteTally + (Pieces[abs(Squares[EE][DD])].Value * 10);
	    if(Squares[EE][DD] > 0) BlackTally = BlackTally + (Pieces[abs(Squares[EE][DD])].Value * 10);
	    if(BanMap[EE][DD].BlackNum == 0 && BanMap[EE][DD].WhiteNum > 0 ) {
		WhiteTally = 1 + abs((BoardSizeY / 2) - DD) + WhiteTally;
		if(Squares[EE][DD] > 0 && !strcmp(Turn, "Black") ) BlackTally = BlackTally - (Pieces[abs(Squares[EE][DD])].Value * 10);
	    }
	    if(BanMap[EE][DD].WhiteNum == 0 && BanMap[EE][DD].BlackNum > 0 ) {
		BlackTally = abs((BoardSizeY / 2) - DD) + BlackTally;
		if(Squares[EE][DD] < 0 && !strcmp(Turn, "White") ) WhiteTally = WhiteTally - (Pieces[abs(Squares[EE][DD])].Value * 10);
	    }
	    if(BanMap[EE][DD].WhiteNum > 0 && BanMap[EE][DD].BlackNum > 0 ) {
		if(Squares[EE][DD] > 0 && !strcmp(Turn, "Black") ) {
		    for(QQ = 1; QQ <= Attacker[EE][DD]; QQ++) {
			if(BanMap[EE][DD].Info[QQ].Piece < 0 && (Pieces[abs(BanMap[EE][DD].Info[QQ].Piece)].Value < LowWhite[EE][DD] || LowWhite[EE][DD] == 0) )
			    LowWhite[EE][DD] = Pieces[abs(BanMap[EE][DD].Info[QQ].Piece)].Value;
		    } // Next QQ;
		    BlackLoss = ((Pieces[abs(Squares[EE][DD])].Value - LowWhite[EE][DD]) * 10);
		    if(BlackLoss > HighBlackLoss ) HighBlackLoss = BlackLoss;
		}
		if(Squares[EE][DD] < 0 && !strcmp(Turn, "White") ) {
		    for(QQ = 1; QQ <= Attacker[EE][DD]; QQ++) {
			if(BanMap[EE][DD].Info[QQ].Piece > 0 && (Pieces[abs(BanMap[EE][DD].Info[QQ].Piece)].Value < LowBlack[EE][DD] || LowBlack[EE][DD] == 0) )
			    LowBlack[EE][DD] = Pieces[abs(BanMap[EE][DD].Info[QQ].Piece)].Value;
		    } // Next QQ;
		    WhiteLoss = ((Pieces[abs(Squares[EE][DD])].Value - LowBlack[EE][DD]) * 10);
		    if(WhiteLoss > HighWhiteLoss ) HighWhiteLoss = WhiteLoss;
		}
	    }
	    LowWhite[EE][DD] = 0; LowBlack[EE][DD] = 0;
	 } // Next EE;
    } // Next DD;
    if(Drop == 1 ) {
	for(FF = 1; FF <= Capture; FF++) {
	    BlackTally = BlackTally + (Pieces[abs(CapRef[FF])].Value * 11) * (InHand[FF]);
	    WhiteTally = WhiteTally + (Pieces[abs(CapRef[FF + Capture])].Value * 11) * (InHand[FF + Capture]);
	} // Next FF;
    }
    BlackTally = BlackTally - HighBlackLoss; HighBlackLoss = 0;
    WhiteTally = WhiteTally - HighWhiteLoss; HighWhiteLoss = 0;
    if(!strcmp(Turn, "White") ) BestTally[1] = WhiteTally - BlackTally; else BestTally[1] = BlackTally - WhiteTally;
}

void AreaMove ()
{
  int X;
  AreaMove2();
  for(X = 1; X <= Area - 1; X++) {
    for(N = InitRank - X; N <= InitRank + X; N++) {
	if(N > 0 && N <= BoardSizeY ) {
	    for(P = InitFile - X; P <= InitFile + X; P++) {
		if(P > 0 && P <= BoardSizeX ) {
		    if(AreaOK[P][N] == 1 && (abs(P - InitFile) <= X && abs(N - InitRank) <= X) ) AreaMove2();
		}
	    } // Next P;
	}
    } // Next N;
  } // Next X;
}

void AreaMove2 ()
{
  int Q, S;
  Evaluate = 0;
  for(Q = N - 1; Q <= N + 1; Q++) {
    if(Q > 0 && Q <= BoardSizeY ) {
	for(S = P - 1; S <= P + 1; S++) {
	    if(S > 0 && S <= BoardSizeX ) {
		if(Squares[S][Q] == 0 ) {
		    Board.FillColor = 0xFFFFFF;
		    if(SeeMove == 1 ) { SeeFile = S; SeeRank = Q; LookMove(); }
		    Legal[S][Q] = 1;
		    AreaOK[S][Q] = 1;
		    NewFile = S; NewRank = Q; CheckBurn();
		    if(Demon == 1 ) FireDemon();
		} else {
		    if(Sgn(Squares[S][Q]) != Sgn(Squares[InitFile][InitRank]) || ((Influence > 0) && (S != InitFile || Q != InitRank)) ) {
			Board.FillColor = 0xFF; // 0xFF&
			if(SeeMove == 1 ) { SeeFile = S; SeeRank = Q; LookMove(); }
			Legal[S][Q] = 1;
			NewFile = S; NewRank = Q; CheckBurn();
			if(Demon == 1 ) FireDemon();
		    }
		}
	    }
	} // Next S;
    }
  } // Next Q;
}

void AskMate ()
{
  INT BB, CC, SS, TT, CheckDrop, OldCompMove, OldInfluence, OldEvaluate, TestFile, TestRank;
  if((!strcmp(Pieces[abs(CapRef[I])].Name, "Pawn") || !strcmp(Pieces[abs(CapRef[I])].Name, "Sparrow Pawn") || !strcmp(Pieces[abs(CapRef[I])].Name, "Swallow") || !strcmp(Pieces[abs(CapRef[I])].Name, "Dolphin")) && (strcmp(Choice, "Micro") && strcmp(Choice, "Yari")) ) {
    CheckDrop = 0;
    if(!strcmp(Turn, "Black") && WhiteKingY < BoardSizeY ) {
	if(Legal[WhiteKingX][WhiteKingY + 1] == 1 ) { Squares[WhiteKingX][WhiteKingY + 1] = CapRef[I]; File = WhiteKingX; Rank = WhiteKingY + 1; CheckDrop = 1; }
    }
    if(!strcmp(Turn, "White") && BlackKingY > 1 ) {
	if(Legal[BlackKingX][BlackKingY - 1] == 1 ) { Squares[BlackKingX][BlackKingY - 1] = CapRef[I]; File = BlackKingX; Rank = BlackKingY - 1; CheckDrop = 1; }
    }
    if(CheckDrop == 1 ) {
	for(CC = 1; CC <= BoardSizeY; CC++) {
	    for(BB = 1; BB <= BoardSizeX; BB++) {
		Comp[BB][CC] = Squares[BB][CC];
		OldLegal[BB][CC] = Legal[BB][CC];
	    } // Next BB;
	} // Next CC;
	OldInfluence = Influence; OldEvaluate = Evaluate; OldCompMove = CompMove;
	OldSeeMove = SeeMove; Influence = 3; Evaluate = 1; CompMove = 0; SeeMove = 1;
	TestFile = File; TestRank = Rank;
	MakeMap();
	File = TestFile; Rank = TestRank;
	for(CC = 1; CC <= BoardSizeY; CC++) {
	    for(BB = 1; BB <= BoardSizeX; BB++) {
		Squares[BB][CC] = Comp[BB][CC];
	    } // Next BB;
	} // Next CC;
	Influence = OldInfluence; Evaluate = OldEvaluate; CompMove = OldCompMove;
	if(!strcmp(Turn, "Black") ) {
	    if(BanMap[File][Rank].WhiteNum < 2 && BanMap[File][Rank].BlackNum > 0 ) {
		PawnMate = 1;
		for(SS = Rank - 2; SS <= Rank; SS++) {
		    for(TT = File - 1; TT <= File + 1; TT++) {
			if(TT > 0 && TT <= BoardSizeX && SS > 0 && SS <= BoardSizeY && (SS != Rank || TT != File) && (SS != WhiteKingX || TT != WhiteKingY) ) {
			    if(BanMap[TT][SS].BlackNum == 0 && Squares[TT][SS] >= 0 ) PawnMate = 0;
			}
		    } // Next TT;
		} // Next SS;
		if(PawnMate == 1 ) OldLegal[File][Rank] = 0;
	    }
	} else {
	    if(BanMap[File][Rank].BlackNum < 2 && BanMap[File][Rank].WhiteNum > 0 ) {
		PawnMate = 1;
		for(SS = Rank; SS <= Rank + 2; SS++) {
		    for(TT = File - 1; TT <= File + 1; TT++) {
			if(TT > 0 && TT <= BoardSizeX && SS > 0 && SS <= BoardSizeY && (SS != Rank || TT != File) && (SS != BlackKingY || TT != BlackKingX) ) {
			    if(BanMap[TT][SS].WhiteNum == 0 && Squares[TT][SS] <= 0 ) PawnMate = 0;
			}
		    } // Next TT;
		} // Next SS;
		if(PawnMate == 1 ) OldLegal[File][Rank] = 0;
	    }
	}
	SeeMove = OldSeeMove;
	for(AA = 1; AA <= BoardSizeY; AA++) {
	    for(BB = 1; BB <= BoardSizeX; BB++) {
		Squares[BB][AA] = Comp[BB][AA];
		Legal[BB][AA] = OldLegal[BB][AA];
	    } // Next BB;
	} // Next AA;
	Squares[File][Rank] = 0;
    }
  }
  PawnMate = 0;
}

void AutoMessage ()
{
  Board.PieceID.ForeColor = 0x8000; // 0x8000&
  sprintf(Board.PieceID.Caption, "%s promotes", PromPieceSTR);
  if(strcmp(Game, "Micro") && strcmp(Game, "Tori") && strcmp(Game, "Whale") && strcmp(Game, "Maka") && strcmp(Game, "DaiDai") && strcmp(Game, "Tai") && strcmp(Computer, Turn) && strcmp(Computer, "Both") ) {
    Notice = 1;
    sprintf(Board.Caption, "Double Click on new %s if not promoting.", Pieces[abs(Squares[File][Rank])].Name);
  }
}

void BugFix ()
{
  INT A, B, BugNo;
  Count = 0; Backwards = 0; BugNo = 64;
    if(!strcmp(Choice, "HShogi")) BugNo = 40;
    else if(!strcmp(Choice, "Wa")) BugNo = 69;
    else if(!strcmp(Choice, "Chu")) BugNo = 93;
    else if(!strcmp(Choice, "Dai")) BugNo = 132;
    else if(!strcmp(Choice, "Tenjiku")) BugNo = 158;
    else if(!strcmp(Choice, "DaiDai")) BugNo = 192;
    else if(!strcmp(Choice, "Maka")) BugNo = 192;
    else if(!strcmp(Choice, "Tai")) BugNo = 356;
    else if(!strcmp(Choice, "Heian")) BugNo = 92;
  for(A = 0; A <= BugNo; A++) {
    if(Board.showpic[A].Visible == False ) Count = Count + 1;
  } // Next A;
  for(A = 0; A <= BugNo; A++) {
    Board.showpic[A].Visible = False;
    for(B = 1; B <= BoardSizeY; B++) {
	for(C = 1; C <= BoardSizeX; C++) {
	    if(Grafix[C][B] == A ) Board.showpic[A].Visible = True;
	} // Next C;
    } // Next B;
  } // Next A;
  Setup = 0;
}

void ChangeGame ()
{ // Menu Function.
// TODO
//    UnloadStart();
    UnloadBoard();
    UnloadPieceHelp();
//  Unload RulesHelp;
//  Main(); // [HGM] we do this with delay, scheduled by caller
}

void ChangeSides ()
{
  if(!strcmp(Pieces[abs(Squares[File][Rank])].Name, "Porpoise") ) {
    strcpy(Board.Caption, "Captured Porpoise becomes a Killer Whale");
    Notice = 1;
  }
  if(Pieces[abs(Squares[File][Rank])].Promotes == 0 && Pieces[abs(Squares[File][Rank])].PrGraphic > 0 ) {
    CaptPiece = Pieces[abs(Squares[File][Rank])].PrGraphic;
    if(Squares[File][Rank] > 0 ) {
	CaptPiece = 0 - CaptPiece;
    }
  } else {
    CaptPiece = 0 - Squares[File][Rank];
  }
  AddHand();
}

void ChangeTurn ()
{ // Menu function to set side to move. NewTurn = "White" or "Black". (Calls EndSetup() before)
  if(!strcmp(NewTurn, "Black") ) {
    strcpy(Board.LastMove.Caption, "");
    MoveCount = 0; TurnCount = 0;
    strcpy(Board.NextMove.Caption, "Black to Move");
    strcpy(Turn, "Black");
    strcpy(Board.BlackClock.Caption, "00:00:00");
    strcpy(Board.WhiteClock.Caption, "00:00:00");
  }
  if(!strcmp(NewTurn, "White") ) {
    strcpy(Board.LastMove.Caption, "");
    MoveCount = 1; TurnCount = 0;
    strcpy(Turn, "White");
    strcpy(Board.NextMove.Caption, "White to Move");
    strcpy(Board.BlackClock.Caption, "00:00:00");
    strcpy(Board.WhiteClock.Caption, "00:00:00");
  }
}

void CheckAdd ()
{ // AddWhite/Black menu function. ExtraPiece = "White"/"Black" (after EndSetup())
  INT K, L;
  strcpy(Board.LastMove.Caption, "");
  Count = 0; Setup = 1;
  for(K = 1; K <= BoardSizeY; K++) {
    for(L = 1; L <= BoardSizeX; L++) {
	if(Squares[L][K] != 0 ) Count = Count + 1;
    } // Next L;
  } // Next K;
  if(Drop == 1 ) {
    for(K = 1; K <= Capture * 2; K++) {
	Count = Count + InHand[K];
	if(!strcmp(Choice, "Micro") ) Count = Count - (InHand[K] / 2);
    } // Next K;
  }
  if(Count >= Totpiece ) {
    strcpy(Board.Caption, "You can not add any more pieces!");
    Notice = 1;
    Selection = 0; MovePiece = 0;
    Board.Timer1.Enabled = True;
    TurnCount = 0; MoveCount = 0;
    if(Drop == 1 ) ResetHand();
    if(!strcmp(Turn, "White") ) MoveCount = 1;
    BugFix();
  } else {
    if(Selection == 0 ) LoadAddPieces();
  }
}

void CheckBurn ()
{
  INT K, L;
  Evaluate = 0;
  for(K = NewRank - 1; K <= NewRank + 1; K++) {
    for(L = NewFile - 1; L <= NewFile + 1; L++) {
	if(L > 0 && L <= BoardSizeX && K > 0 && K <= BoardSizeY ) {
	    if((K != NewRank || L != NewFile) && Squares[L][K] != 0 ) {
		if(Pieces[abs(Squares[L][K])].special == 'F' && Sgn(Squares[InitFile][InitRank]) != Sgn(Squares[L][K]) ) {
		    Board.FillColor = 0; // 0x0&
		    if(SeeMove == 1 ) { SeeFile = NewFile; SeeRank = NewRank; LookMove(); }
		    Legal[NewFile][NewRank] = 3;
		}
	    }
	}
    } // Next L;
  } // Next K;
}

void CheckEmperor ()
{
  INT FFF, GGG, OldInfluence, OldSeeFile, OldSeeRank;
  OldInfluence = Influence; ProtectLion = 0; OldSeeMove = SeeMove; SeeMove = 1;
  Influence = 1; ChuLionTest = 1; OldInitFile = InitFile; OldInitRank = InitRank;
  OldNewFile = NewFile; OldNewRank = NewRank; OldSeeFile = SeeFile; OldSeeRank = SeeRank; OldFile = File; OldRank = Rank;
  for(FFF = 1; FFF <= BoardSizeY; FFF++) {
    for(GGG = 1; GGG <= BoardSizeX; GGG++) {
	OldLegal[GGG][FFF] = Legal[GGG][FFF];
    } // Next GGG;
  } // Next FFF;
  for(FFF = 1; FFF <= BoardSizeY; FFF++) {
    for(GGG = 1; GGG <= BoardSizeX; GGG++) {
	if(Sgn(Squares[GGG][FFF]) == Sgn(Squares[OldNewFile][OldNewRank]) ) {
	    InitFile = GGG; InitRank = FFF; Validate();
	    if(ProtectLion == 1 ) { OldLegal[OldNewFile][OldNewRank] = 0; break; }
	}
    } // Next GGG;
    if(ProtectLion == 1 ) break;
  } // Next FFF;
  for(FFF = 1; FFF <= BoardSizeY; FFF++) {
    for(GGG = 1; GGG <= BoardSizeX; GGG++) {
	Legal[GGG][FFF] = OldLegal[GGG][FFF];
    } // Next GGG;
  } // Next FFF;
  Influence = OldInfluence;
  ChuLionTest = 0; InitFile = OldInitFile; InitRank = OldInitRank;
  NewFile = OldNewFile; NewRank = OldNewRank; SeeFile = OldSeeFile; SeeRank = OldSeeRank; File = OldFile; Rank = OldRank;
  SeeMove = OldSeeMove;
}

void CheckMate ()
{
  if(!strcmp(Turn, "White") ) {
    if(abs(Squares[File][Rank]) == 1 || !strcmp(Pieces[abs(Squares[File][Rank])].Name, "Emperor") ) BlackKing = 1;
    if(!strcmp(Pieces[abs(Squares[File][Rank])].Name, "Crown Prince") || !strcmp(Pieces[abs(Squares[File][Rank])].Name, "Prince") ) BlackPrince = BlackPrince - 1;
    if(!strcmp(Pieces[abs(Squares[File][Rank])].Name, "Emperor") ) BlackEmperor = 0;
  } else {
    if(!strcmp(Turn, "Black") ) {
	if(abs(Squares[File][Rank]) == 1 || !strcmp(Pieces[abs(Squares[File][Rank])].Name, "Emperor") ) WhiteKing = 1;
	if(!strcmp(Pieces[abs(Squares[File][Rank])].Name, "Emperor") ) WhiteEmperor = 0;
	if(!strcmp(Pieces[abs(Squares[File][Rank])].Name, "Crown Prince") || !strcmp(Pieces[abs(Squares[File][Rank])].Name, "Prince") ) WhitePrince = WhitePrince - 1;
    }
  }
  if(!strcmp(Choice, "Tenjiku") || !strcmp(Choice, "Maka") || !strcmp(Choice, "Tai") ) {
    if(BlackKing == 1 && BlackPrince == 0 ) { Mate = 1; GameOver = 1; }
    if(WhiteKing == 1 && WhitePrince == 0 ) { Mate = 1; GameOver = 1; }
  }
}

void ChuLion ()
{
  INT FFF, GGG, OldInfluence, OldSeeFile, OldSeeRank;
  OldInfluence = Influence; ProtectLion = 0; OldSeeMove = SeeMove; SeeMove = 1;
  Influence = 1; ChuLionTest = 1; OldInitFile = InitFile; OldInitRank = InitRank;
  OldNewFile = NewFile; OldNewRank = NewRank; OldSeeFile = SeeFile; OldSeeRank = SeeRank; OldFile = File; OldRank = Rank;
  for(FFF = 1; FFF <= BoardSizeY; FFF++) {
    for(GGG = 1; GGG <= BoardSizeX; GGG++) {
	OldLegal[GGG][FFF] = Legal[GGG][FFF];
    } // Next GGG;
  } // Next FFF;
  for(FFF = 1; FFF <= BoardSizeY; FFF++) {
    for(GGG = 1; GGG <= BoardSizeX; GGG++) {
	if(Sgn(Squares[GGG][FFF]) == Sgn(Squares[OldNewFile][OldNewRank]) ) {
	    InitFile = GGG; InitRank = FFF; Validate();
	    if(ProtectLion == 1 ) { OldLegal[OldNewFile][OldNewRank] = 0; break; }
	}
    } // Next GGG;
    if(ProtectLion == 1 ) break;
  } // Next FFF;
  for(FFF = 1; FFF <= BoardSizeY; FFF++) {
    for(GGG = 1; GGG <= BoardSizeX; GGG++) {
	Legal[GGG][FFF] = OldLegal[GGG][FFF];
    } // Next GGG;
  } // Next FFF;
  Influence = OldInfluence;
  ChuLionTest = 0; InitFile = OldInitFile; InitRank = OldInitRank; SeeMove = OldSeeMove;
  NewFile = OldNewFile; NewRank = OldNewRank; SeeFile = OldSeeFile; SeeRank = OldSeeRank; File = OldFile; Rank = OldRank;
}

void ClearBoard ()
{ // Menu function. (calls EndSetup() before.)
  INT A, B;
  int Response;
  Response = MsgBox("Are You Sure ?", 36, "Clear the Board");
  if(Response == 6 ) {
    for(A = 1; A <= BoardSizeY; A++) {
	for(B = 1; B <= BoardSizeX; B++) {
	    if(Squares[B][A] != 0 ) {
		Board.showpic[Grafix[B][A]].Visible = False;
		MOVE(0, Board.showpic[Grafix[B][A]], 0, 0);
		Squares[B][A] = 0;
	    }
	    Grafix[B][A] = -1;
	} // Next B;
    } // Next A;
    if(Drop == 1 ) {
	for(A = 1; A <= Capture * 2; A++) {
	    InHand[A] = 0;
	    Board.HandPic[A].Visible = False;
	    strcpy(Board.Held[A].Caption, "");
	} // Next A;
    }
    WhitePrince = 0; BlackPrince = 0;
    strcpy(Board.LastMove.Caption, "");
    MoveCount = 0; TurnCount = 0;
    strcpy(Board.BlackClock.Caption, "00:00:00");
    strcpy(Board.WhiteClock.Caption, "00:00:00");
    Board.Timer1.Enabled = False;
  }
}

void ClearInfo ()
{
  INT JJ, KK, MM, NN;
  if(CapturedPiece != 1 ) {
    Changed = Changed + 1;
    Clearing[Changed].File = File;
    Clearing[Changed].Rank = Rank;
  }
  CapturedPiece = 0;
  for(JJ = 1; JJ <= BoardSizeY; JJ++) {
    for(KK = 1; KK <= BoardSizeX; KK++) {
	for(MM = 1; MM <= Attacker[KK][JJ]; MM++) {
	    if(BanMap[KK][JJ].Info[MM].File == File && BanMap[KK][JJ].Info[MM].Rank == Rank ) {
		if(Attacker[KK][JJ] == 1 ) {
		    BanMap[KK][JJ].Info[MM].File = 0;
		    BanMap[KK][JJ].Info[MM].Rank = 0;
		    BanMap[KK][JJ].Info[MM].Piece = 0;
		} else {
		  for(NN = MM; NN <= Attacker[KK][JJ] - 1; NN++) {
		    BanMap[KK][JJ].Info[NN].File = BanMap[KK][JJ].Info[NN + 1].File;
		    BanMap[KK][JJ].Info[NN].Rank = BanMap[KK][JJ].Info[NN + 1].Rank;
		    BanMap[KK][JJ].Info[NN].Piece = BanMap[KK][JJ].Info[NN + 1].Piece;
		  } // Next NN;
		}
		Attacker[KK][JJ] = Attacker[KK][JJ] - 1;
		if(Squares[File][Rank] < 0 ) {
		    BanMap[KK][JJ].WhiteNum = BanMap[KK][JJ].WhiteNum - 1;
		} else {
		    if(Squares[File][Rank] > 0 ) {
			BanMap[KK][JJ].BlackNum = BanMap[KK][JJ].BlackNum - 1;
		    }
		}
	    }
	} // Next MM;
    } // Next KK;
  } // Next JJ;

}

void ClearLegal ()
{ // default DragDrop handler in almost all visible objects.
  // MouseUp in Form if LegalMoves = 0, also triggered by key 192
  INT A, B;
  if(Notice == 0 && Checked != 1 ) strcpy(Board.PieceID.Caption, "");
  if(GameOver == 1 ) {
    strcpy(Board.NextMove.Caption, "Game Over");
  } else {
    if(!strcmp(Turn, "White") ) strcpy(Board.NextMove.Caption, "White to Move"); else strcpy(Board.NextMove.Caption, "Black to Move");
  }
  if(SeeMove == 1 ) Board.Refresh();
  for(A = 1; A <= BoardSizeY; A++) {
   for(B = 1; B <= BoardSizeX; B++) {
       Legal[B][A] = 0;
       AreaOK[B][A] = 0;
   } // Next B;
  } // Next A;
}

void Clock ()
{ // Handler for Timer1 interrupts.
  if(Timing == 0 && GameOver != 1 ) {
    if((!strcmp(Turn, "White") && Level > 0) || (!strcmp(Turn, "Black") && Level == 0) ) {
	strcpy(ElapsedSTR, Board.WhiteClock.Caption);
	SetClock();
	strcpy(Board.WhiteClock.Caption, ElapsedSTR);
    } else {
	strcpy(ElapsedSTR, Board.BlackClock.Caption);
	SetClock();
	strcpy(Board.BlackClock.Caption, ElapsedSTR);
    }
  }
}

void ClocksOff ()
{ // Menu function.
  Board.Timer1.Enabled = False;
  strcpy(Board.WhiteClock.Caption, "00:00:00");
  strcpy(Board.BlackClock.Caption, "00:00:00");
  Timing = 1;
  Board.WhiteClock.Visible = False;
  Board.BlackClock.Visible = False;
  Board.MnuClockOff.Enabled = False;
  Board.MnuClockOn.Enabled = True;
  Board.MnuClockOff.Checked = True;
  Board.MnuClockOn.Checked = False;
}

void ClocksOn ()
{ // Menu function.
  Board.Timer1.Enabled = True;
  strcpy(Board.WhiteClock.Caption, "00:00:00");
  strcpy(Board.BlackClock.Caption, "00:00:00");
  Timing = 0;
  Board.WhiteClock.Visible = True;
  Board.BlackClock.Visible = True;
  Board.MnuClockOff.Enabled = True;
  Board.MnuClockOn.Enabled = False;
  Board.MnuClockOn.Checked = True;
  Board.MnuClockOff.Checked = False;
}

void CompLion ()
{
  INT DeadPiece;
  CompLionTest = 1;
  AddLegalMove();
  DeadPiece = Squares[NewFile][NewRank];
  Squares[NewFile][NewRank] = Squares[InitFile][InitRank];
  Squares[InitFile][InitRank] = 0;
  for(YYY = NewRank - 1; YYY <= NewRank + 1; YYY++) {
    for(XXX = NewFile - 1; XXX <= NewFile + 1; XXX++) {
	if(XXX > 0 && XXX <= BoardSizeX && YYY > 0 && YYY <= BoardSizeY ) {
	    if(Squares[XXX][YYY] == 0 || Sgn(Squares[XXX][YYY]) != Sgn(Squares[NewFile][NewRank]) ) {
		AddLionMove();
		ExtraCapture[LegalMoves].Piece = DeadPiece;
		ExtraCapture[LegalMoves].File = NewFile;
		ExtraCapture[LegalMoves].Rank = NewRank;
	    }
	}
    } // Next XXX;
  } // Next YYY;
  Squares[InitFile][InitRank] = Squares[NewFile][NewRank];
  Squares[NewFile][NewRank] = DeadPiece;
}

void CompLionPower ()
{
  INT WWW, DeadPiece;
  CompLionTest = 1;
  AddLegalMove();
  DeadPiece = Squares[NewFile][NewRank];
  Squares[NewFile][NewRank] = Squares[InitFile][InitRank];
  Squares[InitFile][InitRank] = 0;
  for(WWW = -1; WWW <= 1; WWW += 2) {
    XXX = NewFile + (FileInc * WWW);
    YYY = NewRank + (RankInc * WWW);
    if(XXX > 0 && XXX <= BoardSizeX && YYY > 0 && YYY <= BoardSizeY ) {
	if(Squares[XXX][YYY] == 0 || Sgn(Squares[XXX][YYY]) != Sgn(Squares[NewFile][NewRank]) ) {
	    AddLionMove();
	    ExtraCapture[LegalMoves].Piece = DeadPiece;
	    ExtraCapture[LegalMoves].File = NewFile;
	    ExtraCapture[LegalMoves].Rank = NewRank;
	}
    }
  } // Next WWW;
  Squares[InitFile][InitRank] = Squares[NewFile][NewRank];
  Squares[NewFile][NewRank] = DeadPiece;
}

void CompMain ()
{
  INT A, B, BB, YZ, VX, TU, AB, CD, OldInfluence;
  if(Level != 0 && (!strcmp(Computer, "White") || !strcmp(Computer, "Black")) ) // Board.MnuSwitch.Enabled = True; else Board.MnuSwitch.Enabled = False;
    EnableNamedMenuItem("Setup.SwitchSides", True); else EnableNamedMenuItem("Setup.SwitchSides", False); // [HGM] added

// Find Legal Moves;

  if(GameOver == 1 ) return;
  for(AA = 1; AA <= BoardSizeY; AA++) {
    for(BB = 1; BB <= BoardSizeX; BB++) {
	Comp[BB][AA] = Squares[BB][AA];
    } // Next BB;
  } // Next AA;
  CompMove = 1; LegalMoves = 0;
  for(YZ = 1; YZ <= BoardSizeY; YZ++) {
    for(VX = 1; VX <= BoardSizeX; VX++) {
	if(!strcmp(Turn, "White") ) {
	    if(Squares[VX][YZ] < 0 ) {
		InitFile = VX; InitRank = YZ;
		I = Grafix[VX][YZ];
		Validate();
		Taken = 0;
	    }
	} else {
	    if(Squares[VX][YZ] > 0 ) {
		InitFile = VX; InitRank = YZ;
		I = Grafix[VX][YZ];
		Validate();
		Taken = 0;
	    }
	}
    } // Next VX;
    DoEvents();
  } // Next YZ;

// Find Legal Drops;

  if(Drop == 1 ) {
    if((!strcmp(Turn, "Black") && Reverse == 0) || (!strcmp(Turn, "White") && Reverse == 1) ) {
	for(TU = 1; TU <= Capture; TU++) {
	    ResetLegal();
	    if(InHand[TU] > 0 ) {
		I = TU; InitFile = 0; InitRank = 0;
		HeldValid();
		DoEvents();
	    }
	} // Next TU;
    } else {
	for(TU = Capture + 1; TU <= Capture * 2; TU++) {
	    if(InHand[TU] > 0 ) {
		ResetLegal();
		I = TU; InitFile = 0; InitRank = 0;
		HeldValid();
		DoEvents();
	    }
	} // Next TU;
    }
  }

// Find Legal Emperor Moves;

  if(!strcmp(Choice, "Tai") || !strcmp(Choice, "Maka") ) {
    OldInfluence = Influence; CompMove = 0;
    if(WhiteEmperor == 1 && BlackEmperor == 1 ) AddEmperorAttack();
    if((!strcmp(Turn, "White") && WhiteEmperor == 1) || (!strcmp(Turn, "Black") && BlackEmperor == 1) ) {
	Influence = 2;
	for(AB = 1; AB <= BoardSizeY; AB++) {
	    for(CD = 1; CD <= BoardSizeX; CD++) {
		InitFile = CD; InitRank = AB; Validate();
	    } // Next CD;
	} // Next AB;
	FindEmperorMove();
    }
    Influence = OldInfluence; CompMove = 1;
  }

  FirstLegal = LegalMoves;
  if(FirstLegal < Level ) TestDepth = FirstLegal; else TestDepth = Level;
  if(LookMate == 1 ) ConsiderMate(); else ConsiderMove();
  Evaluate = 0; CompMove = 0; Influence = 0; EndMove = 0;
  if(Suggest == 1 ) { SuggestMove(); return; }
  if(GameOver == 1 ) return;
  if(Level > 1 ) LookAhead();
    
// Make Best Move;

  for(AA = 1; AA <= BoardSizeY; AA++) {
    for(BB = 1; BB <= BoardSizeX; BB++) {
	Squares[BB][AA] = Comp[BB][AA];
    } // Next BB;
  } // Next AA;
  for(A = 1; A <= BoardSizeY; A++) {
    for(B = 1; B <= BoardSizeX; B++) {
	Legal[B][A] = 0;
	AreaOK[B][A] = 0;
    } // Next B;
  } // Next A;
  OldSeeMove = SeeMove; SeeMove = 0;
  if(LookMate == 1 ) return;
  if(CompLegal[BestMove].StartFile == 0 ) {
    InitFile = 0;
    DropTest = 1;
    TestDrop();
  } else {
    InitRank = CompLegal[BestMove].StartRank;
    InitFile = CompLegal[BestMove].StartFile;
    I = Grafix[InitFile][InitRank];
    if(!strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "Emperor") ) Legal[CompLegal[BestMove].EndFile][CompLegal[BestMove].EndRank] = 1;
    Validate();
  }
  SeeMove = OldSeeMove; CompMove = 1; Evaluate = 0; MakeMove = 1;
  File = CompLegal[BestMove].EndFile;
  Rank = CompLegal[BestMove].EndRank;
  if(InitFile == 0 ) {
    MakeDrop();
    DropTest = 0;
  } else {
    if(Pieces[abs(Squares[InitFile][InitRank])].special == 'L' || !strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "Horned Falcon") || !strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "Soaring Eagle") ) Legal[InitFile][InitRank] = 1;
    I = Grafix[InitFile][InitRank];
    if(Squares[File][Rank] == 0 ) {
	FormDrop();
    } else {
	NewIndex = Grafix[File][Rank];
	PicDrop();
    }
  }
  if(Taken == 1 && (!strcmp(Choice, "DaiDai") || !strcmp(Choice, "Maka") || !strcmp(Choice, "Tai")) ) {
    I = Grafix[File][Rank];
    Promote();
  } else {
    if(CompLegal[BestMove].StartPiece != CompLegal[BestMove].EndPiece && Squares[File][Rank] != 0 ) {
      I = Grafix[File][Rank];
      Promote();
    }
  }
  CompMove = 0; Evaluate = 0; LegalMoves = 0;
  if(Mate != 1 ) NextTurn2();
}

void CompMate ()
{
  INT SS, TT, MateCheck;
  if(Squares[File][Rank] == 1 && !strcmp(Turn, "White") ) {
    if((abs(File - CompLegal[AA].EndFile) < 2 && abs(Rank - CompLegal[AA].EndRank) < 2) && BanMap[CompLegal[AA].EndFile][CompLegal[AA].EndRank].BlackNum < 2 && BanMap[CompLegal[AA].EndFile][CompLegal[AA].EndRank].WhiteNum > 0 && BanMap[File][Rank].WhiteNum > 0 && CompLegal[AA].EndPiece != -1 ) {
	MateCheck = 1;
	for(SS = Rank - 1; SS <= Rank + 1; SS++) {
	    for(TT = File - 1; TT <= File + 1; TT++) {
		if(TT > 0 && TT <= BoardSizeX && SS > 0 && SS <= BoardSizeY && (SS != Rank || TT != File) ) {
		    if(BanMap[TT][SS].WhiteNum == 0 && Squares[TT][SS] <= 0 ) MateCheck = 0;
		}
	    } // Next TT;
	} // Next SS;
	if(MateCheck == 1 ) WhiteTally = WhiteTally + 8888; else WhiteTally = WhiteTally + 20;
    } else {
	if(BanMap[File][Rank].WhiteNum > 0 && BanMap[CompLegal[AA].EndFile][CompLegal[AA].EndRank].BlackNum == 0 && CompLegal[AA].EndPiece != -1 ) WhiteTally = WhiteTally + 20;
    }
  }
  if(Squares[File][Rank] == -1 && !strcmp(Turn, "Black") ) {
    if(abs(File - CompLegal[AA].EndFile) < 2 && abs(Rank - CompLegal[AA].EndRank) < 2 && BanMap[CompLegal[AA].EndFile][CompLegal[AA].EndRank].WhiteNum < 2 && BanMap[CompLegal[AA].EndFile][CompLegal[AA].EndRank].BlackNum > 0 && BanMap[File][Rank].BlackNum > 0 && CompLegal[AA].EndPiece != 1 ) {
	MateCheck = 1;
	for(SS = Rank - 1; SS <= Rank + 1; SS++) {
	    for(TT = File - 1; TT <= File + 1; TT++) {
		if(TT > 0 && TT <= BoardSizeX && SS > 0 && SS <= BoardSizeY && (SS != Rank || TT != File) ) {
		    if(BanMap[TT][SS].BlackNum == 0 && Squares[TT][SS] >= 0 ) MateCheck = 0;
		}
	    } // Next TT;
	} // Next SS;
	if(MateCheck == 1 ) BlackTally = BlackTally + 8888; else BlackTally = BlackTally + 20;
    } else {
	if(BanMap[File][Rank].BlackNum > 0 && BanMap[CompLegal[AA].EndFile][CompLegal[AA].EndRank].WhiteNum == 0 && CompLegal[AA].EndPiece != 1 ) BlackTally = BlackTally + 20;
    }
  }
}

void CompPromote ()
{
  ForceProm = 0; Taken = 0;
  if(!strcmp(Choice, "Micro") || !strcmp(Choice, "DaiDai") || !strcmp(Choice, "Maka") || !strcmp(Choice, "Tai") ) {
    if(Squares[SeeFile][SeeRank] != 0 ) {
	if(Sgn(Squares[InitFile][InitRank]) != Sgn(Squares[SeeFile][SeeRank]) ) {
	    Taken = 1; ForceProm = 1;
	    CompPromote2();
	}
    }
  } else {
    if(Pieces[abs(Squares[InitFile][InitRank])].special == '1' ) {
	if(Squares[InitFile][InitRank] > 0 && SeeRank == 1 ) { ForceProm = 1; CompPromote2(); }
	if(Squares[InitFile][InitRank] < 0 && SeeRank == BoardSizeY ) { ForceProm = 1; CompPromote2(); }
    }
    if(Pieces[abs(Squares[InitFile][InitRank])].special == '2' ) {
	if(Squares[InitFile][InitRank] > 0 && SeeRank < 3 ) { ForceProm = 1; CompPromote2(); }
	if(Squares[InitFile][InitRank] < 0 && SeeRank > BoardSizeY - 2 ) { ForceProm = 1; CompPromote2(); }
    }
    if(Squares[InitFile][InitRank] > 0 && (SeeRank <= PromDotY || InitRank <= PromDotY) ) CompPromote2();
    if(Squares[InitFile][InitRank] < 0 && (SeeRank > BoardSizeY - PromDotY || InitRank > BoardSizeY - PromDotY) ) CompPromote2();
  }
}

void CompPromote2 ()
{
  if((strcmp(Choice, "Micro") && strcmp(Choice, "Tai") && strcmp(Choice, "Maka") && strcmp(Choice, "DaiDai")) || (Taken == 1) ) {
    if((ForceProm == 1 && Pieces[abs(Squares[InitFile][InitRank])].Promotes != 0) || Prom == 1 ) {
	CompLegal[LegalMoves].EndPiece = Pieces[abs(Squares[InitFile][InitRank])].Promotes;
	if(CompLegal[LegalMoves].StartPiece < 0 ) CompLegal[LegalMoves].EndPiece = 0 - CompLegal[LegalMoves].EndPiece;
    } else {
	if((Pieces[abs(Squares[InitFile][InitRank])].Promotes != 0) ) {
	    LegalMoves = LegalMoves + 1;
	    CompLegal[LegalMoves].StartFile = InitFile;
	    CompLegal[LegalMoves].StartRank = InitRank;
	    CompLegal[LegalMoves].EndFile = SeeFile;
	    CompLegal[LegalMoves].EndRank = SeeRank;
	    CompLegal[LegalMoves].StartPiece = Squares[InitFile][InitRank];
	    CompLegal[LegalMoves].EndPiece = Pieces[abs(Squares[InitFile][InitRank])].Promotes;
	    if(Squares[InitFile][InitRank] < 0 ) CompLegal[LegalMoves].EndPiece = 0 - CompLegal[LegalMoves].EndPiece;
	}
    }
  }
}

void CompTeach ()
{
  Board.MnuVer2.Enabled = False;
  Board.MnuVer1.Enabled = False;
  Board.MnuVer1.Checked = True;
  Board.MnuVer2.Checked = False;
  TeachVer = 1;
  if(!strcmp(Choice, "Tai") ) {
    Pieces[29].PrValue = 28; Pieces[100].Value = 28;
  } else {
    Pieces[29].PrValue = 28; Pieces[76].Value = 28;
  }
}

void CompTurn ()
{
  if(LegalMoves == 0 ) {
    strcpy(Board.Caption, Cap);
    if(!strcmp(Computer, Turn) ) {
	FirstSeeMove = SeeMove;
	SeeMove = 1;
	CompMain();
    }
  }
}

void CompVComp ()
{
  do {
    CompMain();
  } while( !( GameOver == 1 || Mate == 1) );
  if(GameOver == 1 ) NextTurn2();
  GameOver = 0;
}

void ConfigLoad ()
{ // WARNING: this does not allow values to be missing, as in the Basic code
  if(Loading != 1 ) {
    FILE *f4;
    if(f4 = fopen((sprintf(StringTmp, CONFIGFILE), StringTmp), "r")) { // [HGM] allow for failing
	fscanf(f4, "%d,%d,\"%[^\"]\",\"%[^\"]\",%d,%d,%d,%d,%d,\"%[^\"],%d", &SeeMove, &Timing, Computer, Threat, &AutoPromote, &Notate, &LionHawkVer, &TeachVer, &Eval, Grade, &ShowLast);
	fclose(f4);
    }
  }
  if(SeeMove == 1 ) {
    Board.MnuShowOn.Enabled = False;
    Board.MnuShowOn.Checked = True;
    Board.MnuShowOff.Enabled = True;
    Board.MnuShowOff.Checked = False;
  } else {
    Board.MnuShowOn.Enabled = True;
    Board.MnuShowOn.Checked = False;
    Board.MnuShowOff.Enabled = False;
    Board.MnuShowOff.Checked = True;
  }
  MarkMenuItem("Moves.ShowLegal", SeeMove == 1); // [HGM] added
  ConfigLoad2();
  if(ShowLast == 1 ) {
    Board.MnuShowLastOn.Enabled = False;
    Board.MnuShowLastOn.Checked = True;
    Board.MnuShowLastOff.Enabled = True;
    Board.MnuShowLastOff.Checked = False;
  } else {
    Board.MnuShowLastOn.Enabled = True;
    Board.MnuShowLastOn.Checked = False;
    Board.MnuShowLastOff.Enabled = False;
    Board.MnuShowLastOff.Checked = True;
  }
  MarkMenuItem("Moves.Flash", ShowLast == 1); // [HGM] added
  if(!strcmp(Threat, "On") ) {
    Board.MnuThreatOn.Enabled = False;
    Board.MnuThreatOn.Checked = True;
    Board.MnuThreatOff.Enabled = True;
    Board.MnuThreatOff.Checked = False;
  } else {
    Board.MnuThreatOn.Enabled = True;
    Board.MnuThreatOn.Checked = False;
    Board.MnuThreatOff.Enabled = False;
    Board.MnuThreatOff.Checked = True;
  }
  MarkMenuItem("Moves.ShowThreat", !strcmp(Threat, "On")); // [HGM] added

  if(strcmp(Choice, "Whale") && strcmp(Choice, "Maka") && strcmp(Choice, "Tai") && strcmp(Choice, "DaiDai") && strcmp(Choice, "Tori") && strcmp(Choice, "Micro") ) {
    if(AutoPromote == 1 ) {
	Board.MnuAutoOn.Enabled = False;
	Board.MnuAutoOn.Checked = True;
	Board.MnuAutoOff.Enabled = True;
	Board.MnuAutoOff.Checked = False;
    } else {
	Board.MnuAutoOn.Enabled = True;
	Board.MnuAutoOn.Checked = False;
	Board.MnuAutoOff.Enabled = False;
	Board.MnuAutoOff.Checked = True;
    }
    MarkMenuItem("Setup.AutoProm", AutoPromote == 1); // [HGM] added
  }

  if(Eval == 1 ) {
    Board.MnuEvalOn.Enabled = False;
    Board.MnuEvalOn.Checked = True;
    Board.MnuEvalOff.Enabled = True;
    Board.MnuEvalOff.Checked = False;
  } else {
    Board.MnuEvalOn.Enabled = True;
    Board.MnuEvalOn.Checked = False;
    Board.MnuEvalOff.Enabled = False;
    Board.MnuEvalOff.Checked = True;
  }
  MarkMenuItem("Moves.Evaluate", Eval == 1); // [HGM] added

  if(!strcmp(Choice, "Tenjiku") ) Board.MnuEvalOn.Enabled = False;
  if(!strcmp(Choice, "Tenjiku") ) {
    if(LionHawkVer == 1 ) {
	Board.MnuLVer1.Enabled = False;
	Board.MnuLVer1.Checked = True;
	Board.MnuLVer2.Enabled = True;
	Board.MnuLVer2.Checked = False;
    } else {
	Board.MnuLVer1.Enabled = True;
	Board.MnuLVer1.Checked = False;
	Board.MnuLVer2.Enabled = False;
	Board.MnuLVer2.Checked = True;
    }
    MarkMenuItem("Setup.LionHawk", LionHawkVer == 2); // [HGM] added
  }
  if(!strcmp(Choice, "Maka") || !strcmp(Choice, "Tai") ) {
    if(TeachVer == 1 ) {
	Board.MnuVer1.Enabled = False;
	Board.MnuVer1.Checked = True;
	Board.MnuVer2.Enabled = True;
	Board.MnuVer2.Checked = False;
    } else {
	Board.MnuVer1.Enabled = True;
	Board.MnuVer1.Checked = False;
	Board.MnuVer2.Enabled = False;
	Board.MnuVer2.Checked = True;
    }
    MarkMenuItem("Setup.TeachingKing", TeachVer == 2); // [HGM] added
  }
  if(Notate == 1 ) {
    Board.NotTop.Visible = True;
    Board.NotSide.Visible = True;
    Board.MnuNotOff.Enabled = True;
    Board.MnuNotOff.Checked = False;
    Board.MnuNotOn.Enabled = False;
    Board.MnuNotOn.Checked = True;
  } else {
    Board.NotTop.Visible = False;
    Board.NotSide.Visible = False;
    Board.MnuNotOff.Enabled = False;
    Board.MnuNotOff.Checked = True;
    Board.MnuNotOn.Enabled = True;
    Board.MnuNotOn.Checked = False;
  }
  MarkMenuItem("Setup.Notation", Notate == 1); // [HGM] added
  SetDifficulty();
}

void ConfigLoad2 ()
{
  if(!strcmp(Choice, "Tenjiku") ) strcpy(Computer, "None");
  if(!strcmp(Computer, "White") || !strcmp(Computer, "Both") ) {
    Board.MnuWhitePlayer.Enabled = True;
    Board.MnuWhitePlayer.Checked = False;
    Board.MnuWhiteComp.Enabled = False;
    Board.MnuWhiteComp.Checked = True;
    Board.MnuBlackPlayer.Enabled = False;
    Board.MnuBlackPlayer.Checked = True;
    Board.MnuBlackComp.Enabled = True;
    Board.MnuBlackComp.Checked = False;
  }
  if(!strcmp(Computer, "Black") ) {
    Board.MnuWhitePlayer.Enabled = False;
    Board.MnuWhitePlayer.Checked = True;
    Board.MnuWhiteComp.Enabled = True;
    Board.MnuWhiteComp.Checked = False;
    Board.MnuBlackPlayer.Enabled = True;
    Board.MnuBlackPlayer.Checked = False;
    Board.MnuBlackComp.Enabled = False;
    Board.MnuBlackComp.Checked = True;
  }
  if(!strcmp(Computer, "") || !strcmp(Computer, "None") ) {
    Board.MnuWhitePlayer.Enabled = False;
    Board.MnuWhitePlayer.Checked = True;
    Board.MnuWhiteComp.Enabled = True;
    Board.MnuWhiteComp.Checked = False;
    Board.MnuBlackPlayer.Enabled = False;
    Board.MnuBlackPlayer.Checked = True;
    Board.MnuBlackComp.Enabled = True;
    Board.MnuBlackComp.Checked = False;
  }
  MarkMenuItem("Setup.CompWhite", !strcmp(Computer, "White") || !strcmp(Computer, "Both")); // [HGM] added
  MarkMenuItem("Setup.CompBlack", !strcmp(Computer, "Black") || !strcmp(Computer, "Both")); // [HGM] added
  if(!strcmp(Choice, "Tenjiku") ) {
    Eval = 0;
    Board.MnuWhiteComp.Enabled = False;
    Board.MnuBlackComp.Enabled = False;
  }
  EnableNamedMenuItem("Setup.CompWhite", !!strcmp(Choice, "Tenjiku")); // [HGM] added
  EnableNamedMenuItem("Setup.CompBlack", !!strcmp(Choice, "Tenjiku")); // [HGM] added
}

void ConfigSave ()
{ // Callen on Quit from menu.
  int Response;
  Response = MsgBox("Do you wish to save " LF "your Game Preferences?", 36, "");
  if(Response == 6 ) {
    FILE *f4;
    if(f4 = fopen((sprintf(StringTmp, CONFIGFILE), StringTmp), "w")) {
	fprintf(f4, "%d,%d,\"%s\",\"%s\",%d,%d,%d,%d,%d,\"%s\",%d\n", SeeMove, Timing, Computer, Threat, AutoPromote, Notate, LionHawkVer, TeachVer, Eval, Grade, ShowLast);
	fclose(f4);
    }
  }
}

void ConsiderMate ()
{
  INT DD, EE, JJ, KK, PP, StartAA, EndAA, Steps;
  BestTally[0] = -999999; CompMove = 0; Influence = 3; Evaluate = 1;
  MakeMap();
  if(!strcmp(Turn, "Black") ) {
    Steps = -1;
    StartAA = LegalMoves;
    EndAA = 1;
  } else {
    Steps = 1;
    StartAA = 1;
    EndAA = LegalMoves;
  }
  for(AA = StartAA; Steps > 0 ? AA <= EndAA : AA >= EndAA; AA += Steps) {
    WhiteTally = 0; BlackTally = 0;
    DoEvents();
    Evaluate = 1;
    for(JJ = 1; JJ <= BoardSizeY; JJ++) {
	for(KK = 1; KK <= BoardSizeX; KK++) {
	    Squares[KK][JJ] = Comp[KK][JJ];
	} // Next KK;
    } // Next JJ;
    InitFile = CompLegal[AA].StartFile;
    InitRank = CompLegal[AA].StartRank;
    CompFile = CompLegal[AA].EndFile;
    CompRank = CompLegal[AA].EndRank;
    Evaluate = 0;
    FindInfluence();
    if(InitFile != 0 ) Squares[InitFile][InitRank] = 0;
    Squares[CompFile][CompRank] = CompLegal[AA].EndPiece;
    Evaluate = 1;
    for(PP = 1; PP <= Changed; PP++) {
	InitFile = Clearing[PP].File;
	InitRank = Clearing[PP].Rank;
	if(Squares[InitFile][InitRank] != 0 ) Validate();
    } // Next PP;
    Evaluate = 0;
    for(DD = 1; DD <= BoardSizeY; DD++) {
	for(EE = 1; EE <= BoardSizeX; EE++) {
	    if(Squares[EE][DD] != 0 ) {
		if(BanMap[EE][DD].WhiteNum > 0 && (Squares[EE][DD] == 1 || (Squares[EE][DD] > 0 && BlackKing == 1 && (!strcmp(Pieces[abs(Squares[EE][DD])].Name, "Crown Prince") || !strcmp(Pieces[abs(Squares[EE][DD])].Name, "Prince")))) ) BlackTally = -99999;
		if(BanMap[EE][DD].BlackNum > 0 && (Squares[EE][DD] == -1 || (Squares[EE][DD] < 0 && WhiteKing == 1 && (!strcmp(Pieces[abs(Squares[EE][DD])].Name, "Crown Prince") || !strcmp(Pieces[abs(Squares[EE][DD])].Name, "Prince")))) ) WhiteTally = -99999;
	    }
	} // Next EE;
    } // Next DD;
    if(!strcmp(Turn, "White") && WhiteTally == 0 ) { BestTally[0] = 0; return; }
    if(!strcmp(Turn, "Black") && BlackTally == 0 ) { BestTally[0] = 0; return; }
    RestoreMap();
  } // Next AA;
  LegalMoves = 0;
}

void ConsiderMove ()
{
  INT BB, DD, EE, FF, JJ, KK, PP, QQ, JJJ, KKK, BlackLoss, WhiteLoss, HighBlackLoss, HighWhiteLoss;
// Evaluate Legal Moves;

  BestTally[0] = -999999; CompMove = 0; Influence = 3; Evaluate = 1;
  if(Level > 0 ) {
    for(BB = 1; BB <= Level + 1; BB++) {
	BestTally[BB] = -999999;
	OldKingTally[BB] = 0;
	ECapture[BB].Piece = 0;
    } // Next BB;
  }
  MakeMap();
  for(AA = 1; AA <= LegalMoves; AA++) {
    KingTally[AA] = 0; WhiteTally = 0; BlackTally = 0;
    DoEvents();
    Evaluate = 1;
    for(JJ = 1; JJ <= BoardSizeY; JJ++) {
	for(KK = 1; KK <= BoardSizeX; KK++) {
	    Squares[KK][JJ] = Comp[KK][JJ];
	} // Next KK;
    } // Next JJ;
    InitFile = CompLegal[AA].StartFile;
    InitRank = CompLegal[AA].StartRank;
    CompFile = CompLegal[AA].EndFile;
    CompRank = CompLegal[AA].EndRank;
    if(!strcmp(Choice, "Maka") || !strcmp(Choice, "Tai") ) {
	if(!strcmp(Turn, "Black") && !strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "Emperor") ) BlackTally = BlackTally - 20;
	if(!strcmp(Turn, "White") && !strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "Emperor") ) WhiteTally = WhiteTally - 20;
    }
    if(!strcmp(Choice, "Shogi") && TurnCount <= 1 ) {
	if(InitFile == 6 && InitRank == 1 ) KingTally[AA] = KingTally[AA] - 5;
	if(InitFile == 5 && InitRank == 1 ) KingTally[AA] = KingTally[AA] - 2;
	if((InitRank == 3 || InitRank == 7) ) KingTally[AA] = KingTally[AA] + 1;
	if(InitFile == 4 && InitRank == 9 ) KingTally[AA] = KingTally[AA] - 5;
	if(InitFile == 5 && InitRank == 9 ) KingTally[AA] = KingTally[AA] - 2;
	if((InitFile == 2 && InitRank == 3) || (InitFile == 8 && InitRank == 7) ) KingTally[AA] = KingTally[AA] + 4;
    }
    if(Drop == 1 && Level == 0 ) {
	if(!strcmp(Turn, "Black") && Squares[CompFile][CompRank] < 0 ) {
	    if(Pieces[abs(Squares[CompFile][CompRank])].Promotes == 0 && Pieces[abs(Squares[CompFile][CompRank])].PrGraphic > 0 )
		 BlackTally = BlackTally + (Pieces[Pieces[abs(Squares[CompFile][CompRank])].PrGraphic].Value * 10);
	    else BlackTally = BlackTally + (Pieces[abs(Squares[CompFile][CompRank])].Value * 10);
	}
	if(!strcmp(Turn, "White") && Squares[CompFile][CompRank] > 0 ) {
	    if(Pieces[abs(Squares[CompFile][CompRank])].Promotes == 0 && Pieces[abs(Squares[CompFile][CompRank])].PrGraphic > 0 )
		 WhiteTally = WhiteTally + (Pieces[Pieces[abs(Squares[CompFile][CompRank])].PrGraphic].Value * 10);
	    else WhiteTally = WhiteTally + (Pieces[abs(Squares[CompFile][CompRank])].Value * 10);
	}
    }
    if(Drop == 1 && Level > 1 ) {
	if(!strcmp(Turn, "Black") && Squares[CompFile][CompRank] < 0 ) {
	    if(Pieces[abs(Squares[CompFile][CompRank])].Promotes == 0 && Pieces[abs(Squares[CompFile][CompRank])].PrGraphic > 0 )
		 KingTally[AA] = KingTally[AA] + (Pieces[Pieces[abs(Squares[CompFile][CompRank])].PrGraphic].Value * 10);
	    else KingTally[AA] = KingTally[AA] + (Pieces[abs(Squares[CompFile][CompRank])].Value * 10);
	}
	if(!strcmp(Turn, "White") && Squares[CompFile][CompRank] > 0 ) {
	    if(Pieces[abs(Squares[CompFile][CompRank])].Promotes == 0 && Pieces[abs(Squares[CompFile][CompRank])].PrGraphic > 0 )
		 KingTally[AA] = KingTally[AA] + (Pieces[Pieces[abs(Squares[CompFile][CompRank])].PrGraphic].Value * 10);
	    else KingTally[AA] = KingTally[AA] + (Pieces[abs(Squares[CompFile][CompRank])].Value * 10);
	}
    }
    if((TurnCount < 4 && BoardSizeY > 7) || TurnCount < 2 ) KingTally[AA] = KingTally[AA] + 4 - (Rnd() * 8);
    KingSafety();
    Evaluate = 0;
    FindInfluence();
    if(InitFile != 0 ) Squares[InitFile][InitRank] = 0;
    if(ExtraCapture[AA].Piece != 0 ) Squares[ExtraCapture[AA].File][ExtraCapture[AA].Rank] = 0;
    Squares[CompFile][CompRank] = CompLegal[AA].EndPiece;
    Evaluate = 1;
    for(PP = 1; PP <= Changed; PP++) {
	InitFile = Clearing[PP].File;
	InitRank = Clearing[PP].Rank;
	if(Squares[InitFile][InitRank] != 0 ) Validate();
    } // Next PP;
    Evaluate = 0;
    PawnMates();
    if(!strcmp(Turn, "Black") && BanMap[CompFile][CompRank].WhiteNum > 0 && BanMap[CompFile][CompRank].BlackNum == 0 && Comp[CompFile][CompRank] >= 0 ) BlackTally = BlackTally - 999;
    if(!strcmp(Turn, "White") && BanMap[CompFile][CompRank].BlackNum > 0 && BanMap[CompFile][CompRank].WhiteNum == 0 && Comp[CompFile][CompRank] <= 0 ) WhiteTally = WhiteTally - 999;

    for(DD = 1; DD <= BoardSizeY; DD++) {
	for(EE = 1; EE <= BoardSizeX; EE++) {
	    if(Squares[EE][DD] != 0 ) {
		if(Squares[EE][DD] == 1 || (Squares[EE][DD] > 1 && !strcmp(Pieces[abs(Squares[EE][DD])].Name, "Emperor")) || (Squares[EE][DD] > 0 && BlackKing == 1 && (!strcmp(Pieces[abs(Squares[EE][DD])].Name, "Crown Prince") || !strcmp(Pieces[abs(Squares[EE][DD])].Name, "Prince"))) ) BlackTally = BlackTally + 9999;
		if(Squares[EE][DD] == -1 || (Squares[EE][DD] < -1 && !strcmp(Pieces[abs(Squares[EE][DD])].Name, "Emperor")) || (Squares[EE][DD] < 0 && WhiteKing == 1 && (!strcmp(Pieces[abs(Squares[EE][DD])].Name, "Crown Prince") || !strcmp(Pieces[abs(Squares[EE][DD])].Name, "Prince"))) ) WhiteTally = WhiteTally + 9999;
	    }
	    if(ExtraCapture[AA].Piece == 0 || ExtraCapture[AA].File != EE || ExtraCapture[AA].Rank != DD ) {
		if(Squares[EE][DD] < 0 ) WhiteTally = WhiteTally + (Pieces[abs(Squares[EE][DD])].Value * 10);
		if(Squares[EE][DD] > 0 ) BlackTally = BlackTally + (Pieces[abs(Squares[EE][DD])].Value * 10);
	    }
	    if(!strcmp(Choice, "Tai") || !strcmp(Choice, "Maka") ) {
		if(!strcmp(Turn, "Black") ) {
		    if(Squares[EE][DD] > 0 ) {
			if(BlackEmperor == 0 && WhiteEmperor == 1 && BanMap[EE][DD].BlackNum == 0 ) BlackTally = BlackTally - (Pieces[abs(Squares[EE][DD])].Value * 10);
			if(!strcmp(Pieces[abs(Squares[EE][DD])].Name, "Emperor") && BanMap[EE][DD].WhiteNum > 0 ) BlackTally = BlackTally - (Pieces[abs(Squares[EE][DD])].Value * 20);
			if(!strcmp(Pieces[abs(Squares[EE][DD])].Name, "Emperor") && WhiteEmperor == 1 && (EE != WhiteEmpX || DD != WhiteEmpY) && BanMap[EE][DD].BlackNum == 0 ) BlackTally = BlackTally - (Pieces[abs(Squares[EE][DD])].Value * 20);
		    }
		} else {
		    if(Squares[EE][DD] < 0 ) {
			if(WhiteEmperor == 0 && BlackEmperor == 1 && BanMap[EE][DD].WhiteNum == 0 ) WhiteTally = WhiteTally - (Pieces[abs(Squares[EE][DD])].Value * 10);
			if(!strcmp(Pieces[abs(Squares[EE][DD])].Name, "Emperor") && BanMap[EE][DD].BlackNum > 0 ) WhiteTally = WhiteTally - (Pieces[abs(Squares[EE][DD])].Value * 20);
			if(!strcmp(Pieces[abs(Squares[EE][DD])].Name, "Emperor") && BlackEmperor == 1 && (EE != BlackEmpX || DD != BlackEmpY) && BanMap[EE][DD].WhiteNum == 0 ) WhiteTally = WhiteTally - (Pieces[abs(Squares[EE][DD])].Value * 20);
		    }
		}
	    }
	    if(BanMap[EE][DD].BlackNum == 0 && BanMap[EE][DD].WhiteNum > 0 ) {
		WhiteTally = 1 + abs((BoardSizeY / 2) + .5 - DD) / 2 + WhiteTally;
		if(Squares[EE][DD] < 0 ) WhiteTally = WhiteTally + 1;
		if(Squares[EE][DD] > 0 && !strcmp(Turn, "Black") ) BlackTally = BlackTally - (Pieces[abs(Squares[EE][DD])].Value * 10);
		if(Squares[EE][DD] == 1 && !strcmp(Turn, "Black") && Level > 1 ) KingTally[AA] = -99999;
		if(Squares[EE][DD] > 0 && !strcmp(Turn, "Black") && Level == 0 ) BlackTally = BlackTally + (Pieces[abs(Squares[EE][DD])].Value);
	    }
	    if(BanMap[EE][DD].WhiteNum == 0 && BanMap[EE][DD].BlackNum > 0 ) {
		BlackTally = 1 + abs((BoardSizeY / 2) + .5 - DD) / 2 + BlackTally;
		if(Squares[EE][DD] > 0 ) BlackTally = BlackTally + 1;
		if(Squares[EE][DD] == -1 && !strcmp(Turn, "White") && Level > 1 ) KingTally[AA] = -99999;
		if(Squares[EE][DD] < 0 && !strcmp(Turn, "White") ) WhiteTally = WhiteTally - (Pieces[abs(Squares[EE][DD])].Value * 10);
		if(Squares[EE][DD] < 0 && !strcmp(Turn, "White") && Level == 0 ) WhiteTally = WhiteTally + (Pieces[abs(Squares[EE][DD])].Value);
	    }
	    if(BanMap[EE][DD].WhiteNum > 0 && BanMap[EE][DD].BlackNum > 0 ) {
		if(Squares[EE][DD] > 0 && !strcmp(Turn, "Black") ) {
		    LionAttack = 0;
		    if((!strcmp(Choice, "Chu") || !strcmp(Choice, "Dai") || !strcmp(Choice, "DaiDai") || !strcmp(Choice, "Maka") || !strcmp(Choice, "Tenjiku") || !strcmp(Choice, "Tai")) ) {
			for(JJJ = DD - 1; JJJ <= DD + 1; JJJ++) {
			    for(KKK = EE - 1; KKK <= EE + 1; KKK++) {
				if(JJJ > 0 && JJJ < BoardSizeY && KKK > 0 && KKK < BoardSizeX ) {
				    if(Squares[KKK][JJJ] < 0 ) {
					if(Pieces[abs(Squares[KKK][JJJ])].special == 'L' ) LionAttack = 1;
				    }
				}
			    } // Next KKK;
			} // Next JJJ;
		    }
		    if(LionAttack == 1 ) {
			BlackTally = BlackTally - (Pieces[abs(Squares[EE][DD])].Value * 10);
			LionAttack = 0;
		    } else {
		      for(QQ = 1; QQ <= Attacker[EE][DD]; QQ++) {
			if(BanMap[EE][DD].Info[QQ].Piece != 0 ) {
			    if(BanMap[EE][DD].Info[QQ].Piece < 0 && (Pieces[abs(BanMap[EE][DD].Info[QQ].Piece)].Value < LowWhite[EE][DD] || LowWhite[EE][DD] == 0) ) {
				LowWhite[EE][DD] = Pieces[abs(BanMap[EE][DD].Info[QQ].Piece)].Value;
				if(Level < 2 ) LowWhite[EE][DD] = LowWhite[EE][DD] - 1;
			    }
			    if(BanMap[EE][DD].WhiteNum > 1 && BanMap[EE][DD].Info[QQ].Piece > 0 && Level > 0 ) {
				if(Pieces[abs(BanMap[EE][DD].Info[QQ].Piece)].Value < LowBlack[EE][DD] || LowBlack[EE][DD] == 0 ) LowBlack[EE][DD] = Pieces[abs(BanMap[EE][DD].Info[QQ].Piece)].Value;
			    }
			}
		      } // Next QQ;
		      BlackLoss = ((Pieces[abs(Squares[EE][DD])].Value + LowBlack[EE][DD] - LowWhite[EE][DD]) * 10);
// [HGM] The line below looks fishy
		      if(Squares[EE][DD] == 1 || !strcmp(Pieces[abs(Squares[EE][DD])].Name, "Emperor") || (BlackKing == 1 && !strcmp(Pieces[abs(Squares[EE][DD])].Name, "Crown Prince") || !strcmp(Pieces[abs(Squares[EE][DD])].Name, "Prince")) ) { BlackLoss = 29999; if(Level > 0 ) KingTally[AA] = -99999; }
		      if(BlackLoss > HighBlackLoss ) HighBlackLoss = BlackLoss;
		    }
		}
		if(Squares[EE][DD] < -1 && !strcmp(Turn, "Black") ) BlackTally = BlackTally + (Pieces[abs(Squares[EE][DD])].Value / 4);
		if(Squares[EE][DD] < 0 && !strcmp(Turn, "White") ) {
		    LionAttack = 0;
		    if((!strcmp(Choice, "Chu") || !strcmp(Choice, "Dai") || !strcmp(Choice, "DaiDai") || !strcmp(Choice, "Maka") || !strcmp(Choice, "Tenjiku") || !strcmp(Choice, "Tai")) ) {
			for(JJJ = DD - 1; JJJ <= DD + 1; JJJ++) {
			    for(KKK = EE - 1; KKK <= EE + 1; KKK++) {
				if(JJJ > 0 && JJJ < BoardSizeY && KKK > 0 && KKK < BoardSizeX ) {
				    if(Squares[KKK][JJJ] > 0 ) {
					if(Pieces[abs(Squares[KKK][JJJ])].special == 'L' ) LionAttack = 1;
				    }
				}
			    } // Next KKK;
			} // Next JJJ;
		    }
		    if(LionAttack == 1 ) {
			WhiteTally = WhiteTally - (Pieces[abs(Squares[EE][DD])].Value * 10);
			LionAttack = 0;
		    } else {
		      for(QQ = 1; QQ <= Attacker[EE][DD]; QQ++) {
			if(BanMap[EE][DD].Info[QQ].Piece != 0 ) {
			    if(BanMap[EE][DD].Info[QQ].Piece > 0 && (Pieces[abs(BanMap[EE][DD].Info[QQ].Piece)].Value < LowBlack[EE][DD] || LowBlack[EE][DD] == 0) ) {
				LowBlack[EE][DD] = Pieces[abs(BanMap[EE][DD].Info[QQ].Piece)].Value;
				if(Level < 2 ) LowWhite[EE][DD] = LowWhite[EE][DD] - 1;
			    }
			    if(BanMap[EE][DD].BlackNum > 1 && BanMap[EE][DD].Info[QQ].Piece < 0 && Level > 0 ) {
				if(Pieces[abs(BanMap[EE][DD].Info[QQ].Piece)].Value < LowWhite[EE][DD] || LowWhite[EE][DD] == 0 ) LowWhite[EE][DD] = Pieces[abs(BanMap[EE][DD].Info[QQ].Piece)].Value;
			    }
			}
		      } // Next QQ;
		      WhiteLoss = ((Pieces[abs(Squares[EE][DD])].Value + LowWhite[EE][DD] - LowBlack[EE][DD]) * 10);
// [HGM] Looks again fishy, as Prince and Crown Prince are not equivalent
		      if(Squares[EE][DD] == -1 || !strcmp(Pieces[abs(Squares[EE][DD])].Name, "Emperor") || (WhiteKing == 1 && !strcmp(Pieces[abs(Squares[EE][DD])].Name, "Crown Prince") || !strcmp(Pieces[abs(Squares[EE][DD])].Name, "Prince")) ) { WhiteLoss = 29999; if(Level > 0 ) KingTally[AA] = -99999; }
		      if(WhiteLoss > HighWhiteLoss ) HighWhiteLoss = WhiteLoss;
		    }
		}
		if(Squares[EE][DD] > 1 && !strcmp(Turn, "White") ) WhiteTally = WhiteTally + (Pieces[abs(Squares[EE][DD])].Value / 4);
	    }
	    File = EE; Rank = DD; CompMate();
	    LowWhite[EE][DD] = 0; LowBlack[EE][DD] = 0;
	} // Next EE;
    } // Next DD;
    if(Drop == 1 ) {
	for(FF = 1; FF <= Capture; FF++) {
	    BlackTally = BlackTally + (Pieces[abs(CapRef[FF])].Value * 10.5) * (InHand[FF]);
	    WhiteTally = WhiteTally + (Pieces[abs(CapRef[FF + Capture])].Value * 10.5) * (InHand[FF + Capture]);
	} // Next FF;
	if(CompLegal[AA].StartFile == 0 ) {
	    if(!strcmp(Turn, "White") ) {
		WhiteTally = WhiteTally - Pieces[abs(CompLegal[AA].StartPiece)].Value * 10.5;
	    } else {
		BlackTally = BlackTally - Pieces[abs(CompLegal[AA].StartPiece)].Value * 10.5;
	    }
	}
    }
    BlackTally = BlackTally - HighBlackLoss; HighBlackLoss = 0;
    WhiteTally = WhiteTally - HighWhiteLoss; HighWhiteLoss = 0;
    if(Level > 0 && Rnd() < .5 ) {
	if(!strcmp(Turn, "White") ) WhiteTally = WhiteTally + Int(Rnd() * 2) + 1; else BlackTally = BlackTally + Int(Rnd() * 2) + 1;
    }
    if(Level > 1 ) {
	if(!strcmp(Turn, "White") && (WhiteTally - BlackTally) + KingTally[AA] > BestTally[TestDepth] ) {
	    BestTally[Level + 1] = (WhiteTally - BlackTally) + KingTally[AA]; BestMove = AA; ReorderMoves();
	}
	if(!strcmp(Turn, "Black") && (BlackTally - WhiteTally) + KingTally[AA] > BestTally[TestDepth] ) {
	    BestTally[Level + 1] = (BlackTally - WhiteTally) + KingTally[AA]; BestMove = AA; ReorderMoves();
	}
    } else {
	if(Level == 1 ) {
	    if(!strcmp(Turn, "White") && WhiteTally - BlackTally + KingTally[AA] > BestTally[1] ) {
		BestTally[1] = WhiteTally - BlackTally + KingTally[AA]; BestMove = AA;
		if((!strcmp(Choice, "Chu") || !strcmp(Choice, "Dai") || !strcmp(Choice, "DaiDai") || !strcmp(Choice, "Maka") || !strcmp(Choice, "Tenjiku") || !strcmp(Choice, "Tai")) && ExtraCapture[AA].Piece != 0 ) {
		    LionVictim.Piece = ExtraCapture[AA].Piece;
		    LionVictim.File = ExtraCapture[AA].File;
		    LionVictim.Rank = ExtraCapture[AA].Rank;
		} else {
		    LionVictim.Piece = 0;
		}
	    }
	    if(!strcmp(Turn, "Black") && BlackTally - WhiteTally + KingTally[AA] > BestTally[1] ) {
		BestTally[1] = BlackTally - WhiteTally + KingTally[AA]; BestMove = AA;
		if((!strcmp(Choice, "Chu") || !strcmp(Choice, "Dai") || !strcmp(Choice, "DaiDai") || !strcmp(Choice, "Maka") || !strcmp(Choice, "Tenjiku") || !strcmp(Choice, "Tai")) && ExtraCapture[AA].Piece != 0 ) {
		    LionVictim.Piece = ExtraCapture[AA].Piece;
		    LionVictim.File = ExtraCapture[AA].File;
		    LionVictim.Rank = ExtraCapture[AA].Rank;
		} else {
		    LionVictim.Piece = 0;
		}
	    }
	} else {
	    if(!strcmp(Turn, "White") && WhiteTally - BlackTally + KingTally[AA] > BestTally[0] ) {
		BestTally[0] = WhiteTally - BlackTally + KingTally[AA];
	    }
	    if(!strcmp(Turn, "Black") && BlackTally - WhiteTally + KingTally[AA] > BestTally[0] ) {
		BestTally[0] = BlackTally - WhiteTally + KingTally[AA];
	    }
	}
    }
    RestoreMap();
    ExtraCapture[AA].Piece = 0;
  } // Next AA;

}

void Convert ()
{
  INT K, AB, CD, Strip, NoStrip;
  STRING LeftBit, RightBit; // were not declared, and thus integer in basic source???
  ClearLegal();
  DropTest = 0; Strip = 0; NoStrip = 0;
  I = TurnCount + 1;
  strcpy(ShortScore[I], CMoveSTR);
  K = strlen(ShortScore[I]);
  if(strstr(ShortScore[I], "*") == NULL || ShortScore[I][K-1] == '*' ) {
    for(AB = 1; AB <= BoardSizeY; AB++) {
	for(CD = 1; CD <= BoardSizeX; CD++) {
	    if(Squares[CD][AB] == Score[TurnCount].IDStart && Score[TurnCount].IDStart != 0 && (File != CD || Rank != AB) ) {
		if(!strcmp(Pieces[abs(Squares[CD][AB])].sname, Pieces[abs(Score[TurnCount].IDStart)].sname) ) {
		    FirstFile = CD; FirstRank = AB;
		    TestOther();
		}
	    }
	} // Next CD;
    } // Next AB;
    if(Testing123 != 1 ) {
    for(J = 1; J <= K; J++) {
	if(ShortScore[I][J-1] > 47 && ShortScore[I][J-1] < 58 && NoStrip == 0 && Strip == 0 ) {
	  Strip = 1;
	  strcpy(LeftBit, ShortScore[I]); LeftBit[J-1] = 0;
	}
	if(Strip == 1 && ShortScore[I][J-1] > 96 && ShortScore[I][J-1] < 123 ) {
	NoStrip = 1; Strip = 0;
	strcpy(RightBit, ShortScore[I]+J);
	}
    } // Next J;
    sprintf(ShortScore[I], "%s%s", LeftBit, RightBit);
    }
    Testing123 = 0;
  }
}

void ConvertScore ()
{
  INT K, Strip, NoStrip, OldScore;
  STRING LeftBit, RightBit; // not declared in basic, thus integer???
  STRING TmpSTR;
  for(I = 1; I <= TurnCount; I++) {
    Strip = 0; NoStrip = 0;
    strcpy(ShortScore[I], Score[I].Caption);
    for(J = 1; J <= strlen(ShortScore[I]); J++) {
      if(ShortScore[I][J-1] == ' ' ) { strcpy(TmpSTR, ShortScore[I]+J); strcpy(ShortScore[I], TmpSTR); OldScore = 1; }
    } // Next J;
    if(OldScore == 1 ) {
	K = strlen(ShortScore[I]);
	if(strstr(ShortScore[I], "*") == NULL || ShortScore[I][K-1] == '*' ) {
	    for(J = 1; J <= K; J++) {
		if(ShortScore[I][J-1] > 47 && ShortScore[I][J-1] < 58 && NoStrip == 0 && Strip == 0 ) {
		    Strip = 1;
		    strcpy(LeftBit, ShortScore[I]); LeftBit[J-1] = 0;
		}
		if(Strip == 1 && ShortScore[I][J-1] > 96 && ShortScore[I][J-1] < 123 ) {
		    NoStrip = 1; Strip = 0;
		    strcpy(RightBit, ShortScore[I]+J);
		}
	    } // Next J;
	    sprintf(ShortScore[I], "%s%s", LeftBit, RightBit);
	}
    }
    OldScore = 0;
    strcpy(Score[I].Caption, ShortScore[I]);
  } // Next I;
}

void DiagramDrop ()
{
  INT K, L, Z;
  STRING TmpSTR;
  strcpy(BlackDrop[1], "|"); strcpy(BlackDrop[2], "|"); Z = 1;
  if(Reverse == 0 ) {
    J = 1; K = Capture;
    L = K + 1; M = K * 2;
  } else {
    L = 1; M = Capture;
    J = M + 1; K = M * 2;
  }
  for(I = J; I <= K; I++) {
    if(InHand[I] > 0 ) {
// FIXME is this correct? why trim?
//	BlackDrop[Z] = BlackDrop[Z] + " " + Left(Pieces[abs(CapRef[I])].sname, Len(Trim$(Pieces[abs(CapRef[I])].sname)));
	sprintf(TmpSTR, " %s", Pieces[abs(CapRef[I])].sname);
	strcat(BlackDrop[Z], TmpSTR);
	if(InHand[I] > 1 ) { strcpy(TmpSTR, BlackDrop[Z]); sprintf(BlackDrop[Z], "%sx%d", TmpSTR, InHand[I]); }
	if(strlen(BlackDrop[Z]) > 12 ) Z = 2;
    }
  } // Next I;
  strcpy(WhiteDrop[2], "|"); strcpy(WhiteDrop[1], "|"); Z = 1;
  for(I = L; I <= M; I++) {
    if(InHand[I] > 0 ) {
//	WhiteDrop(Z) = WhiteDrop(Z) + " " + Left(Pieces[abs(CapRef[I])].sname, Len(Trim$(Pieces[abs(CapRef[I])].sname)))
	sprintf(TmpSTR, " %s", Pieces[abs(CapRef[I])].sname);
        strcat(WhiteDrop[Z], TmpSTR);
	if(InHand[I] > 1 ) { strcpy(TmpSTR, WhiteDrop[Z]); sprintf(WhiteDrop[Z], "%sx%d", TmpSTR, InHand[I]); }
	if(strlen(WhiteDrop[Z]) > 13 ) Z = 2;
    }
  } // Next I;
  if(!strcmp(WhiteDrop[1], "|") ) strcat(WhiteDrop[1], " Nothing");
  if(!strcmp(BlackDrop[1], "|") ) strcat(BlackDrop[1], " Nothing");
  for(I = 1; I <= 2; I++) {
    strcpy(TmpSTR, WhiteDrop[I]); sprintf(WhiteDrop[I], "%-17s|", TmpSTR);
    strcpy(TmpSTR, BlackDrop[I]); sprintf(BlackDrop[I], "%-17s|", TmpSTR);
  } // Next I;
}

void DiagramFile ()
{ // Menu function for large diagrams.
  FILE *f3;
  STRING TmpSTR;
  INT Short, DropStart, DropEnd;
// TODO  On Error Resume Next;
  strcpy(Board.CMDiagram.DialogTitle, "Create Text Diagram");
  Board.CMDiagram.Flags = 0x400L | 0x800L | 0x4L;
  Board.CMDiagram.Action = 2;
  if(Err == 32755 ) return;
  strcpy(SavedSTR, Board.CMDiagram.Filename);

  if( !(f3 = fopen(SavedSTR, "w")) ) return;

  if(!strcpy(GameName, "Whale") || !strcpy(GameName, "Early") || !strcpy(GameName, "Bird") || !strcpy(GameName, "Micro") || !strcpy(GameName, "Mini") || !strcpy(GameName, "") ) Short = 1; else Short = 0;
  if(Drop == 1 ) DiagramDrop();
  DropStart = 0; DropEnd = 0;
  if(Drop == 1 ) {
    DropStart = 1;
    DropEnd = BoardSizeY - 1;
  }
  if(strcpy(GameName, "") ) {
    fprintf(f3, "%s Shogi\n", GameName);
    for(I = 1; I <= strlen(GameName) + 6; I++) {
	fprintf(f3, "=");
    } // Next I;
  }
//  Print #3,
  fprintf(f3, "\n");
  fprintf(f3, " ");
  for(I = BoardSizeX; I >= 1; I--) {
    if(I < 10 ) fprintf(f3, " ");
    if(Short == 0 ) fprintf(f3, " ");
    fprintf(f3, "% d  ", I);
  } // Next I;
  fprintf(f3, "\n");
  for(J = 1; J <= BoardSizeY; J++) {
    fprintf(f3, "+");
    for(I = 1; I <= BoardSizeX - 1; I++) {
	if(Short == 0 ) fprintf(f3, "------"); else fprintf(f3, "-----");
    } // Next I;
    if(Short == 0 ) fprintf(f3, "-----+"); else fprintf(f3, "----+");
    if(Drop == 1 ) {
	if(J == DropStart ) fprintf(f3, "      White in hand:");
	if(J == DropStart + 1 ) fprintf(f3, "      %s", WhiteDrop[1]);
	if(J == DropEnd + 1 ) fprintf(f3, "      %s", BlackDrop[1]);
	if(J == DropEnd ) fprintf(f3, "      Black in hand:");
	if(J == DropStart + 2 ) fprintf(f3, "      +----------------+");
    }
    fprintf(f3, "\n");
    for(I = 1; I <= BoardSizeX; I++) {
	fprintf(f3, "|");
	if(Squares[I][J] != 0 ) {
	    if(Squares[I][J] > 0 ) fprintf(f3, " b"); else fprintf(f3, " w");
// FIXME?   Print #3, Left$(Pieces[abs(Squares[I][J])].sname, 3 - Short);
	    strcpy(TmpSTR, Pieces[abs(Squares[I][J])].sname); TmpSTR[3 - Short] = 0;
	    if(Short) fprintf(f3, "%-2s", TmpSTR); else fprintf(f3, "%-3s", TmpSTR);
	} else {
	    if(Short == 0 ) fprintf(f3, "     "); else fprintf(f3, "    ");
	}
    } // Next I;
    fprintf(f3, "| %c", (96 + J));
    if(J == DropStart || J == DropEnd ) fprintf(f3, "    +----------------+");
    if(J == DropStart + 1 ) fprintf(f3, "    %s", WhiteDrop[2]);
    if(J == DropEnd + 1 ) fprintf(f3, "    %s", BlackDrop[2]);
    fprintf(f3, "\n");
  } // Next J;
  fprintf(f3, "+");
  for(I = 1; I <= BoardSizeX - 1; I++) {
    if(Short == 0 ) fprintf(f3, "------"); else fprintf(f3, "-----");
  } // Next I;
  if(Short == 0 ) fprintf(f3, "-----+"); else fprintf(f3, "----+");
  if(DropEnd > 0 ) fprintf(f3, "      +----------------+");
  fprintf(f3, "\n");
  fprintf(f3, "\n");
  fprintf(f3, "%s to move.\n", Turn);
  sprintf(Board.Caption, "Diagram text file %s created.", Board.CMDiagram.Filename); Notice = 1;
  fclose(f3);
}

void DiagramSmall ()
{ // Menu function for small diagrams.
  FILE *f3;
  STRING TmpSTR;
  INT Short, DropStart, DropEnd;
// TODO  On Error Resume Next;
  strcpy(Board.CMDiagram.DialogTitle, "Create Small Text Diagram");
  Board.CMDiagram.Flags = 0x400L | 0x800L | 0x4;
  Board.CMDiagram.Action = 2;
  if(Err == 32755 ) return;
  strcpy(SavedSTR, Board.CMDiagram.Filename);

  if( !(f3 = fopen(SavedSTR, "w")) ) return;

  if(!strcmp(GameName, "Whale") || !strcmp(GameName, "Early") || !strcmp(GameName, "Bird") || !strcmp(GameName, "Micro") || !strcmp(GameName, "Mini") || !strcmp(GameName, "") ) Short = 1; else Short = 0;
  if(Drop == 1 ) DiagramDrop();
  DropStart = 0; DropEnd = 0;
  if(Drop == 1 ) {
    DropStart = 1;
    DropEnd = BoardSizeY - 1;
  }
  if(strcmp(GameName, "") ) {
    fprintf(f3, "%s Shogi", GameName);
    for(I = 1; I <= strlen(GameName) + 6; I++) {
	fprintf(f3, "=");
    } // Next I;
  }
  fprintf(f3, "\n");
  for(I = BoardSizeX; I >= 1; I--) {
    if(I < 10 ) fprintf(f3, " ");
    if(Short == 0 ) fprintf(f3, " ");
    fprintf(f3, "%d", I);
    fprintf(f3, " ");
  } // Next I;
  fprintf(f3, "\n");
  fprintf(f3, "+");
  for(I = 1; I <= BoardSizeX - 1; I++) {
    if(Short == 0 ) fprintf(f3, "----"); else fprintf(f3, "---");
  } // Next I;
  if(Short == 0 ) fprintf(f3, "----+"); else fprintf(f3, "---+");
  if(DropStart == 1 ) fprintf(f3, "    White in hand:");
  fprintf(f3, "\n");
  for(J = 1; J <= BoardSizeY; J++) {
    fprintf(f3, "|");
    for(I = 1; I <= BoardSizeX; I++) {
	if(Squares[I][J] != 0 ) {
	    if(Squares[I][J] > 0 ) fprintf(f3, "b"); else fprintf(f3, "w");
	    strcpy(TmpSTR, Pieces[abs(Squares[I][J])].sname); TmpSTR[3 - Short] = 0;
	    if(Short) fprintf(f3, "%-2s", TmpSTR); else  fprintf(f3, "%-3s", TmpSTR);
	} else {
	    if(Short == 0 ) fprintf(f3, " *  "); else fprintf(f3, " * ");
	}
    } // Next I;
    fprintf(f3, "|%c", (96 + J));
    if(J == DropStart ) fprintf(f3, "  %s", Mid(WhiteDrop[1], 2, 16));
    if(J == DropStart + 1 ) fprintf(f3, "  %s", Mid(WhiteDrop[2], 2, 16));
    if(J == DropEnd + 1 ) fprintf(f3, "  %s", Mid(BlackDrop[1], 2, 16));
    if(J == DropEnd ) fprintf(f3, "   Black in hand:");
    fprintf(f3, "\n");
  } // Next J;
  fprintf(f3, "+");
  for(I = 1; I <= BoardSizeX - 1; I++) {
    if(Short == 0 ) fprintf(f3, "----"); else fprintf(f3, "---");
  } // Next I;
  if(Short == 0 ) fprintf(f3, "----+"); else fprintf(f3, "---+");
  if(DropEnd > 0 ) fprintf(f3, "    %s", Mid(BlackDrop[2], 2, 16));
  fprintf(f3, "\n");
  fprintf(f3, "%s to move.\n", Turn);
  sprintf(Board.Caption, "Diagram text file %s created.", Board.CMDiagram.Filename); Notice = 1;
  fclose(f3);
}

void DisplayEmperor ()
{
  INT K, L, EF;
  for(K = 1; K <= BoardSizeY; K++) {
    for(J = 1; J <= BoardSizeX; J++) {
	Board.ForeColor = 0xFF8080;
	Legal[J][K] = 1; L = 0;
	if(!strcmp(Turn, "Black") && Squares[J][K] < 0 && ((Camps[J][K] == 1 || Camps[J][K] == 3) || WhiteEmperor == 1) ) Legal[J][K] = 0;
	if(!strcmp(Turn, "White") && Squares[J][K] > 0 && (Camps[J][K] > 1 || BlackEmperor == 1) ) Legal[J][K] = 0;
	if(!strcmp(Turn, "Black") && Squares[J][K] < 0 && Legal[J][K] == 1 ) Board.ForeColor = 0xC0;
	if(!strcmp(Turn, "White") && Squares[J][K] > 0 && Legal[J][K] == 1 ) Board.ForeColor = 0xC0;
	if(FirstInitRank == K && FirstInitFile == J ) Legal[J][K] = 0;
	if(Squares[J][K] != 0 ) {
	    if(!strcmp(Pieces[abs(Squares[J][K])].Name, "Emperor") && Sgn(Squares[J][K]) != Sgn(Squares[FirstInitFile][FirstInitRank]) ) {
		NewRank = K; NewFile = J; Legal[J][K] = 1; CheckEmperor();
		K = NewRank; J = NewFile;
		if(Legal[J][K] == 1 ) Board.ForeColor = 0xC0;
	    }
	}
	if(SeeMove == 1 && Legal[J][K] == 1 ) {
	    if(Squares[J][K] == 0 ) {
		if(Reverse == 0 ) {
		    Board.Line(XStart + (J - 1) * Pixels, 11 + (K - 1) * Pixels, XStart + (J * Pixels) - 2, 9 + (K * Pixels), NOCOLOR, "BF");
		} else {
		    Board.Line(XStart + (BoardSizeX - J) * Pixels, 11 + (BoardSizeY - K) * Pixels, XStart - 2 + (BoardSizeX - J + 1) * Pixels, 9 + (BoardSizeY - K + 1) * Pixels + L, NOCOLOR, "BF");
		}
	    } else {
		for(EF = 1; EF < Pixels; EF++) { // FIXME: lowered EF by one to access correct PieceMask elements
		    if(Reverse == 0 ) {
			if(Squares[J][K] > 0 ) {
			   Board.Line(XStart + (J - 1) * Pixels + EF - 2, 11 + (K - 1) * Pixels, XStart + (J - 1) * Pixels + EF - 2, 11 + (K - 1) * Pixels + PieceMask[Pieces[abs(Squares[J][K])].Mask][EF] + 1, NOCOLOR, "");
			} else {
			   Board.Line(XStart + (J - 1) * Pixels + (Pixels - EF), 9 + (K * Pixels), XStart + (J - 1) * Pixels + (Pixels - EF), 8 + (K * Pixels) - PieceMask[Pieces[abs(Squares[J][K])].Mask][EF], NOCOLOR, "");
			}
		    } else {
			if(Squares[J][K] > 0 ) {
			   Board.Line(XStart + (BoardSizeX - J) * Pixels + (Pixels - EF), 8 + (BoardSizeY - K + 1) * Pixels + 1, XStart + (BoardSizeX - J) * Pixels + (Pixels - EF), 8 + (BoardSizeY - K + 1) * Pixels - PieceMask[Pieces[abs(Squares[J][K])].Mask][EF], NOCOLOR, "");
			} else {
			   Board.Line(XStart + (BoardSizeX - J) * Pixels + EF - 2, 10 + (BoardSizeY - K) * Pixels + 1, XStart + (BoardSizeX - J) * Pixels + EF - 2, 12 + (BoardSizeY - K) * Pixels + PieceMask[Pieces[abs(Squares[J][K])].Mask][EF], NOCOLOR, "");
			}
		    }
		} // Next EF;
	    }
	}
	Camps[J][K] = 0;
    } // Next J;
  } // Next K;
  Influence = 0;
  Board.ForeColor = 0x0L;
  Board.FillColor = 0xFFFFFF;
}

void DisplayTerritory ()
{
  INT K, L, EF;
  for(K = 1; K <= BoardSizeY; K++) {
    for(J = 1; J <= BoardSizeX; J++) {
	L = 0;
	if(!strcmp(Choice, "Heian") || !strcmp(Choice, "Chu") || !strcmp(Choice, "Dai") || !strcmp(Choice, "Tenjiku") ) L = 1;
	switch( Camps[J][K] ) {
	    case 1: Board.ForeColor = 0xFFFFFF; break;
	    case 2: Board.ForeColor = 0x606060; break;
	    case 3: Board.ForeColor = 0xC0C0C0; break;
	}
	if(Camps[J][K] > 0 ) {
	    if(Squares[J][K] == 0 ) {
		if(Reverse == 0 ) {
		    Board.Line(XStart + L + (J - 1) * Pixels, 11 + L + (K - 1) * Pixels, XStart + (J * Pixels) - 2 + L, 9 + L + (K * Pixels), NOCOLOR, "BF");
		} else {
		    Board.Line(XStart + L + (BoardSizeX - J) * Pixels, 11 + (BoardSizeY - K) * Pixels + L, XStart - 2 + L + (BoardSizeX - J + 1) * Pixels, 9 + (BoardSizeY - K + 1) * Pixels + L, NOCOLOR, "BF");
		}
	    } else {
		if(Squares[J][K] > 0 && Camps[J][K] == 1 ) Board.ForeColor = 0xFF8080;
		if(Squares[J][K] < 0 && Camps[J][K] == 2 ) Board.ForeColor = 0xFF8080;
		if(Squares[J][K] < 0 ) {
		    if((Squares[J][K] == -1 || !strcmp(Pieces[abs(Squares[J][K])].Name, "Crown Prince") || !strcmp(Pieces[abs(Squares[J][K])].Name, "Prince")) && Camps[J][K] > 1 ) Board.ForeColor = 0xC0L;
		} else {
		    if((Squares[J][K] == 1 || !strcmp(Pieces[abs(Squares[J][K])].Name, "Crown Prince") || !strcmp(Pieces[abs(Squares[J][K])].Name, "Prince")) && (Camps[J][K] == 1 || Camps[J][K] == 3) ) Board.ForeColor = 0xC0L;
		}
		for(EF = 1; EF < Pixels; EF++) { // FIXME: lowered EF by 1 to access correct PieceMask elements ???
		    if(Reverse == 0 ) {
			if(Squares[J][K] > 0 ) {
			   Board.Line(XStart + (J - 1) * Pixels + EF /*- 2*/ + L, 11 + (K - 1) * Pixels + L, XStart + (J - 1) * Pixels + EF /*- 2*/ + L, 11 + (K - 1) * Pixels + PieceMask[Pieces[abs(Squares[J][K])].Mask][EF] + 1, NOCOLOR, ""); // FIXME: added 2 to x
			} else {
			   Board.Line(XStart + (J - 1) * Pixels + (Pixels - EF) + L, 9 + (K * Pixels) + L + 1, XStart + (J - 1) * Pixels + (Pixels - EF) + L, 8 + K * Pixels - (PieceMask[Pieces[abs(Squares[J][K])].Mask][EF] - (L * 2)) + 1 + YStart, NOCOLOR, ""); // FIXME: added 1 to y
			}
		    } else {
			if(Squares[J][K] > 0 ) {
			   Board.Line(XStart + (BoardSizeX - J) * Pixels + (Pixels - EF) + L, 8 + (BoardSizeY - K + 1) * Pixels + 1 + L + 1, XStart + (BoardSizeX - J) * Pixels + (Pixels - EF) + L, 8 + (BoardSizeY - K + 1) * Pixels - PieceMask[Pieces[abs(Squares[J][K])].Mask][EF] + (L * 2) + 1, NOCOLOR, ""); // FIXME: added 1 to y
			} else {
			   Board.Line(XStart + (BoardSizeX - J) * Pixels + EF /*- 2*/ + L, 10 + (BoardSizeY - K) * Pixels + 1 + L, XStart + (BoardSizeX - J) * Pixels + EF /*- 2*/ + L, 12 + (BoardSizeY - K) * Pixels + PieceMask[Pieces[abs(Squares[J][K])].Mask][EF], NOCOLOR, ""); // FIXME: added 2 to x
			}
		    }
		} // Next EF;
	    }
	}
	Camps[J][K] = 0;
    } // Next J;
  } // Next K;
  Board.ForeColor = 0x0L;
  Board.FillColor = 0xFFFFFF;
}

void DolphinMove ()
{
  if((Squares[InitFile][InitRank] > 0 && InitRank == 1) || (Squares[InitFile][InitRank] < 0 && InitRank == 6) ) {
    Pieces[abs(Squares[InitFile][InitRank])].Moves[7] = 64;
    Pieces[abs(Squares[InitFile][InitRank])].Moves[8] = 64;
  }
}

void DoubleMove ()
{
  ClearLegal();
  strcpy(LionNameSTR, Pieces[abs(Squares[File][Rank])].Name);
  sprintf(Board.Caption, "Extra %s move - Double Click on Piece to End Move.", LionNameSTR);
  LionPiece = I;
  Teach = Teach - 1;
  if(Reverse == 0 ) {
    MOVE(1, Board.Frame, XStart + (File - 1) * Pixels, 11 + (Rank - 1) * Pixels);
  } else {
    MOVE(1, Board.Frame, XStart + (BoardSizeX - File) * Pixels, 11 + (BoardSizeY - Rank) * Pixels);
  }
  Board.Frame.Visible = True;
}

void DropPiece ()
{ // MouseUp handler for board piece. I = piece number, NewButton = button
  if(NewButton == 1 ) {
    strcpy(Board.PieceID.Caption, "");
    DRAG(Board.showpic[I], 0);
  } else {
    strcpy(Board.PieceID.Caption, "");
    Board.PieceID.ForeColor = 0xFF0000;
    RightClick = 0;
  }
  if(LegalMoves == 0 ) ClearLegal();
}

void DropPiece2 ()
{ // MouseUp handler for holdings pieces. I = (holdings) piece number, NewButton = button
  if(NewButton == 1 ) {
    strcpy(Board.PieceID.Caption, "");
    DRAG(Board.HandPic[I], 0);
    if(LegalMoves == 0 ) ClearLegal();
  } else {
    strcpy(Board.PieceID.Caption, "");
    Board.PieceID.ForeColor = 0xFF0000;
    if(LegalMoves == 0 ) ClearLegal();
  }
}

void Emperor ()
{
  INT AB, CD;
  FirstInitFile = InitFile; FirstInitRank = InitRank; EmperorTest = 1;
  Influence = 2;
  for(AB = 1; AB <= BoardSizeY; AB++) {
    for(CD = 1; CD <= BoardSizeX; CD++) {
	if((!strcmp(Turn, "Black") && Squares[CD][AB] < 0) || (!strcmp(Turn, "White") && Squares[CD][AB] > 0) ) {
	    InitFile = CD; InitRank = AB;
	    Validate();
	}
    } // Next CD;
  } // Next AB;
  DisplayEmperor();
  Influence = 0; EmperorTest = 0;
  InitRank = FirstInitRank; InitFile = FirstInitFile;
}

void EmperorCheck ()
{
  INT AB, CD, OldInfluence, OldCompMove;
  int Response; // write-only?
  OldInfluence = Influence; OldCompMove = CompMove; OldSeeMove = SeeMove; SeeMove = 1;
  CompMove = 0; Influence = 2;
  for(AB = 1; AB <= BoardSizeY; AB++) {
   for(CD = 1; CD <= BoardSizeX; CD++) {
       InitFile = CD; InitRank = AB; Validate();
   } // Next CD;
  } // Next AB;
  if(Camps[BlackEmpX][BlackEmpY] == 0 ) {
    if(!strcmp(Turn, "White") ) {
	Response = MsgBox("You have left your Emperor in Check! ", 0, "Black");
    } else {
	strcpy(Board.PieceID.Caption, "Black Emperor is in Check!"); Notice = 1;
    }
  } else {
    if(Camps[WhiteEmpX][WhiteEmpY] = 0 ) {
	if(!strcmp(Turn, "Black") ) {
	    Response = MsgBox("You have left your Emperor in Check! ", 0, "White");
	} else {
	    strcpy(Board.PieceID.Caption, "White Emperor is in Check!"); Notice = 1;
	}
    }
  }
  for(AB = 1; AB <= BoardSizeY; AB++) {
    for(CD = 1; CD <= BoardSizeX; CD++) {
	Camps[CD][AB] = 0;
    } // Next CD;
  } // Next AB;
  Influence = OldInfluence; CompMove = OldCompMove; SeeMove = OldSeeMove;
}

void EmperorInfluence ()
{
  INT EF;
  if(Squares[InitFile][InitRank] > 0 ) {
     Board.ForeColor = 0x606060;
  } else {
     Board.ForeColor = 0xFFFFFF;
  }
  for(EF = 1; EF < Pixels; EF++) {
    if(Reverse == 0 ) {
	if(Squares[InitFile][InitRank] != 0 ) {
	    if(Squares[InitFile][InitRank] > 0 ) {
		Board.Line(XStart + (InitFile - 1) * Pixels + EF /*- 2*/, 11 + (InitRank - 1) * Pixels, XStart + (InitFile - 1) * Pixels + EF /*- 2*/, 11 + (InitRank - 1) * Pixels + PieceMask[Pieces[abs(Squares[InitFile][InitRank])].Mask][EF] + 1, NOCOLOR, ""); // FIXME: added 2 to x
	    } else {
		Board.Line(XStart + (InitFile - 1) * Pixels + (Pixels - EF), 9 + InitRank * Pixels + 1, XStart + (InitFile - 1) * Pixels + (Pixels - EF), 8 + InitRank * Pixels - PieceMask[Pieces[abs(Squares[InitFile][InitRank])].Mask][EF] + 1, NOCOLOR, ""); // FIXME: added 1 to y
	    }
	}
    } else {
	if(Squares[InitFile][InitRank] != 0 ) {
	    if(Squares[InitFile][InitRank] > 0 ) {
		Board.Line(XStart + (BoardSizeX - InitFile) * Pixels + (Pixels - EF), 8 + (BoardSizeY - InitRank + 1) * Pixels + 2, XStart + (BoardSizeX - InitFile) * Pixels + (Pixels - EF), 8 + (BoardSizeY - InitRank + 1) * Pixels - PieceMask[Pieces[abs(Squares[InitFile][InitRank])].Mask][EF] + 1, NOCOLOR, ""); // FIXME: added 1 to y2
	    } else {
		Board.Line(XStart + (BoardSizeX - InitFile) * Pixels + EF /*- 2*/, 10 + (BoardSizeY - InitRank) * Pixels + 1, XStart + (BoardSizeX - InitFile) * Pixels + EF /*- 2*/, 12 + (BoardSizeY - InitRank) * Pixels + PieceMask[Pieces[abs(Squares[InitFile][InitRank])].Mask][EF], NOCOLOR, ""); // FIXME: added 2 to x
	    }
	}
    }
  } // Next EF;
  Board.ForeColor = 0x0L;
  Board.FillColor = 0xFFFFFF;
}

void EndLionMove ()
{
  STRING PieceName;
  INT K, L, Found;
  if(LionPiece == I ) {
    K = 1;
    do {
	L = 1;
	do {
	    if(Grafix[L][K] == I ) {
		strcpy(PieceName, Pieces[abs(Squares[L][K])].Name);
		strcpy(Board.PieceID.Caption, PieceName);
		File = L;
		Rank = K;
		Found = 1;
	    }
	    L = L + 1;
	} while( !( L > BoardSizeX || Found == 1) );
	K = K + 1;
    } while( !(K > BoardSizeY || Found == 1) );
    Found = 0;
    Board.Frame.Visible = False;
    LionPiece = -1;
    LionBurn();
    NextTurn();
  } else {
    UnPromote();
  }
}

void EndSelect ()
{
  if(MovePiece == 1 ) {
    strcpy(Board.Caption, Cap);
    MovePiece = 0; Handicap = 0; Reduce = 0;
    Board.Timer1.Enabled = True;
    MoveCount = 0; TurnCount = 0;
    if(!strcmp(Turn, "White") ) MoveCount = 1;
    strcpy(Board.LastMove.Caption, "");
  } else {
    Handicap = 0; Reduce = 0; MovePiece = 0;
    if(Drop == 1 ) ResetHand();
    if(!strcmp(Turn, "White") ) MoveCount = 1;
    LoadAddPieces();
  }
  BugFix();
}

void EndSetup ()
{
  MovePiece = 0; Selection = 0; Reduce = 0; Handicap = 0; strcpy(ExtraPiece, "");
}

void EndTilde ()
{ // called when MouseDown on board or holdings piece
  if(Tilde == 1 ) {
    ClearLegal();
    Board.Refresh();
    Tilde = 0;
  }
}

void FillSquare ()
{ // Button1-Down handler for Form. (NewX, NewY) is coords, CCC = 1 and CleckPiece = 0;
  if(strcmp(Computer, Turn) && strcmp(Computer, "Both") && Level != 0 && GameOver != 1 ) {
   if(Selection != 0 ) {
    if(NewX > XStart && NewX < XStart + (BoardSizeX * Pixels) && NewY > 11 && NewY < 11 + (BoardSizeY * Pixels) ) {
	GetSquare();
	Squares[InitFile][InitRank] = Selection;
	NewGraf = 0;
	while( Board.showpic[NewGraf].Visible == True) {
	    NewGraf = NewGraf + 1;
	}
	SetGrafix();
    }
    Reduce = 0; Handicap = 0; MovePiece = 0;
    CheckAdd();
   } else {
    Territory();
   }
  }
}

void FindEmperorMove ()
{
  INT K, JStart, KStart, JEnd, KEnd, Steps, VacantX, VacantY, CaptureX, CaptureY, Vacant, BestCapture;
  BestCapture = 0; Vacant = 0;
  if(!strcmp(Turn, "Black") ) {
    KStart = BoardSizeY; JStart = BoardSizeX; KEnd = 1; JEnd = 1; Steps = -1;
  } else {
    KStart = 1; JStart = 1; KEnd = BoardSizeY; JEnd = BoardSizeX; Steps = 1;
  }
  for(K = KStart; Steps > 0 ? K <= KEnd : K >= KEnd; K += Steps) {
    for(J = JStart; Steps > 0 ? J <= JEnd : J >= JEnd; J += Steps) {
	if(!strcmp(Turn, "Black") && Squares[J][K] < 0 && Camps[J][K] != 1 && Camps[J][K] != 3 && WhiteEmperor != 1 ) {
	   if(Pieces[abs(Squares[J][K])].Value > BestCapture ) {
	       BestCapture = Pieces[abs(Squares[J][K])].Value; CaptureX = J; CaptureY = K;
	   }
	}
	if(!strcmp(Turn, "White") && Squares[J][K] > 0 && Camps[J][K] < 2 && BlackEmperor != 1 ) {
	   if(Pieces[abs(Squares[J][K])].Value > BestCapture ) {
	       BestCapture = Pieces[abs(Squares[J][K])].Value; CaptureX = J; CaptureY = K;
	   }
	}
	if(!strcmp(Turn, "Black") && Squares[J][K] == 0 && Camps[J][K] != 1 && Camps[J][K] != 3 && WhiteEmperor != 1 && Vacant != 1 ) {
	    VacantX = J; VacantY = K; Vacant = 1;
	}
	if(!strcmp(Turn, "White") && Squares[J][K] == 0 && Camps[J][K] < 2 && BlackEmperor != 1 && Vacant != 1 ) {
	    VacantX = J; VacantY = K; Vacant = 1;
	}
	if(!strcmp(Turn, "Black") && Squares[J][K] == 0 && Camps[J][K] == 2 && Vacant != 1 ) {
	    VacantX = J; VacantY = K; Vacant = 1;
	}
	if(!strcmp(Turn, "White") && Squares[J][K] == 0 && Camps[J][K] == 1 && Vacant != 1 ) {
	    VacantX = J; VacantY = K; Vacant = 1;
	}
	Camps[J][K] = 0;
    } // Next J;
  } // Next K;
  if(!strcmp(Turn, "Black") ) { InitFile = BlackEmpX; InitRank = BlackEmpY; }
  if(!strcmp(Turn, "White") ) { InitFile = WhiteEmpX; InitRank = WhiteEmpY; }
  if(BestCapture > 0 ) {
    SeeFile = CaptureX; SeeRank = CaptureY; AddEmperorMove();
  }
  SeeFile = VacantX; SeeRank = VacantY; AddEmperorMove();
  Influence = 0;
}

void FindInfluence ()
{
  INT LL;
  Changed = 1;
  if(LookMate == 1 ) {
    Clearing[1].File = LastPieceX;
    Clearing[1].Rank = LastPieceY;
    File = LastPieceX; Rank = LastPieceY;
    ClearInfo();
  }
  Clearing[Changed].File = CompFile;
  Clearing[Changed].Rank = CompRank;
  File = InitFile; Rank = InitRank;
  if(InitFile != 0 ) ClearInfo();
  if(Squares[CompFile][CompRank] != 0 ) {
    File = CompFile; Rank = CompRank; CapturedPiece = 1; ClearInfo();
  } else {
    for(LL = 1; LL <= Attacker[CompFile][CompRank]; LL++) {
	File = BanMap[CompFile][CompRank].Info[LL].File;
	Rank = BanMap[CompFile][CompRank].Info[LL].Rank;
	if(File != 0 && (InitFile != File || InitRank != Rank) ) {
	    if(Pieces[abs(Squares[File][Rank])].Range != 0 ) ClearInfo();
	}
    } // Next LL;
  }
  if(ExtraCapture[AA].Piece != 0 ) {
    File = ExtraCapture[AA].File; Rank = ExtraCapture[AA].Rank;  ClearInfo();
    for(LL = 1; LL <= Attacker[ExtraCapture[AA].File][ExtraCapture[AA].Rank]; LL++) {
	File = BanMap[ExtraCapture[AA].File][ExtraCapture[AA].Rank].Info[LL].File;
	Rank = BanMap[ExtraCapture[AA].File][ExtraCapture[AA].Rank].Info[LL].Rank;
	if(Pieces[abs(Squares[File][Rank])].Range != 0 ) ClearInfo();
    } // Next LL;
  }
  for(LL = 1; LL <= Attacker[InitFile][InitRank]; LL++) {
    File = BanMap[InitFile][InitRank].Info[LL].File;
    Rank = BanMap[InitFile][InitRank].Info[LL].Rank;
    if(File != 0 ) {
	if(Pieces[abs(Squares[File][Rank])].Range != 0 ) ClearInfo();
    }
  } // Next LL;
}

void FireDemon ()
{
  INT K, L;
  Evaluate = 0;
  if(SeeMove == 1 && Other != 1 ) {
    if((Influence == 0) || (Squares[NewFile][NewRank] == 0 || Sgn(Squares[InitFile][InitRank]) != Sgn(Squares[NewFile][NewRank])) ) {
      for(K = NewRank - 1; K <= NewRank + 1; K++) {
       for(L = NewFile - 1; L <= NewFile + 1; L++) {
	if(L > 0 && L <= BoardSizeX && K > 0 && K <= BoardSizeY ) {
	    if(((Sgn(Squares[InitFile][InitRank]) != Sgn(Squares[L][K])) && Squares[L][K] != 0 && Legal[L][K] != 1) ) {
		if((K != NewRank || L != NewFile) && Pieces[abs(Squares[L][K])].special != 'F' ) {
		    Board.FillColor = 0xFFFFL;
		    if(Legal[L][K] < 1 ) { SeeFile = L; SeeRank = K; LookMove(); }
		}
	    }
	    if((Influence != 0) && (Squares[L][K] == 0) ) {
		SeeFile = L; SeeRank = K; LookMove();
	    }
	}
       } // Next L;
      } // Next K;
    }
  }
}

void Flame ()
{
  INT K, L, W, King=0; // FIXME: read-only variable
  W = 0;
  for(K = Rank - 1; K <= Rank + 1; K++) {
    for(L = File - 1; L <= File + 1; L++) {
	if(L > 0 && L <= BoardSizeX && K > 0 && K <= BoardSizeY ) {
	    if((Sgn(Squares[File][Rank]) != Sgn(Squares[L][K])) && Squares[L][K] != 0 ) {
		if(strcmp(Pieces[abs(Squares[L][K])].Name, "Fire Demon") ) {
		    if(abs(Squares[L][K]) == 1 || (!strcmp(Pieces[abs(Squares[L][K])].Name, "Crown Prince") && King == 1) ) {
			File = L; Rank = K;
			CheckMate();
		    }
		    if(Mate == 1 ) return;
		    if(W == 0 ) strcat(CMoveSTR, CR "\n    x!"); else strcat(CMoveSTR, ",");
		    if(W == 3 ) strcat(CMoveSTR, CR "\n    ");
		    sprintf(CMoveSTR + strlen(CMoveSTR), "%d%c", ((BoardSizeX - L) + 1), (K + 96));
		    Captures[TurnCount].number = Captures[TurnCount].number + 1;
		    Captures[TurnCount].Positions[Captures[TurnCount].number] = (K * (BoardSizeX + 1)) + L;
		    Captures[TurnCount].PieceNum[Captures[TurnCount].number] = Squares[L][K];
		    Squares[L][K] = 0;
		    Board.showpic[Grafix[L][K]].Visible = False;
		    MOVE(1, Board.showpic[Grafix[L][K]], 0, 0);
		    Grafix[L][K] = -1;
		    W = W + 1;
		}
	    }
	}
    } // Next L;
  } // Next K;
}

void FlashPiece ()
{
  INT PPP;
  if(LastPieceX > -1 && LastPieceY > -1 ) {
    if(Grafix[LastPieceX][LastPieceY] > -1 ) {
	CCC = 0; XA = 0; Blink = 0; PPP = 2;
	Board.Timer2.Interval = 200;
	do {
	    if(Blink / 2. == Int(Blink / 2) && PPP != 2 ) {
		Board.showpic[Grafix[LastPieceX][LastPieceY]].Visible = False; PPP = PPP + 1;
		PPP = 2;
		Board.Refresh();
	    }
	    if(Blink / 2. != Int(Blink / 2) && PPP != 1 ) {
		Board.showpic[Grafix[LastPieceX][LastPieceY]].Visible = True;
		PPP = 1;
		Board.Refresh();
	    }
#ifdef MSVC
	    DoEvents();
#else
	    Blink++; usleep(200000); // [HGM] added. Freezes application during blink
#endif
	} while( !(CCC == 1 || Blink == 14) );
	CCC = 0; XA = 0; Blink = 0; Board.Timer2.Interval = 0;
	Board.showpic[Grafix[LastPieceX][LastPieceY]].Visible = True;
    }
  }
}

void FormActing ()
{  
  if(!strcmp(Threat, "On") && SeeMove == 1 && GameOver != 1 ) {
    GetSquare();
    ActingPieces();
  }
}

void FormDrop ()
{ // DragDrop-event handler for Form; (NewX, NewY) pass coords
  INT K=0, L=0; // FIXME: read-only
  int Response; // write-only?
  if(MovePiece == 1 ) MoveFormDrop();
  if(InitRank == 0 ) HeldDrop();
  if((NewX > XStart && NewX < XStart + (BoardSizeX * Pixels) && NewY > 11 && NewY < 11 + (BoardSizeY * Pixels)) || CompMove == 1 ) {
    GetSquare2();
    if(Legal[File][Rank] > 0 && RightClick != 1 ) {
	if(LionPiece != I ) {
	    Score[TurnCount].IDStart = Squares[InitFile][InitRank];
	    Score[TurnCount].PosStart = (InitRank * (BoardSizeX + 1)) + InitFile;
	    strcat(CMoveSTR, TrimSTR(Pieces[abs(Squares[InitFile][InitRank])].sname));
	    sprintf(CMoveSTR + strlen(CMoveSTR), "%d%c", ((BoardSizeX - InitFile) + 1), (96 + InitRank));
	    if((!strcmp(Computer, Turn) || !strcmp(Computer, "Both")) && LionVictim.Piece != 0 ) {
		sprintf(CMoveSTR + strlen(CMoveSTR), "x%d%c", (BoardSizeX - LionVictim.File + 1), (96 + LionVictim.Rank));
	    }
	}
	sprintf(CMoveSTR + strlen(CMoveSTR), "-%d%c", ((BoardSizeX - File) + 1), (Rank + 96));
	if(Legal[File][Rank] == 1 ) LionPiece = -1;
	Board.Frame.Visible = False;
	Squares[File][Rank] = Squares[InitFile][InitRank];
	if(Squares[File][Rank] == 1 ) { BlackKingX = File; BlackKingY = Rank; }
	if(Squares[File][Rank] == -1 ) { WhiteKingX = File; WhiteKingY = Rank; }
	if(!strcmp(Pieces[abs(Squares[File][Rank])].Name, "Emperor") && Squares[File][Rank] > 0 ) { BlackEmpX = File; BlackEmpY = Rank; }
	if(!strcmp(Pieces[abs(Squares[File][Rank])].Name, "Emperor") && Squares[File][Rank] < 0 ) { WhiteEmpX = File; WhiteEmpY = Rank; }
	Score[TurnCount].PosEnd = (Rank * (BoardSizeX + 1)) + File;
	Score[TurnCount].IDEnd = Squares[File][Rank];
	if(LionVictim.Piece != 0 ) {
	    Captures[TurnCount].number = Captures[TurnCount].number + 1;
	    Captures[TurnCount].Positions[Captures[TurnCount].number] = (LionVictim.Rank * (BoardSizeX + 1)) + LionVictim.File;
	    Captures[TurnCount].PieceNum[Captures[TurnCount].number] = Squares[LionVictim.File][LionVictim.Rank];
	    Taken = 1;
	}
	Grafix[File][Rank] = I;
	if(InitFile != File || InitRank != Rank ) Grafix[InitFile][InitRank] = -1;
	if(InitFile != File || InitRank != Rank ) Squares[InitFile][InitRank] = 0;
	if(LionVictim.Piece != 0 ) {
	    Squares[LionVictim.File][LionVictim.Rank] = 0;
	    Board.showpic[Grafix[LionVictim.File][LionVictim.Rank]].Visible = False;
	    MOVE(1, Board.showpic[Grafix[LionVictim.File][LionVictim.Rank]], 0, 0);
	    Grafix[LionVictim.File][LionVictim.Rank] = -1;
	}
	if(Legal[File][Rank] == 3 ) {
	    Score[TurnCount].IDEnd = 0;
	    Grafix[File][Rank] = -1;
	    Board.showpic[I].Visible = False;
	    MOVE(1, Board.showpic[I], 0, 0);
	    Squares[File][Rank] = 0;
	    LionPiece = -1;
	} else {
	    if(Reverse == 0 ) {
		MOVE(1, Board.showpic[I], XStart + ((File - 1) * Pixels), 11 + ((Rank - 1) * Pixels));
	    } else {
		MOVE(1, Board.showpic[I], XStart + ((BoardSizeX - File) * Pixels), 11 + ((BoardSizeY - Rank) * Pixels));
	    }
	}
	if(Demon == 1 ) Flame();
	if(Mate != 1 ) {
	     if(Legal[File][Rank] == 3 ) strcat(CMoveSTR, "*");
	     if(Legal[File][Rank] == 6 ) Teach = 3;
	     if(Legal[File][Rank] == 2 ) Teach = 2; else Teach = 1;
	     if(Legal[L][K] == 2 || Legal[L][K] == 6 ) { // FIXME: uninitialized????
		 DoubleMove();
	     } else {
		 LastPieceX = File; LastPieceY = Rank;
		 if(CompMove != 1 ) NextTurn();
	     }
	 }
    }
  }
  ClearLegal();
  if(Mate == 1 ) {
    GameOver = 1;
    Response = MsgBox("        Checkmate!          ", 0, (sprintf(StringTmp, "%s Wins!", Turn), StringTmp)); NextTurn();
  }
}

void GetSquare ()
{
  if(Reverse == 0 ) {
    InitFile = Int((NewX - XStart) / Pixels) + 1;
    InitRank = Int((NewY - 11) / Pixels) + 1;
  } else {
    InitFile = Int((BoardSizeX * Pixels - (NewX - XStart)) / Pixels) + 1;
    InitRank = Int((BoardSizeY * Pixels - (NewY - 11)) / Pixels) + 1;
  }
  if(InitRank < 1 || InitRank > BoardSizeY || InitFile < 1 || InitFile > BoardSizeX ) {
    InitRank = 0; InitFile = 0;
  }
}

void GetSquare2 ()
{
  if(CompMove != 1 && DropTest != 1 ) {
    if(Reverse == 0 ) {
	File = Int((NewX - XStart) / Pixels) + 1;
	Rank = Int((NewY - 11) / Pixels) + 1;
    } else {
	File = Int((BoardSizeX * Pixels - (NewX - XStart)) / Pixels) + 1;
	Rank = Int((BoardSizeY * Pixels - (NewY - 11)) / Pixels) + 1;
    }
  }
}

void HeldDown ()
{ // Button1-Down handler for holdings piece. CCC = 1, I = (holdings) piece number
  if(strcmp(Computer, Turn) && strcmp(Computer, "Both") && Level != 0 && GameOver != 1 ) {
   Notice = 0;
   Board.PieceID.ForeColor = 0xFF0000;
   if(Selection != 0 || MovePiece == 1 ) {
    AddHand2();
   } else {
    if(Reduce == 1 ) {
	InHand[I] = InHand[I] - 1;
	if(InHand[I] < 2 ) strcpy(Board.Held[I].Caption, ""); else sprintf(Board.Held[I].Caption, "%d", InHand[I]);
	if(InHand[I] < 1 ) Board.HandPic[I].Visible = False;
    } else {
	strcpy(Board.Caption, Cap);
	strcpy(Board.PieceID.Caption, Pieces[abs(CapRef[I])].Name);
	if((I > Capture && !strcmp(Turn, "White") && Reverse == 0) || (I <= Capture && !strcmp(Turn, "Black") && Reverse == 0) || (I > Capture && !strcmp(Turn, "Black") && Reverse == 1) || (I <= Capture && !strcmp(Turn, "White") && Reverse == 1) ) {
	    DRAG(Board.HandPic[I], 1);
	    HeldValid();
	} else {
	    Board.FillStyle = 1; Other = 1;
	    Board.ForeColor = 0xC00000;
	    HeldValid();
	    Board.ForeColor = 0x0;
	    Board.FillStyle = 0; Other = 0;
	}
    }
   }
  }
}

void HeldDrop ()
{
  if((NewX > XStart && NewX < XStart + (BoardSizeX * Pixels) && NewY > 11 && NewY < 11 + (BoardSizeY * Pixels)) || DropTest == 1 ) {
    GetSquare2();
    if(Legal[File][Rank] == 1 ) {
	Squares[File][Rank] = CapRef[I];
	NewGraf = 0;
	while( Board.showpic[NewGraf].Visible == True ) {
	    NewGraf = NewGraf + 1;
	}
	Score[TurnCount].IDStart = Squares[File][Rank];
	Score[TurnCount].IDEnd = Squares[File][Rank];
	Score[TurnCount].PosStart = 0;
	Score[TurnCount].PosEnd = (Rank * (BoardSizeX + 1)) + File;
	strcpy(CMoveSTR, TrimSTR(Pieces[abs(Squares[File][Rank])].sname));
	sprintf(CMoveSTR + strlen(CMoveSTR), "*%d%c", ((BoardSizeX - File) + 1), (Rank + 96));
	Grafix[File][Rank] = NewGraf;
	Board.showpic[NewGraf] = Board.HandPic[I];
	if(Reverse == 0 ) {
	    MOVE(1, Board.showpic[NewGraf], XStart + (File - 1) * Pixels, 11 + (Rank - 1) * Pixels);
	} else {
	    MOVE(1, Board.showpic[NewGraf], XStart + (BoardSizeX - File) * Pixels, 11 + (BoardSizeY - Rank) * Pixels);
	}
	Board.showpic[NewGraf].Visible = True;
	InHand[I] = InHand[I] - 1;
	if(InHand[I] < 1 ) Board.HandPic[I].Visible = False;
	if(InHand[I] < 2 ) strcpy(Board.Held[I].Caption, ""); else sprintf(Board.Held[I].Caption, "%d", InHand[I]);
	if(!strcmp(Choice, "Micro") ) MicroDrop();
	InitRank = 0; InitFile = 0;
	Dropped = 1;
	LastPieceX = File; LastPieceY = Rank;
	if(CompMove != 1 ) NextTurn();
    }
  }
  ClearLegal();
}

void HeldProm ()
{ // Button2-Down handler for holdings piece. CCC = 1, I = (holdings) piece number
  STRING PromName;
  if(strcmp(Computer, Turn) && strcmp(Computer, "Both") && Level != 0 && GameOver != 1 ) {
    strcpy(Board.PieceID.Caption, "");
    Board.PieceID.ForeColor = 0x8000L;
    if(Pieces[abs(CapRef[I])].Promotes > 0 && strcmp(Pieces[abs(CapRef[I])].Name, "Killer Whale") ) strcpy(PromName, Pieces[abs(Pieces[abs(CapRef[I])].Promotes)].Name );else strcpy(PromName, "None");
    sprintf(Board.PieceID.Caption, "Promotes to %s", PromName);
  }
}

void HeldShow ()
{
  INT K, L;
  if(SeeMove == 1 || CompMove == 1 ) {
    Board.FillColor = 0xFFFFFF;
    for(K = 1; K <= BoardSizeY; K++) {
	for(L = 1; L <= BoardSizeX; L++) {
	    if(Legal[L][K] == 1 ) { SeeFile = L; SeeRank = K; LookMove(); }
	} // Next L;
    } // Next K;
  }
  if(Evaluate != 1 && DropTest != 1 ) InitRank = 0;
}

void HeldValid ()
{
  INT K, L, X;
  for(K = 1; K <= BoardSizeY; K++) {
    for(L = 1; L <= BoardSizeX; L++) {
	if(Squares[L][K] == 0 ) Legal[L][K] = 1;
	if(Pieces[abs(CapRef[I])].special == '1' ) {
	    if(CapRef[I] > 0 && K == 1 ) Legal[L][K] = 0;
	    if(CapRef[I] < 0 && K == BoardSizeY ) Legal[L][K] = 0;
	}
	if(Pieces[abs(CapRef[I])].special == '2' ) {
	    if(CapRef[I] > 0 && K < 3 ) Legal[L][K] = 0;
	    if(CapRef[I] < 0 && K > BoardSizeY - 2 ) Legal[L][K] = 0;
	}
    } // Next L;
  } // Next K;
  if((strcmp(Choice, "Micro")) && (!strcmp(Pieces[abs(CapRef[I])].Name, "Pawn") || !strcmp(Pieces[abs(CapRef[I])].Name, "Sparrow Pawn")) ) {
    for(N = 1; N <= BoardSizeY; N++) {
	for(P = 1; P <= BoardSizeX; P++) {
	    if(Squares[P][N] != 0 ) {
		if((!strcmp(Pieces[abs(Squares[P][N])].Name, "Pawn") || !strcmp(Pieces[abs(Squares[P][N])].Name, "Sparrow Pawn")) && Sgn(Squares[P][N]) == Sgn(CapRef[I]) ) {
		    for(X = 1; X <= BoardSizeY; X++) {
			Legal[P][X] = 0;
		    } // Next X;
		}
	    }
	} // Next P;
    } // Next N;
  }
  if(!strcmp(Choice, "Tori") && !strcmp(Pieces[abs(CapRef[I])].Name, "Swallow") ) {
    for(N = 1; N <= BoardSizeX; N++) {
	C = 0;
	for(P = 1; P <= BoardSizeY; P++) {
	    if(Squares[N][P] != 0 ) {
		if(!strcmp(Pieces[abs(Squares[N][P])].Name, "Swallow") && Sgn(Squares[N][P]) == Sgn(CapRef[I]) ) {
		    C = C + 1;
		    if(C == 2 ) {
			for(X = 1; X <= BoardSizeY; X++) {
			    Legal[N][X] = 0;
			} // Next X;
		    }
		}
	    }
	} // Next P;
    } // Next N;
  }
  if(!strcmp(Choice, "Whale") && !strcmp(Pieces[abs(CapRef[I])].Name, "Dolphin") ) {
    for(N = 1; N <= BoardSizeX; N++) {
	C = 0;
	for(P = 1; P <= BoardSizeY; P++) {
	    if(Squares[N][P] != 0 ) {
		if(!strcmp(Pieces[abs(Squares[N][P])].Name, "Dolphin") && Sgn(Squares[N][P]) == Sgn(CapRef[I]) ) {
		    C = C + 1;
		    if(C == 2 ) {
			for(X = 1; X <= BoardSizeY; X++) {
			    Legal[N][X] = 0;
			} // Next X;
		    }
		}
	    }
	} // Next P;
    } // Next N;
  }
  if(CompMove != 1 ) AskMate();
  if(Dropping != 1 ) HeldShow();
}

void HookMove ()
{
  OldLast = Last; OldFile = NewFile; OldRank = NewRank;
  while( NewFile - RankInc > 0 && NewFile - RankInc <= BoardSizeX && NewRank + FileInc > 0 && NewRank + FileInc <= BoardSizeY && Last == 0) {
    NewFile = NewFile - RankInc;
    NewRank = NewRank + FileInc;
    ShowMove();
  }
  NewFile = OldFile; NewRank = OldRank; Last = 0;
  while( NewFile + RankInc > 0 && NewFile + RankInc <= BoardSizeX && NewRank - FileInc > 0 && NewRank - FileInc <= BoardSizeY && Last == 0) {
    NewFile = NewFile + RankInc;
    NewRank = NewRank - FileInc;
    ShowMove();
  }
  NewFile = OldFile; NewRank = OldRank; Last = OldLast;
}

void Igui ()
{
  INT K, L;
  for(K = InitRank - 1; K <= InitRank + 1; K++) {
    for(L = InitFile - 1; L <= InitFile + 1; L++) {
	if(K > 0 && K <= BoardSizeY && L > 0 && L <= BoardSizeX ) {
	    if(Sgn(Squares[InitFile][InitRank]) != Sgn(Squares[L][K]) || (Squares[L][K] == 0 && Influence > 0) ) {
		Board.FillColor = 0x8000L;
		if(SeeMove == 1 ) { SeeFile = L; SeeRank = K; LookMove(); }
		Legal[L][K] = 4;
	    }
	}
    } // Next L;
  } // Next K;
}

void Jumping ()
{
  NewFile = InitFile + (FileInc * 2); NewRank = InitRank + (RankInc * 2);
  if(NewFile > 0 && NewFile <= BoardSizeX && NewRank > 0 && NewRank <= BoardSizeY ) ShowMove();
}

void KingSafety ()
{    
// King Safety \ Attacks;

  if(strcmp(Choice, "Micro") && strcmp(Choice, "Mini") && strcmp(Choice, "Judkin") && strcmp(Choice, "Yari") && strcmp(Choice, "Tori") && strcmp(Choice, "Whale") ) {
    if(CompLegal[AA].EndPiece == 1 ) {
	if(abs(InitFile - ((BoardSizeX / 2) + .5)) < abs(CompFile - ((BoardSizeX / 2) + .5)) ) KingTally[AA] = KingTally[AA] + 3;
	KingTally[AA] = KingTally[AA] - (BoardSizeY - CompRank);
    }
    if(CompLegal[AA].EndPiece == -1 ) {

	if(abs(((BoardSizeX / 2) + .5) - InitFile) < abs(((BoardSizeX / 2) + .5) - CompFile) ) KingTally[AA] = KingTally[AA] + 3;
	KingTally[AA] = KingTally[AA] - (CompRank - 1);
    }
    if(!strcmp(Turn, "Black") && !strcmp(Pieces[abs(CompLegal[AA].EndPiece)].Name, "Gold") && abs(CompFile - BlackKingX) > 1 && abs(CompRank - BlackKingY) > 1 && (abs(CompFile - BlackKingX) > abs(InitFile - BlackKingX) || abs(CompRank - BlackKingY) > abs(InitRank - BlackKingY)) ) KingTally[AA] = KingTally[AA] - 2;
    if(!strcmp(Turn, "White") && !strcmp(Pieces[abs(CompLegal[AA].EndPiece)].Name, "Gold") && abs(CompFile - WhiteKingX) > 1 && abs(CompRank - WhiteKingY) > 1 && (abs(CompFile - WhiteKingX) > abs(InitFile - WhiteKingX) || abs(CompRank - WhiteKingY) > abs(InitRank - WhiteKingY)) ) KingTally[AA] = KingTally[AA] - 2;
  }
  if(!strcmp(Turn, "Black") && CompRank <= PromDotY ) KingTally[AA] = KingTally[AA] + 3;
  if(!strcmp(Turn, "White") && CompRank > BoardSizeY - PromDotY ) KingTally[AA] = KingTally[AA] + 3;
  if(BoardSizeY > 11 ) {
    if(!strcmp(Turn, "Black") && Pieces[abs(CompLegal[AA].EndPiece)].Range != 1 && InitRank > CompRank ) KingTally[AA] = KingTally[AA] + 3;
    if(!strcmp(Turn, "White") && Pieces[abs(CompLegal[AA].EndPiece)].Range != 1 && InitRank < CompRank ) KingTally[AA] = KingTally[AA] + 3;
    if(!strcmp(Turn, "Black") && Pieces[abs(CompLegal[AA].EndPiece)].Range == 1 && InitRank < CompRank ) KingTally[AA] = KingTally[AA] + 2;
    if(!strcmp(Turn, "White") && Pieces[abs(CompLegal[AA].EndPiece)].Range == 1 && InitRank > CompRank ) KingTally[AA] = KingTally[AA] + 2;
  }
  if(!strcmp(Turn, "Black") && InitRank < BoardSizeY * .35 ) {
    if(abs(InitFile - WhiteKingX) > abs(CompFile - WhiteKingX) || abs(InitRank - WhiteKingY) > abs(CompRank - WhiteKingY) ) KingTally[AA] = KingTally[AA] + 5;
  }
  if(!strcmp(Turn, "White") && InitRank > BoardSizeY * .65 ) {
    if(abs(InitFile - BlackKingX) > abs(CompFile - BlackKingX) || abs(InitRank - BlackKingY) > abs(CompRank - BlackKingY) ) KingTally[AA] = KingTally[AA] + 5;
  }
}

void KnightJump ()
{
  if(abs(RankInc) == 1 ) {
    NewFile = InitFile + 1; NewRank = InitRank + (RankInc * 2);
    if(NewFile <= BoardSizeX && NewRank > 0 && NewRank <= BoardSizeY ) ShowMove();
    NewFile = InitFile - 1;
    if(NewFile > 0 && NewRank > 0 && NewRank <= BoardSizeY ) ShowMove();
  }
  if(abs(FileInc) == 1 ) {
    NewRank = InitRank + 1; NewFile = InitFile + (FileInc * 2);
    if(NewRank <= BoardSizeY && NewFile > 0 && NewFile <= BoardSizeX ) ShowMove();
    NewRank = InitRank - 1;
    if(NewRank > 0 && NewFile > 0 && NewFile <= BoardSizeX ) ShowMove();
  }
}

void Lion ()
{
  INT X, Y;
  if(Teach == 1 ) {
    Lion2();
  } else {
    for(Y = InitRank - 2; Y <= InitRank + 2; Y++) {
	for(X = InitFile - 2; X <= InitFile + 2; X++) {
	    if(X > 0 && X <= BoardSizeX && Y > 0 && Y <= BoardSizeY ) {
		if(Squares[X][Y] == 0 || Sgn(Squares[X][Y]) != Sgn(Squares[InitFile][InitRank]) || (Influence > 0 && (Y != InitRank || X != InitFile)) ) {
		    if(abs(Y - InitRank) <= 1 && abs(X - InitFile) <= 1 ) {
			Board.FillColor = 0xFF0000;
			NewRank = Y; NewFile = X;
			if(!strcmp(Choice, "Tenjiku") ) CheckBurn();
			if(LegalMoves > 0 && Squares[X][Y] != 0 && strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "Teaching King") ) {
			    if(Evaluate == 0 && CompMove == 1 && Influence != 1 ) CompLion();
			    Legal[X][Y] = 1;
			} else {
			    Legal[X][Y] = 2;
			}
		    } else {
			Legal[X][Y] = 1;
			NewRank = Y; NewFile = X;
			if(!strcmp(Choice, "Chu") && Squares[X][Y] != 0 && ChuLionTest != 1 ) {
			    if(!strcmp(Pieces[abs(Squares[X][Y])].Name, "Lion") ) ChuLion();
			}
			Board.FillColor = 0xFFFF00;
			if(!strcmp(Choice, "Tenjiku") ) CheckBurn();
		    }
		    if(SeeMove == 1 && ProtectLion != 1 && CompLionTest != 1 && Legal[X][Y] > 0 ) { SeeFile = X; SeeRank = Y; LookMove(); }
		    ProtectLion = 0; CompLionTest = 0;
		}
	    }
	} // Next X;
    } // Next Y;
  }
}

void Lion2 ()
{
  INT X, Y;
  for(Y = InitRank - 1; Y <= InitRank + 1; Y++) {
    for(X = InitFile - 1; X <= InitFile + 1; X++) {
	if(X > 0 && X <= BoardSizeX && Y > 0 && Y <= BoardSizeY ) {
	    if(Squares[X][Y] == 0 || Sgn(Squares[X][Y]) != Sgn(Squares[InitFile][InitRank]) || Influence == 2 ) {
		Legal[X][Y] = 1;
		Board.FillColor = 0xFFFF00;
		if(SeeMove == 1 ) { SeeFile = X; SeeRank = Y; LookMove(); }
		NewFile = X; NewRank = Y;
		if(!strcmp(Choice, "Tenjiku") ) CheckBurn();
	    }
	}
    } // Next X;
  } // Next Y;
}

void LionBurn ()
{
  INT K, L;
  for(K = Rank - 1; K <= Rank + 1; K++) {
    for(L = File - 1; L <= File + 1; L++) {
	if(LionTest != 1 && L > 0 && L <= BoardSizeX && K > 0 && K <= BoardSizeY ) {
	    if((K != Rank || L != File) && Squares[L][K] != 0 ) {
		if(Pieces[abs(Squares[L][K])].special == 'F' && Sgn(Squares[File][Rank]) != Sgn(Squares[L][K]) ) LionBurn2();
	    }
	}
    } // Next L;
  } // Next K;
  LionTest = 0;
}

void LionBurn2 ()
{
  Score[TurnCount].IDEnd = 0;
  strcat(CMoveSTR, "*");
  Grafix[File][Rank] = -1;
  Board.showpic[I].Visible = False;
  MOVE(1, Board.showpic[I], 0, 0);
  Squares[File][Rank] = 0;
  LionTest = 1;
}

void LionIgui ()
{
  INT K, L, MMM;
  if(BoardSizeY > 11 ) {
    MMM = 0;
    for(K = 1; K <= BoardSizeY; K++) {
	for(L = 1; L <= BoardSizeX; L++) {
	    if(Grafix[L][K] == I ) { File = L; Rank = K; MMM = 1; break; }
	} // Next L;
	if(MMM == 1 ) break;
    } // Next K;
    if(MMM == 1 && ((!strcmp(Turn, "Black") && Squares[File][Rank] > 0) || (!strcmp(Turn, "White") && Squares[File][Rank] < 0)) ) {

	if(Pieces[abs(Squares[File][Rank])].special == 'L' || !strcmp(Pieces[abs(Squares[File][Rank])].Name, "Horned Falcon") || !strcmp(Pieces[abs(Squares[File][Rank])].Name, "Soaring Eagle") || (!strcmp(Pieces[abs(Squares[File][Rank])].Name, "Teaching King") && TeachVer == 2) ) {
	    InitFile = File; InitRank = Rank;
	    Score[TurnCount].IDStart = Squares[File][Rank];
	    Score[TurnCount].IDEnd = Squares[File][Rank];
	    Score[TurnCount].PosStart = (InitRank * (BoardSizeX + 1)) + InitFile;
	    Score[TurnCount].PosEnd = (InitRank * (BoardSizeX + 1)) + InitFile;
	    sprintf(CMoveSTR + strlen(CMoveSTR), "%s!", TrimSTR(Pieces[abs(Squares[InitFile][InitRank])].sname));
	    LastPieceX = File; LastPieceY = Rank;
	    Ligui = 1;
	    strcpy(ShortScore[TurnCount + 1], CMoveSTR);
	    NextTurn();
	}
    }
  }
}

void LionPower ()
{
  INT K;
  for(K = 1; K <= 2; K++) {
    C = InitFile; D = InitRank;
    NewFile = InitFile + (FileInc * K); NewRank = InitRank + (RankInc * K);
    if(NewFile > 0 && NewFile <= BoardSizeX && NewRank > 0 && NewRank <= BoardSizeY ) {
	if(Squares[NewFile][NewRank] == 0 || Sgn(Squares[InitFile][InitRank]) != Sgn(Squares[NewFile][NewRank]) || (Influence > 0 && (NewFile != InitFile || NewRank != InitRank)) ) {
	    if(K == 1 ) {
		Board.FillColor = 0xFF0000;
		if(!strcmp(Choice, "Tenjiku") ) CheckBurn();
		if(LegalMoves > 0 && Squares[NewFile][NewRank] != 0 ) {
		    if(CompMove == 1 && Evaluate == 0 && Influence != 1 ) CompLionPower();
		    Legal[NewFile][NewRank] = 1;
		} else {
		    Legal[NewFile][NewRank] = 2;
		}
	    } else {
		Board.FillColor = 0xFFFF00;
		Legal[NewFile][NewRank] = 1;
		if(!strcmp(Choice, "Tenjiku") ) CheckBurn();
	    }
	    if(SeeMove == 1 && CompLionTest != 1 && Legal[NewFile][NewRank] != 0 ) { SeeFile = NewFile; SeeRank = NewRank; LookMove(); }
	    CompLionTest = 0;
	}
    }
  } // Next K;
}

void LionPower2 ()
{
  INT X, Y;
  Legal[C][D] = 1;
  Board.FillColor = 0xFFFF00;
  SeeFile = C; SeeRank = D; LookMove();
  X = InitFile + (InitFile - C);
  Y = InitRank + (InitRank - D);
  if(X > 0 && X <= BoardSizeX && Y > 0 && Y <= BoardSizeY ) {
    if(Squares[X][Y] == 0 || Sgn(Squares[InitFile][InitRank]) != Sgn(Squares[X][Y]) || (Influence == 2 && (NewFile != InitFile || NewRank != InitRank)) ) {
	Legal[X][Y] = 1;
	if(SeeMove == 1 ) { SeeFile = X; SeeRank = Y; LookMove(); }
	NewFile = X; NewRank = Y;
	if(!strcmp(Choice, "Tenjiku") ) CheckBurn();
    }
  }
}

void LoadGame ()
{ // Menu function.
  INT Location;
// TODO:  On Error Resume Next;
  strcpy(Board.CMSave.DialogTitle, "Load Game");
  Board.CMSave.Flags = 0x1000L;
  Board.CMSave.Action = 1;
  if(Err == 32755 ) return; // TODO
  strcpy(LoadedSTR, Board.CMSave.Filename);
  Loading = 1;
  strcpy(OldChoice, Choice);
  f2 = fopen(LoadedSTR, "r");
  fscanf(f2, "\"%[^\"]\",", Choice);
  if(!strcmp(Choice, "Tai") && Display < 800 ) {
    TooSmall();
    strcpy(Choice, OldChoice);
    Loading = 0;
    fclose(f2);
  } else {
    UnloadPieceHelp();
// TODO:    Unload RulesHelp;
    Board.MousePointer = 11;
    if(!strcmp(Choice, OldChoice) ) UnloadBoard();
    StartUp();
    fscanf(f2, "%s", SaveTitleSTR);
    if(strcmp(SaveTitleSTR, "") ) {
	strcpy(Cap, SaveTitleSTR);
	strcpy(Board.Caption, Cap);
    }
    fscanf(f2, "\"%[^\"]\",", Computer);
    fscanf(f2, "%d", &Level);
    fscanf(f2, "%d", &GameOver);
    fscanf(f2, "\"%[^\"]\"", Threat);
    fclose(f2);
    EndTurn = TurnCount;
    ConfigLoad();
    Loading = 0;
    Board.MnuHandicap.Enabled = False;
    if(GameOver != 1 ) ConvertScore();
    if(Asc(Score[TurnCount].Caption) > 47 && Asc(Score[TurnCount].Caption) < 58 ) {
	strcpy(Board.LastMove.Caption, Score[TurnCount].Caption);
    } else {
	sprintf(Board.LastMove.Caption, "%d. %s", MoveCount, Score[TurnCount].Caption);
    }
    if(TurnCount > 0 ) {
	Location = Score[TurnCount - 1].PosEnd;
	if(Location > 0 ) {
	    Rank = Int(Location / (BoardSizeX + 1));
	    File = Location - (Rank * (BoardSizeX + 1));
	}
	LastPieceX = File; LastPieceY = Rank;
    } else {
	LastPieceX = -1; LastPieceY = -1;
    }
  }
}

void LoadGame2 ()
{
  INT Location;
// TODO:  On Error Resume Next;
  strcpy(Start.CMSave.DialogTitle, "Load Game");
  Start.CMSave.Flags = 0x1000L;
  Start.CMSave.Action = 1;
  if(Err == 32755 ) return; // TODO
  strcpy(LoadedSTR, Start.CMSave.Filename);
  Start.MousePointer = 11;
  Loading = 1;
  strcpy(OldChoice, Choice);
  f2 = fopen(LoadedSTR, "r");
  fscanf(f2, "%s", Choice);
  if(!strcmp(Choice, "Tai") && Display < 800 ) {
    TooSmall();
    strcpy(Choice, OldChoice);
    Loading = 0;
    fclose(f2);
  } else {
    UnloadPieceHelp();
// TODO:    Unload RulesHelp;
    UnloadBoard();
    StartUp();
    fscanf(f2, "%s", SaveTitleSTR);
    if(strcmp(SaveTitleSTR, "") ) strcpy(Board.Caption, SaveTitleSTR);
    fscanf(f2, "\"%[^\"]\"", Computer);
    fscanf(f2, "%d", &Level);
    fscanf(f2, "%d", &GameOver);
    fscanf(f2, "\"%[^\"]\"", Threat);
    fclose(f2);
//    Start.Hide();
    Start.MousePointer = 0;
    ConfigLoad();
    Loading = 0;
    EndTurn = TurnCount;
    Board.MnuHandicap.Enabled = False;
    EnableNamedMenuItem("Setup.Handicap", False); // [HGM] added
    if(GameOver != 1 ) ConvertScore();
    if(Asc(Score[TurnCount].Caption) > 47 && Asc(Score[TurnCount].Caption) < 58 ) {
      strcpy(Board.LastMove.Caption, Score[TurnCount].Caption);
    } else {
      sprintf(Board.LastMove.Caption, "%d. %s", MoveCount, Score[TurnCount].Caption);
    }
    if(TurnCount > 0 ) {
      Location = Score[TurnCount - 1].PosEnd;
      if(Location > 0 ) {
	Rank = Int(Location / (BoardSizeX + 1));
	File = Location - (Rank * (BoardSizeX + 1));
      }
      LastPieceX = File; LastPieceY = Rank;
    } else {
      LastPieceX = -1; LastPieceY = -1;
    }
  }
}

void LookAhead ()
{
  INT DDD, ZZZZ;
  if(!strcmp(Turn, "White") ) strcpy(Turn, "Black"); else strcpy(Turn, "White");
  RealLevel = Level; Level = 0;
  for(YYY = 1; YYY <= BoardSizeY; YYY++) {
    for(XXX = 1; XXX <= BoardSizeX; XXX++) {
	TestBoard[XXX][YYY] = Comp[XXX][YYY];
    } // Next XXX;
  } // Next YYY;
  if(Drop == 1 ) {
    for(DDD = 1; DDD <= Capture * 2; DDD++) {
	OldHand[DDD] = InHand[DDD];
    } // Next DDD;
  }
  for(Depth = 1; Depth <= TestDepth; Depth++) {
    if(MoveList[Depth].StartFile != 0 ) Comp[MoveList[Depth].StartFile][MoveList[Depth].StartRank] = 0;
    Comp[MoveList[Depth].EndFile][MoveList[Depth].EndRank] = MoveList[Depth].EndPiece;
    if(MoveList[Depth].StartFile == 0 ) {
	for(DDD = 1; DDD <= Capture * 2; DDD++) {
	    if(MoveList[Depth].EndPiece == CapRef[DDD] ) InHand[DDD] = InHand[DDD] - 1;
	} // Next DDD;
    }
    TestAhead();
    FinalTally[Depth] = BestTally[0] - OldKingTally[Depth];
    for(YYY = 1; YYY <= BoardSizeY; YYY++) {
	for(XXX = 1; XXX <= BoardSizeX; XXX++) {
	    Comp[XXX][YYY] = TestBoard[XXX][YYY];
	} // Next XXX;
    } // Next YYY;
    if(Drop == 1 ) {
	for(DDD = 1; DDD <= Capture * 2; DDD++) {
	    InHand[DDD] = OldHand[DDD];
	} // Next DDD;
    }
  } // Next Depth;
  Level = RealLevel; BestMove = TestDepth; BestScore = FinalTally[TestDepth];
  if(!strcmp(Turn, "White") ) strcpy(Turn, "Black"); else strcpy(Turn, "White");
  for(ZZZZ = 1; ZZZZ <= TestDepth - 1; ZZZZ++) {
    if(FinalTally[ZZZZ] < BestScore ) {
	BestMove = ZZZZ;
	BestScore = FinalTally[ZZZZ];
    }
  } // Next ZZZZ;
  CompLegal[BestMove].StartFile = MoveList[BestMove].StartFile;
  CompLegal[BestMove].StartRank = MoveList[BestMove].StartRank;
  CompLegal[BestMove].StartPiece = MoveList[BestMove].StartPiece;
  CompLegal[BestMove].EndFile = MoveList[BestMove].EndFile;
  CompLegal[BestMove].EndRank = MoveList[BestMove].EndRank;
  CompLegal[BestMove].EndPiece = MoveList[BestMove].EndPiece;
  if(!strcmp(Choice, "Dai") || !strcmp(Choice, "Chu") || !strcmp(Choice, "DaiDai") || !strcmp(Choice, "Maka") || !strcmp(Choice, "Tenjiku") || !strcmp(Choice, "Tai") ) {
    LionVictim.Piece = ECapture[BestMove].Piece;
    LionVictim.File = ECapture[BestMove].File;
    LionVictim.Rank = ECapture[BestMove].Rank;
  }
}

void LookCheck ()
{
  INT EFF, GHH;
  if(!strcmp(Turn, "White") && BlackKing == 0 ) {
    OriginalPiece = Squares[BlackKingX][BlackKingY];
    OriginalFile = BlackKingX; OriginalRank = BlackKingY;
  } else {
    if(!strcmp(Turn, "Black") && WhiteKing == 0 ) {
	OriginalPiece = Squares[WhiteKingX][WhiteKingY];
	OriginalFile = WhiteKingX; OriginalRank = WhiteKingY;
    }
  }
  Influence = 0;
  CheckTest = 1;
  CheckLooked = 0;
  LookCheck2();
  CheckTest = 0;
  if(GameOver == 1 || Checked > 0 ) return;
  for(GHH = 1; GHH <= BoardSizeY; GHH++) {
    for(EFF = 1; EFF <= BoardSizeX; EFF++) {
	if(Squares[EFF][GHH] != 0 ) {

	    if(abs(Squares[EFF][GHH]) == 1 || !strcmp(Pieces[abs(Squares[EFF][GHH])].Name, "Emperor") || !strcmp(Pieces[abs(Squares[EFF][GHH])].Name, "Crown Prince") || !strcmp(Pieces[abs(Squares[EFF][GHH])].Name, "Prince") ) {
		OriginalPiece = Squares[EFF][GHH];
		OriginalFile = EFF; OriginalRank = GHH;
		Influence = 0;
		CheckTest = 1;
		CheckLooked = 0;
		LookCheck2();
		CheckTest = 0;
		if(GameOver == 1 || Checked > 0 ) return;
	    }
	}
    } // Next EFF;
  } // Next GHH;
  CheckTest = 0;
}

void LookCheck2 ()
{
  INT ABB, CDD;
  for(ABB = 1; ABB <= BoardSizeY; ABB++) {
    for(CDD = 1; CDD <= BoardSizeX; CDD++) {
	if(Squares[CDD][ABB] != 0 ) {
	    if(Sgn(OriginalPiece) != Sgn(Squares[CDD][ABB]) && strcmp(Pieces[abs(Squares[CDD][ABB])].Name, "Emperor") ) {
		InitFile = CDD; InitRank = ABB;
		Validate();
		if(GameOver == 1 || Checked > 0 ) { Influence = 0; return; }
	    }
	}
    } // Next CDD;
  } // Next ABB;
  Influence = 0;
}

void LookComp ()
{
  Attacker[SeeFile][SeeRank] = Attacker[SeeFile][SeeRank] + 1;
  BanMap[SeeFile][SeeRank].Info[Attacker[SeeFile][SeeRank]].File = InitFile;
  BanMap[SeeFile][SeeRank].Info[Attacker[SeeFile][SeeRank]].Rank = InitRank;
  BanMap[SeeFile][SeeRank].Info[Attacker[SeeFile][SeeRank]].Piece = Squares[InitFile][InitRank];
  if(Squares[InitFile][InitRank] < 0 ) {
    BanMap[SeeFile][SeeRank].WhiteNum = BanMap[SeeFile][SeeRank].WhiteNum + 1;
  } else {
    BanMap[SeeFile][SeeRank].BlackNum = BanMap[SeeFile][SeeRank].BlackNum + 1;
  }
  DoEvents();

  if(FirstTime == 1 ) LookComp2();
}

void LookComp2 ()
{
  OldAttack[SeeFile][SeeRank] = Attacker[SeeFile][SeeRank];
  BackMap[SeeFile][SeeRank].Info[Attacker[SeeFile][SeeRank]].File = InitFile;
  BackMap[SeeFile][SeeRank].Info[Attacker[SeeFile][SeeRank]].Rank = InitRank;
  BackMap[SeeFile][SeeRank].Info[Attacker[SeeFile][SeeRank]].Piece = Squares[InitFile][InitRank];
  if(Squares[InitFile][InitRank] < 0 ) {
    BackMap[SeeFile][SeeRank].WhiteNum = BanMap[SeeFile][SeeRank].WhiteNum;
  } else {
    BackMap[SeeFile][SeeRank].BlackNum = BanMap[SeeFile][SeeRank].BlackNum;
  }
}

void LookForMate ()
{
  STRING OldTurn;
  LookMate = 1;
  strcpy(OldComputer, Computer);
  strcpy(Computer, Turn2); strcpy(OldTurn, Turn); strcpy(Turn, Turn2);
  RealLevel = Level; Level = 0;
  CompMain();
  Level = RealLevel;
  LookMate = 0; LegalMoves = 0; strcpy(Turn, OldTurn);
  strcpy(Computer, OldComputer);
}

void LookMove ()
{
 INT L, EF;
 int Response;
 if(ChuLionTest == 1 ) {
   if(SeeFile == OldNewFile && SeeRank == OldNewRank ) { ProtectLion = 1; return; }
 } else {
  if(Evaluate == 1 ) {
    LookComp();
  } else {
   if(CompMove == 1 && Influence != 2 && Evaluate == 0 ) {
     AddLegalMove();
   } else {
    if((Influence < 1 && CheckTest != 1) ) {
     if(Reverse == 0 ) {
	Board.Circle(XStart + (SeeFile - 1) * Pixels + (Pixels / 2), 11 + (SeeRank - 1) * Pixels + (Pixels / 2), Pixels / 4);
     } else {
	Board.Circle(XStart + (BoardSizeX - SeeFile) * Pixels + (Pixels / 2), 11 + (BoardSizeY - SeeRank) * Pixels + (Pixels / 2), Pixels / 4);
     }
    } else {
     if(Influence == 2 ) {
	if(Legal[SeeFile][SeeRank] > 0 ) {
	    if(Squares[InitFile][InitRank] < 0 ) {
		if((Camps[SeeFile][SeeRank] != 2 && Camps[SeeFile][SeeRank] != 3) ) Camps[SeeFile][SeeRank] = 1; else Camps[SeeFile][SeeRank] = 3;
	    } else {
		if((Camps[SeeFile][SeeRank] != 1 && Camps[SeeFile][SeeRank] != 3) ) Camps[SeeFile][SeeRank] = 2; else Camps[SeeFile][SeeRank] = 3;
	    }
	}
     } else {
	if(OriginalFile == SeeFile && OriginalRank == SeeRank ) {
	    if(CheckTest == 1 && CheckLooked == 0 ) {
		Response = 0;
		if(OriginalPiece > 0 && !strcmp(Turn, "White") && strcmp(Computer, "Black") && strcmp(Computer, "Both") && ((BlackKing == 0 && BlackPrince > 0) || (!strcmp(Choice, "Tenjiku") || !strcmp(Choice, "Maka") || !strcmp(Choice, "Tai"))) ) { Response = MsgBox((sprintf(StringTmp, "You have left your %s in Check! ", Pieces[abs(OriginalPiece)].Name), StringTmp), 0, "Black"); Response = 6; }
		if(OriginalPiece < 0 && !strcmp(Turn, "Black") && strcmp(Computer, "White") && strcmp(Computer, "Both") && ((WhiteKing == 0 && WhitePrince > 0) || (!strcmp(Choice, "Tenjiku") || !strcmp(Choice, "Maka") || !strcmp(Choice, "Tai"))) ) { Response = MsgBox((sprintf(StringTmp, "You have left your %s in Check! ", Pieces[abs(OriginalPiece)].Name), StringTmp), 0, "White"); Response = 6; }
		if(strcmp(Choice, "Tenjiku") && strcmp(Choice, "Maka") && strcmp(Choice, "Tai") ) {
		    if(OriginalPiece > 0 && !strcmp(Turn, "White") && strcmp(Computer, "Black") && strcmp(Computer, "Both") && (BlackKing == 1 || BlackPrince == 0) ) { Response = MsgBox((sprintf(StringTmp, "You can't leave your %s in Check! ", Pieces[abs(OriginalPiece)].Name), StringTmp), 0, "Black"); Response = 7; }
		    if(OriginalPiece < 0 && !strcmp(Turn, "Black") && strcmp(Computer, "White") && strcmp(Computer, "Both") && (WhiteKing == 1 || WhitePrince == 0) ) { Response = MsgBox((sprintf(StringTmp, "You can't leave your %s in Check! ", Pieces[abs(OriginalPiece)].Name), StringTmp), 0, "White"); Response = 7; }
		}
		Legal[SeeFile][SeeRank] = 0;
		CheckLooked = 1;
		if(Response != 7 ) {
		    if((OriginalPiece > 0 && (BlackKing == 1 || BlackPrince == 0)) || (OriginalPiece < 0 && (WhiteKing == 1 || WhitePrince == 0)) && strcmp(Choice, "Tenjiku") && strcmp(Choice, "Maka") && strcmp(Choice, "Tai") ) {
			if(OriginalPiece < 0 ) strcpy(Turn2, "White"); else strcpy(Turn2, "Black");
			LookForMate();
		    }
		    SeeMove = OldSeeMove;
		    if(!strcmp(Choice, "Tenjiku") || !strcmp(Choice, "Maka") || !strcmp(Choice, "Tai") ) { BestTally[0] = 0; strcpy(Turn2, Turn); }
		    if(BestTally[0] == -999999 ) {
			if(!strcmp(Turn2, "Black") ) strcpy(Board.PieceID.Caption, "Checkmate!  White wins."); else strcpy(Board.PieceID.Caption, "Checkmate!  Black wins.");
			Notice = 5; BestTally[0] = -99999;
			GameOver = 1;
			return;
		    } else {
			if(Response != 6 ) {
			    sprintf(Board.PieceID.Caption, "%s %s is in Check!", Turn2, Pieces[abs(OriginalPiece)].Name); 
			    Notice = 1;
			}
			Checked = 1;
			return;
		    }
		} else {
		    Checked = 2;
		    SeeMove = OldSeeMove;
		}
	    } else {
	      if(CheckTest != 1 ) {
		L = 0;
		if(!strcmp(Choice, "Heian") || !strcmp(Choice, "Chu") || !strcmp(Choice, "Dai") || !strcmp(Choice, "Tenjiku") ) L = 1;
		if(Squares[InitFile][InitRank] > 0 ) {
		    BlackInfluence = 1;
		    Board.ForeColor = 0x606060;
		} else {
		    WhiteInfluence = 1;
		    Board.ForeColor = 0xFFFFFF;
		}
		Legal[SeeFile][SeeRank] = 0;
		for(EF = 1; EF < Pixels; EF++) { // FIXME: lowered EF by 1 to access correct PieceMask elements ???
		    if(Reverse == 0 ) {
			if(Squares[InitFile][InitRank] > 0 ) {
			   Board.Line(XStart + (InitFile - 1) * Pixels + EF /*- 2*/ + L, 11 + (InitRank - 1) * Pixels + L, XStart + (InitFile - 1) * Pixels + EF /*- 2*/ + L, 11 + (InitRank - 1) * Pixels + PieceMask[Pieces[abs(Squares[InitFile][InitRank])].Mask][EF] + 1, NOCOLOR, ""); // FIXME: added 2 to x
			} else {
			   Board.Line(XStart + (InitFile - 1) * Pixels + (Pixels - EF) + L, 9 + (InitRank * Pixels) + L + 1, XStart + (InitFile - 1) * Pixels + (Pixels - EF) + L, 8 + (InitRank * Pixels) - (PieceMask[Pieces[abs(Squares[InitFile][InitRank])].Mask][EF] - (L * 2)) + 1, NOCOLOR, ""); // FIXME: added 1 to y
			}
		    } else {
			if(Squares[InitFile][InitRank] > 0 ) {
			   Board.Line(XStart + (BoardSizeX - InitFile) * Pixels + (Pixels - EF) + L, 8 + (BoardSizeY - InitRank + 1) * Pixels + 1 + L + 1, XStart + (BoardSizeX - InitFile) * Pixels + (Pixels - EF) + L, 8 + (BoardSizeY - InitRank + 1) * Pixels - PieceMask[Pieces[abs(Squares[InitFile][InitRank])].Mask][EF] + (L * 2) + 1, NOCOLOR, ""); // FIXME: added 1 to y2
			} else {
			   Board.Line(XStart + (BoardSizeX - InitFile) * Pixels + EF /*- 2*/ + L, 10 + (BoardSizeY - InitRank) * Pixels + 1 + L, XStart + (BoardSizeX - InitFile) * Pixels + EF /*- 2*/ + L, 12 + (BoardSizeY - InitRank) * Pixels + PieceMask[Pieces[abs(Squares[InitFile][InitRank])].Mask][EF], NOCOLOR, ""); // FIXME: added 2 to x
			}
		    }
		} // Next EF;
	      }
	    }
	    Legal[SeeFile][SeeRank] = 0;
	    Board.ForeColor = 0x0L;
	    Board.FillColor = 0xFFFFFF;
	}
     }
    }
   }
  }
 }
}

void Main ()
{
  int Response; // read-only?
// TODO: initialize screen params
//  if(screen.Width / screen.TwipsPerPixelY > 650 ) Display = 800; else Display = 640;
//  if(screen.Width / screen.TwipsPerPixelY > 810 ) Display = 1240;
  LoadStart();
  if(Start.ScaleWidth > 640 ) {
    UnloadStart();
    Response = MsgBox("This program will not run with LARGE FONTS. You must change your display settings to SMALL FONTS before running the program.", 0, "Shogi Variants 1.55a");
  }
}

void MakeDrop ()
{
  INT DD;
  File = CompLegal[BestMove].EndFile;
  Rank = CompLegal[BestMove].EndRank;
  for(DD = 1; DD <= Capture * 2; DD++) {
    if(CompLegal[BestMove].EndPiece == CapRef[DD] ) I = DD;
  } // Next DD;
  HeldDrop();
}

void MakeMap ()
{
  INT FF, GG;
  for(FF = 1; FF <= BoardSizeY; FF++) {
    for(GG = 1; GG <= BoardSizeX; GG++) {
	Attacker[GG][FF] = 0; OldAttack[GG][FF] = 0;
	BanMap[GG][FF].WhiteNum = 0; BackMap[GG][FF].WhiteNum = 0;
	BanMap[GG][FF].BlackNum = 0; BackMap[GG][FF].BlackNum = 0;
	BanMap[GG][FF].WhiteValue = 0; BackMap[GG][FF].WhiteValue = 0;
	BanMap[GG][FF].BlackValue = 0; BackMap[GG][FF].BlackValue = 0;
    } // Next GG;
  } // Next FF;
  FirstTime = 1;
  for(FF = 1; FF <= BoardSizeY; FF++) {
    for(GG = 1; GG <= BoardSizeX; GG++) {
	if(Squares[GG][FF] != 0 ) {
	    InitFile = GG; InitRank = FF;
	    Validate();
	}
    } // Next GG;
  } // Next FF;
  FirstTime = 0;
}

void MicroAdd ()
{
  if((MicroCap > 4 && MicroCap < 9) || MicroCap > 12 ) {
   InHand[MicroCap - 4] = InHand[MicroCap - 4] + 1;
   Board.HandPic[MicroCap - 4].Visible = True;
   if(InHand[MicroCap - 4] > 1 ) sprintf(Board.Held[MicroCap - 4].Caption, "%d", InHand[MicroCap - 4]);
  } else {
   InHand[MicroCap + 4] = InHand[MicroCap + 4] + 1;
   Board.HandPic[MicroCap + 4].Visible = True;
   if(InHand[MicroCap + 4] > 1 ) sprintf(Board.Held[MicroCap + 4].Caption, "%d", InHand[MicroCap + 4]);
  }
}

void MicroDrop ()
{
  if((I < 5) || (I > 8 && I < 13) ) {
    InHand[I + 4] = InHand[I + 4] - 1;
    if(InHand[I + 4] < 1 ) Board.HandPic[I + 4].Visible = False;
    if(InHand[I + 4] < 2 ) strcpy(Board.Held[I + 4].Caption, ""); else sprintf(Board.Held[I - 4].Caption, "%d", InHand[I - 4]);
  } else {
    InHand[I - 4] = InHand[I - 4] - 1;
    if(InHand[I - 4] < 1 ) Board.HandPic[I - 4].Visible = False;
    if(InHand[I - 4] < 2 ) strcpy(Board.Held[I - 4].Caption, ""); else sprintf(Board.Held[I - 4].Caption, "%d", InHand[I - 4]);
  }
}

void Move2 ()
{
  INT K, L;
  for(K = 1; K <= BoardSizeY; K++) {
    for(L = 1; L <= BoardSizeX; L++) {
	if(Grafix[L][K] == I ) {
	    InitFile = L;
	    InitRank = K;
	}
    } // Next L;
  } // Next K;
  DRAG(Board.showpic[Grafix[InitFile][InitRank]], 1);
  strcpy(Board.PieceID.Caption, Pieces[abs(Squares[InitFile][InitRank])].Name);
}

void MoveFormDrop ()
{
  if(NewX > XStart && NewX < XStart + (BoardSizeX * Pixels) && NewY > 11 && NewY < 11 + (BoardSizeY * Pixels) ) {
    GetSquare2();
    Squares[File][Rank] = Squares[InitFile][InitRank];
    Squares[InitFile][InitRank] = 0;
    Grafix[File][Rank] = I;
    Grafix[InitFile][InitRank] = -1;
    if(Reverse == 0 ) {
	MOVE(1, Board.showpic[I], XStart + ((File - 1) * Pixels), 11 + ((Rank - 1) * Pixels));
    } else {
	MOVE(1, Board.showpic[I], XStart + ((BoardSizeX - File) * Pixels), 11 + ((BoardSizeY - Rank) * Pixels));
    }
    ClearLegal();
  }
}

void MovePicDrop ()
{
  STRING VictimSTR;
  INT K, L;
  for(K = 1; K <= BoardSizeY; K++) {
    for(L = 1; L <= BoardSizeX; L++) {
	File = L; Rank = K;
	if(Grafix[L][K] == NewIndex ) {
	    if((L != InitFile) || (K != InitRank) ) {
		Board.showpic[I].Visible = False;
		Grafix[L][K] = I;
		Grafix[InitFile][InitRank] = -1;
		Board.showpic[NewIndex].Visible = False;
		MOVE(1, Board.showpic[NewIndex], 0, 0);
		if(Reverse == 0 ) {
		    MOVE(1, Board.showpic[I], XStart + ((L - 1) * Pixels), 11 + ((K - 1) * Pixels));
		} else {
		    MOVE(1, Board.showpic[I], XStart + ((BoardSizeX - L) * Pixels), 11 + ((BoardSizeY - K) * Pixels));
		}
		Board.showpic[I].Visible = True;
		strcpy(VictimSTR, Pieces[abs(Squares[File][Rank])].Name);
		if(Squares[File][Rank] > 0 && (!strcmp(VictimSTR, "Prince") || !strcmp(VictimSTR, "Crown Prince")) ) BlackPrince = BlackPrince - 1;
		if(Squares[File][Rank] < 0 && (!strcmp(VictimSTR, "Prince") || !strcmp(VictimSTR, "Crown Prince")) ) WhitePrince = WhitePrince - 1;
		Squares[L][K] = Squares[InitFile][InitRank];
		Squares[InitFile][InitRank] = 0;
	    }
	}
    } // Next L;
  } // Next K;
}

void MovePieces ()
{ // Menu function. (Calls EndSetup() before.)
  Board.Refresh();
  strcpy(Board.Caption, "Move Pieces with Left Mouse Button  (Press Right Mouse Button to Finish)");
  MovePiece = 1;
  strcpy(Board.BlackClock.Caption, "00:00:00");
  strcpy(Board.WhiteClock.Caption, "00:00:00");
  Board.Timer1.Enabled = False;
}

void NextTurn ()
{
  if(CompMove == 1 ) {
    NextTurn2();
  } else {
    if(Notice < 1 ) strcpy(Board.Caption, Cap);
    Board.MnuHandicap.Enabled = False;
    EnableNamedMenuItem("Setup.Handicap", False); // [HGM] added
    if((!strcmp(Choice, "Micro") || !strcmp(Choice, "DaiDai") || !strcmp(Choice, "Maka") || !strcmp(Choice, "Tai")) && Taken == 1 ) Promessage();
    if(!strcmp(Choice, "Chu") ) {
	if(Squares[File][Rank] == 2 && (Rank == 2 || Rank == 3) ) NoPro = 1;
	if(Squares[File][Rank] == -2 && (Rank == 10 || Rank == 11) ) NoPro = 1;
	if(Squares[File][Rank] == LastBlack && Taken != 1 ) {
	    NoPro = 1;
	    LastBlack = 0;
	}
	if(Squares[File][Rank] == LastWhite && Taken != 1 ) {
	    NoPro = 1;
	    LastWhite = 0;
	}
	if(Squares[File][Rank] > 0 ) LastBlack = 0; else LastWhite = 0;
    }
    if(Dropped != 1 && NoPro != 1 && strcmp(Choice, "Micro") ) {
	if(Squares[File][Rank] > 0 && (Rank <= PromDotY || InitRank <= PromDotY) ) Promessage();
	if(Squares[File][Rank] < 0 && (Rank > BoardSizeY - PromDotY || InitRank > BoardSizeY - PromDotY) ) Promessage();
	if(LionPro == 1 ) Promessage();
    }
    if(Eval == 1 ) { AddTally(); sprintf(Board.Caption, "%-90sEvaluation: %ld", Cap, BestTally[1]); }
    UpdateCaptions(); // [HGM] added
    NextTurn2();
  }
}

void NextTurn2 ()
{
  INT OrigSeeMove;
  LionVictim.Piece = 0;
  if(!strcmp(Computer, Turn) ) SeeMove = FirstSeeMove;
  if(Eval == 1 ) sprintf(Board.Caption, "%-90sEvaluation: %d", Cap, Int(BestTally[1]));
  MakeMove = 0; Dropped = 0; NoPro = 0; LionPro = 0; CompMove = 0; Evaluate = 0;
  SetKings();
  if(!strcmp(Turn, "Black") ) {
    MoveCount = MoveCount + 1;
    strcpy(Turn, "White");
    WhiteLion = 0;
    Board.MnuNextBlack.Enabled = True;
    Board.MnuNextWhite.Enabled = False;
    Board.MnuNextBlack.Checked = False;
    Board.MnuNextWhite.Checked = True;
  } else {
    strcpy(Turn, "Black");
    BlackLion = 0;
    Board.MnuNextBlack.Enabled = False;
    Board.MnuNextWhite.Enabled = True;
    Board.MnuNextBlack.Checked = True;
    Board.MnuNextWhite.Checked = False;
  }
  if(Ligui != 1 ) Convert();
  TurnCount = TurnCount + 1; EndTurn = TurnCount;
  Captures[TurnCount].number = 0;
  InitFile = 0; InitRank = 0; Teach = 0; Taken = 0; Ligui = 0;
  sprintf(Board.LastMove.Caption, "%d. %s", MoveCount, ShortScore[TurnCount]); // Format$(MoveCount) + ". " + ShortScore[TurnCount]
  strcpy(Score[TurnCount].Caption, ShortScore[TurnCount]); strcpy(CMoveSTR, "");
  Board.Refresh();
  if((!strcmp(Turn, "White") && Squares[File][Rank] == 1 && BlackPrince == 0) || (!strcmp(Turn, "Black") && Squares[File][Rank] == -1 && WhitePrince == 0) ) TwoKings();
  if(Checked != 2 ) {
    OrigSeeMove = SeeMove; SeeMove = 1;
    LookCheck();
    SeeMove = OrigSeeMove;
  }
  if(GameOver == 1 ) {
    sprintf(Score[TurnCount].Caption, "%d. %s mate", MoveCount, ShortScore[TurnCount]); // Format$(MoveCount) + ". " + ShortScore[TurnCount] + " mate"
    sprintf(Board.LastMove.Caption, "%d. %s mate", MoveCount, ShortScore[TurnCount]); //  Format$(MoveCount) + ". " + ShortScore[TurnCount] + " mate"
  }
  if(Checked == 2 ) TakeBack();
  if(PawnMate == 1 ) TakeBack();
  Checked = 0; CheckTest = 0; CheckLooked = 0; PawnMate = 0; Mate = 0;
  if((Backwards == 1) || (strcmp(Choice, "Chu") && strcmp(Choice, "Dai") && strcmp(Choice, "Tenjiku") && strcmp(Choice, "Maka") && strcmp(Choice, "DiaDai") && strcmp(Choice, "Tai")) )   BugFix();
  if(BlackEmperor == 1 && WhiteEmperor == 1 ) EmperorCheck();
  if(ShowLast == 1 && strcmp(Computer, Turn) ) FlashPiece();
  if(!strcmp(Computer, Turn) ) {
    FirstSeeMove = SeeMove;
    SeeMove = 1;
    CompMain();
  }
}

void NoCompTeach ()
{
  Board.MnuVer2.Enabled = True;
  Board.MnuVer1.Enabled = False;
  Board.MnuVer1.Checked = True;
  Board.MnuVer2.Checked = False;
  TeachVer = 1;
}

void Notation ()
{ // Menu function. Notate = 0 (on) or 1 (off)
  if(Notate == 0 ) {
    Board.NotTop.Visible = False;
    Board.NotSide.Visible = False;
  } else {
    Board.NotTop.Visible = True;
    Board.NotSide.Visible = True;
  }
  if(Board.MnuNotOn.Enabled == False ) Board.MnuNotOn.Enabled = True; else Board.MnuNotOn.Enabled = False;
  if(Board.MnuNotOff.Enabled == False ) Board.MnuNotOff.Enabled = True; else Board.MnuNotOff.Enabled = False;
  if(Board.MnuNotOn.Checked == False ) Board.MnuNotOn.Checked = True; else Board.MnuNotOn.Checked = False;
  if(Board.MnuNotOff.Checked == False ) Board.MnuNotOff.Checked = True; else Board.MnuNotOff.Checked = False;
}

void NotSet ()
{
  INT K;
  Board.AutoRedraw = True;
  for(K = 1; K <= BoardSizeX; K++) {
    Board.CurrentX = (XStart - 3) + ((K - 1) * Pixels) + Int(Pixels / 3);
    Board.CurrentY = 0;
    if(strcmp(Choice, "Chu") && strcmp(Choice, "Heian") && strcmp(Choice, "Dai") && strcmp(Choice, "Tenjiku") ) {
	Board.CurrentX = Board.CurrentX - 1;
	Board.CurrentY = Board.CurrentY - 1;
    }
    if(Reverse == 0 ) Board.Print(NumSTR(BoardSizeX - K + 1)); else Board.Print(NumSTR(K));
    Board.CurrentX = (XStart + 2) + (BoardSizeX * Pixels);
    Board.CurrentY = 11 + (K - 1) * Pixels + Int(Pixels / 3);
    if(strcmp(Choice, "Chu") && strcmp(Choice, "Heian") && strcmp(Choice, "Dai") && strcmp(Choice, "Tenjiku") ) {
	Board.CurrentX = Board.CurrentX - 1;
	Board.CurrentY = Board.CurrentY - 1;
    }
    if(K <= BoardSizeY ) {
	if(Reverse == 0 ) Board.Print(CharSTR(96 + K)); else Board.Print(CharSTR(96 + (BoardSizeY - K + 1)));
    }
  } // Next K;
  if(!strcmp(Choice, "Yari") ) {
    Board.CurrentX = (XStart + 1) + (BoardSizeX * Pixels);
    Board.CurrentY = 11 + 7 * Pixels + Int(Pixels / 3);
    if(Reverse == 0 ) Board.Print(CharSTR(104)); else Board.Print(CharSTR(98));
    Board.CurrentX = (XStart + 1) + (BoardSizeX * Pixels);
    Board.CurrentY = 11 + 8 * Pixels + Int(Pixels / 3);
    if(Reverse == 0 ) Board.Print(CharSTR(105)); else Board.Print(CharSTR(97));
  }
  if(!strcmp(Choice, "Micro") ) {
    Board.CurrentY = 270;
    Board.CurrentX = 441;
    if(Reverse == 0 ) Board.Print(CharSTR(101)); else Board.Print(CharSTR(97));
  }
  Board.AutoRedraw = False;
}

void PawnMates ()
{
  INT SS, TT;
  if(CompLegal[AA].EndPiece != 0 ) {
    if((!strcmp(Pieces[abs(CompLegal[AA].EndPiece)].Name, "Pawn") || !strcmp(Pieces[abs(CompLegal[AA].EndPiece)].Name, "Sparrow Pawn") || !strcmp(Pieces[abs(CompLegal[AA].EndPiece)].Name, "Swallow") || !strcmp(Pieces[abs(CompLegal[AA].EndPiece)].Name, "Dolphin")) && CompLegal[AA].StartFile == 0 && strcmp(Choice, "Micro") && strcmp(Choice, "Yari") ) {
	if(!strcmp(Turn, "Black") && CompLegal[AA].EndRank > 1 ) {
	    if(Squares[CompLegal[AA].EndFile][CompLegal[AA].EndRank - 1] == -1 ) {
		PawnMate = 1;
		for(SS = CompLegal[AA].EndRank - 2; SS <= CompLegal[AA].EndRank; SS++) {
		    for(TT = CompLegal[AA].EndFile - 1; TT <= CompLegal[AA].EndFile + 1; TT++) {
			if(TT > 0 && TT <= BoardSizeX && SS > 0 && SS <= BoardSizeY && (TT != CompLegal[AA].EndFile || SS != CompLegal[AA].EndRank) ) {
			    if(BanMap[TT][SS].BlackNum == 0 && Squares[TT][SS] >= 0 ) PawnMate = 0;
			}
		    } // Next TT;
		} // Next SS;
		if(PawnMate == 1 ) WhiteTally = WhiteTally + 999999;
	    }
	}
	if(!strcmp(Turn, "White") && CompLegal[AA].EndRank < BoardSizeY ) {
	    if(Squares[CompLegal[AA].EndFile][CompLegal[AA].EndRank + 1] == 1 ) {
		PawnMate = 1;
		for(SS = CompLegal[AA].EndRank; SS <= CompLegal[AA].EndRank + 2; SS++) {
		    for(TT = CompLegal[AA].EndFile - 1; TT <= CompLegal[AA].EndFile + 1; TT++) {
			if(TT > 0 && TT <= BoardSizeX && SS > 0 && SS <= BoardSizeY && (SS != CompLegal[AA].EndRank || TT != CompLegal[AA].EndFile) ) {
			    if(BanMap[TT][SS].WhiteNum == 0 && Squares[TT][SS] <= 0 ) PawnMate = 0;
			}
		    } // Next TT;
		} // Next SS;
		if(PawnMate == 1 ) BlackTally = BlackTally + 999999;
	    }
	}
	PawnMate = 0;
    }
  }
}

void PicDown ()
{ // Button1-Down on board piece. CCC = 1, I = piece number, ClickPiece = 1, NewButton = button
  STRING PieceName;
  INT K, L, Found=0; // was uninitialized?
  if(strcmp(Computer, Turn) && strcmp(Computer, "Both") && Level != 0 && GameOver != 1 ) {
   Notice = 0;
   Board.PieceID.ForeColor = 0xFF0000;
   if(Handicap == 1 || Reduce == 1 || Selection != 0 || MovePiece == 1 ) {
    RemovePiece();
   } else {
    if(LionPiece == -1 ) strcpy(Board.Caption, Cap);
    if(LionPiece == I || LionPiece == -1 ) {
	K = 1;
	do {
	    L = 1;
	    do {
		if(Grafix[L][K] == I ) {
		    strcpy(PieceName, Pieces[abs(Squares[L][K])].Name);
		    strcpy(Board.PieceID.Caption, PieceName);
		    InitFile = L;
		    InitRank = K;
		    Found = 1;
		}
		L = L + 1;
	    } while( !(L > BoardSizeX || Found == 1) );
	    K = K + 1;
	} while( !(K > BoardSizeY || Found == 1) );
	Found = 0;
	if((Squares[InitFile][InitRank] > 0 && !strcmp(Turn, "Black")) || (Squares[InitFile][InitRank] < 0 && !strcmp(Turn, "White")) ) {
	    DRAG(Board.showpic[I], 1);
	    if(!strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "Emperor") ) Emperor(); else Validate();
	} else {
	    Board.FillStyle = 1; Other = 1;
	    Board.ForeColor = 0xC00000;
	    Validate();
	    Board.ForeColor = 0x0;
	    Board.FillStyle = 0; Other = 0;
	}
     } else {
	Board.PieceID.ForeColor = 0xFFL;
	sprintf(Board.PieceID.Caption, "You must move your %s !", LionNameSTR);
	Notice = 1;
     }
   }
  }
}

void PicDrop ()
{ // DragDrop handler for pieces. I = piece number
  INT K, L, Found;
  int Response; // write-only?
  if(ShowLast == 1 ) Board.Refresh();
  Found = 0;
  if(InitRank == 0 ) ClearLegal();
  if(InitRank != 0 && MovePiece != 1 ) {
  K = 1;
  do {
    L = 1;
    do {
	if(Grafix[L][K] == NewIndex ) {
	    Found = 1; File = L; Rank = K;
	    if(Legal[L][K] > 0 ) {
		if(LionPiece != I ) {
		    Score[TurnCount].IDStart = Squares[InitFile][InitRank];
		    Score[TurnCount].PosStart = (InitRank * (BoardSizeX + 1)) + InitFile;
		    strcat(CMoveSTR, TrimSTR(Pieces[abs(Squares[InitFile][InitRank])].sname));
		    sprintf(CMoveSTR + strlen(CMoveSTR), "%d%c", ((BoardSizeX - InitFile) + 1), (96 + InitRank));
		    if((!strcmp(Computer, Turn) || !strcmp(Computer, "Both")) && LionVictim.Piece != 0 ) {
			sprintf(CMoveSTR + strlen(CMoveSTR), "x%d%c", ((BoardSizeX - LionVictim.File) + 1), (96 + LionVictim.Rank));
		    }
		}
		if(!strcmp(Choice, "Chu") && !strcmp(Pieces[abs(Squares[L][K])].Name, "Lion") && strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "Lion") ) {
		    if(!strcmp(Turn, "Black") ) BlackLion = 1; else WhiteLion = 1;
		}
		if(Legal[L][K] == 1 ) LionPiece = -1;
		Board.Frame.Visible = False;
		if(Legal[L][K] == 4 ) {
		    Score[TurnCount].IDEnd = Squares[InitFile][InitRank];
		    Score[TurnCount].PosEnd = Score[TurnCount].PosStart;
		    strcat(CMoveSTR, "x!");
		} else {
		    Score[TurnCount].IDEnd = Squares[File][Rank];
		    Score[TurnCount].PosEnd = (Rank * (BoardSizeX + 1)) + File;
		    Board.showpic[I].Visible = False;
		    Grafix[L][K] = I;
		    if(InitFile != File || InitRank != Rank ) Grafix[InitFile][InitRank] = -1;
		}
		if(Legal[L][K] != 4 ) {
		    if((InitFile != File || InitRank != Rank) ) strcat(CMoveSTR, "x"); else strcat(CMoveSTR, "-");
		}
		sprintf(CMoveSTR + strlen(CMoveSTR), "%d%c", ((BoardSizeX - File) + 1), (Rank + 96));
		Captures[TurnCount].number = Captures[TurnCount].number + 1;
		Captures[TurnCount].Positions[Captures[TurnCount].number] = (Rank * (BoardSizeX + 1)) + File;

		Captures[TurnCount].PieceNum[Captures[TurnCount].number] = Squares[File][Rank];
		if(LionVictim.Piece != 0 ) {
		    Captures[TurnCount].number = Captures[TurnCount].number + 1;
		    Captures[TurnCount].Positions[Captures[TurnCount].number] = (LionVictim.Rank * (BoardSizeX + 1)) + LionVictim.File;
		    Captures[TurnCount].PieceNum[Captures[TurnCount].number] = Squares[LionVictim.File][LionVictim.Rank];
		}
		Board.showpic[NewIndex].Visible = False;
		MOVE(1, Board.showpic[NewIndex], 0, 0);
		if(abs(Squares[L][K]) == 1 || !strcmp(Pieces[abs(Squares[L][K])].Name, "Emperor") || !strcmp(Pieces[abs(Squares[L][K])].Name, "Crown Prince") || !strcmp(Pieces[abs(Squares[L][K])].Name, "Prince") ) CheckMate();
		Taken = 1;
		if(Drop == 1 ) ChangeSides();
		if(Legal[File][Rank] == 3 ) {
		    Score[TurnCount].IDEnd = 0;
		    Grafix[File][Rank] = -1;
		    MOVE(1, Board.showpic[I], 0, 0);
		    Squares[InitFile][InitRank] = 0;
		    Squares[File][Rank] = 0;
		    NoPro = 1;
		 } else {
		    if(Legal[File][Rank] != 4 ) {
			Squares[File][Rank] = Squares[InitFile][InitRank];
			if(Squares[File][Rank] == 1 ) { BlackKingX = File; BlackKingY = Rank; }
			if(Squares[File][Rank] == -1 ) { WhiteKingX = File; WhiteKingY = Rank; }
			if(!strcmp(Pieces[abs(Squares[File][Rank])].Name, "Emperor") && Squares[File][Rank] > 0 ) { BlackEmpX = File; BlackEmpY = Rank; }
			if(!strcmp(Pieces[abs(Squares[File][Rank])].Name, "Emperor") && Squares[File][Rank] < 0 ) { WhiteEmpX = File; WhiteEmpY = Rank; }
			if(Reverse == 0 ) {
			    MOVE(1, Board.showpic[I], XStart + ((File - 1) * Pixels), 11 + ((Rank - 1) * Pixels));
			} else {
			    MOVE(1, Board.showpic[I], XStart + ((BoardSizeX - File) * Pixels), 11 + ((BoardSizeY - Rank) * Pixels));
			}
			Board.showpic[I].Visible = True;
			if(InitFile != File || InitRank != Rank ) Squares[InitFile][InitRank] = 0;
			if(LionVictim.Piece != 0 ) {
			    Squares[LionVictim.File][LionVictim.Rank] = 0;
			    Board.showpic[Grafix[LionVictim.File][LionVictim.Rank]].Visible = False;
			    MOVE(1, Board.showpic[Grafix[LionVictim.File][LionVictim.Rank]], 0, 0);
			    Grafix[LionVictim.File][LionVictim.Rank] = -1;
			}
		    }
		}
		if(Legal[L][K] == 4 ) { Squares[L][K] = 0; Grafix[L][K] = -1; File = InitFile; Rank = InitRank; }
		if(Demon == 1 ) Flame();
		if(Legal[L][K] == 3 ) strcat(CMoveSTR, "*");
		if(Legal[L][K] == 6 ) Teach = 3;
		if(Legal[L][K] == 2 ) Teach = 2; else Teach = 1;
		if(Mate != 1 ) {
		    if(Legal[L][K] == 2 || Legal[L][K] == 6 ) {
			DoubleMove();
		    } else {
			LastPieceX = File; LastPieceY = Rank;
			if(CompMove != 1 ) NextTurn();
		    }
		}
	    }
	}
	L = L + 1;
    } while( !(L > BoardSizeX || Found == 1) );
    K = K + 1;
  } while( !(K > BoardSizeY || Found == 1) );
  Found = 0;
  ClearLegal();
  if(Mate == 1 ) { GameOver = 1; Response = MsgBox("        Checkmate!          ", 0, (sprintf(StringTmp, "%s Wins!", Turn), StringTmp)); NextTurn(); }
  } else {
    if(MovePiece == 1 ) MovePicDrop();
  }
}

void PrintScore ()
{ // Menu function.
#if 0
  Printer.Print((sprintf(StringTmp, "%s Shogi -  Game Record\n", GameName), StringTmp));
  Printer.Print ("____________________________________\n");
  Printer.Print("\n");

  PrintMax = 4;
  PrintInc = 20;
  if(!strcmp(Choice, "Chu") || !strcmp(Choice, "Dai") || !strcmp(Choice, "Maka") || !strcmp(Choice, "DaiDai") || !strcmp(Choice, "Tai") ) {
    PrintMax = 3;
    PrintInc = 27;
  }
  if(!strcmp(Choice, "Tenjiku") ) {
    PrintMax = 2;
    PrintInc = 40;
  }
  K = 0; J = 0;
  for(W = 1; W <= TurnCount; W += 2) {
    K = K + 1; J = J + 1;
    strcpy(FirstScore, Score[W].Caption);
    strcpy(SecondScore, Score[W + 1].Caption);
    if(strstr(FirstScore, " ") ) TenjikuScore1();
    if(strstr(SecondScore, " ") ) TenjikuScore2();
    if(HandGame == 1 && W == 1 ) {
      Printer.Print("1.  -  %s", SecondScore, Tab(PrintInc)); // TODO tab to desired column
    } else {
      Printer.Print("%d. %s %s", K, FirstScore, SecondScore, Tab(J * PrintInc));
    }
    if(J == PrintMax ) { Printer.Print("\n"); J = 0; }
  } // Next W;
  strcpy(Board.Caption, "Printing..."); Notice = 1;
  Printer.EndDoc;
#endif
}

void Promessage ()
{
  STRING KSTR, LSTR, MessageSTR;
  int Response;
  ClearLegal();
  ForceProm = 0; Taken = 0;
  if(!strcmp(Choice, "Micro") || !strcmp(Choice, "DaiDai") || !strcmp(Choice, "Maka") || !strcmp(Choice, "Tai") ) ForceProm = 1;
  if(Pieces[abs(Squares[File][Rank])].special == '1' ) {
    if(Squares[File][Rank] > 0 && Rank == 1 ) ForceProm = 1;
    if(Squares[File][Rank] < 0 && Rank == BoardSizeY ) ForceProm = 1;
  }
  if(Pieces[abs(Squares[File][Rank])].special == '2' ) {
    if(Squares[File][Rank] > 0 && Rank < 3 ) ForceProm = 1;
    if(Squares[File][Rank] < 0 && Rank > BoardSizeY - 2 ) ForceProm = 1;
  }
  if(Prom == 0 && ForceProm != 1 ) {
    if(Pieces[abs(Squares[File][Rank])].Promotes > 0 ) {
	if(AutoPromote != 1 ) {
	    strcpy(LSTR, Pieces[Pieces[abs(Squares[File][Rank])].Promotes].Name);
	    strcpy(KSTR, Pieces[abs(Squares[File][Rank])].Name);
	    sprintf(MessageSTR, "Promote to %s ?", LSTR);
	    Response = MsgBox(MessageSTR, 36, KSTR);
	    if(Response == 6 ) {
		Promote();
	    } else {
		strcat(CMoveSTR, "=");
		if(!strcmp(Turn, "White") && InitRank < 9 ) LastWhite = Squares[File][Rank];
		if(!strcmp(Turn, "Black") && InitRank > 4 ) LastBlack = Squares[File][Rank];
	    }
	} else {
	    Promote();
	}
    }
  }
  if(Prom == 1 || ForceProm == 1 ) {
    if(Pieces[abs(Squares[File][Rank])].Promotes > 0 ) {
	Board.PieceID.ForeColor = 0x8000L;
	strcpy(KSTR, Pieces[abs(Squares[File][Rank])].Name);
	sprintf(Board.PieceID.Caption, "%s promotes", KSTR);
	Notice = 1;
	Promote();
    }
  }
}

void Promote ()
{
  if(Reverse == 0 ) PromGraf = (TotGraph / 2) - 1; else PromGraf = -1;
  if(Squares[File][Rank] < 0 && Pieces[abs(Squares[File][Rank])].Promotes != 0 ) {
//    if(!strcmp(Choice, "Tai") ) {
//	Board.showpic[I].Picture = TaiPieces.Pix[PromGraf + Pieces[abs(Squares[File][Rank])].PrGraphic].Picture;
//    } else {
	Board.showpic[I].Picture = Board.Pix[PromGraf + Pieces[abs(Squares[File][Rank])].PrGraphic].Picture;
//    }
    strcpy(PromPieceSTR, Pieces[abs(Squares[File][Rank])].Name);
    if(!strcmp(PromPieceSTR, "Drunk Elephant") ) WhitePrince = WhitePrince + 1;
    if(!strcmp(PromPieceSTR, "King") ) {
	WhiteEmperor = 1; WhiteEmpX = File; WhiteEmpY = Rank;
    }
    Squares[File][Rank] = 0 - Pieces[abs(Squares[File][Rank])].Promotes;
    Score[TurnCount].IDEnd = Squares[File][Rank];
    Board.showpic[I].Visible = True;
    strcat(CMoveSTR, "+");
    if(AutoPromote == 1 && ForceProm != 1 ) AutoMessage();
  }
  if(Reverse == 1 ) PromGraf = (TotGraph / 2) - 1; else PromGraf = -1;
  if(Squares[File][Rank] > 0 && Pieces[abs(Squares[File][Rank])].Promotes != 0 ) {
//    if(!strcmp(Choice, "Tai") ) {
//	Board.showpic[I].Picture = TaiPieces.Pix[PromGraf + Pieces[Squares[File][Rank]].PrGraphic].Picture;
//    } else {
	Board.showpic[I].Picture = Board.Pix[PromGraf + Pieces[Squares[File][Rank]].PrGraphic].Picture;
//    }
    strcpy(PromPieceSTR, Pieces[abs(Squares[File][Rank])].Name);
    if(!strcmp(PromPieceSTR, "Drunk Elephant") ) BlackPrince = BlackPrince + 1;
    if(!strcmp(PromPieceSTR, "King") ) {
	BlackEmperor = 1; BlackEmpX = File; BlackEmpY = Rank;
    }
    Squares[File][Rank] = Pieces[abs(Squares[File][Rank])].Promotes;
    Score[TurnCount].IDEnd = Squares[File][Rank];
    Board.showpic[I].Visible = True;
    strcat(CMoveSTR, "+");
    if(AutoPromote == 1 && ForceProm != 1 ) AutoMessage();
  }
}

void RangeJump ()
{
  M = 64; Range = 1;
  SingleStep();
}

void ReduceHand ()
{
  INT Z, OldPiece;
  if(Replaying == 1 ) {
    CaptPiece = Score[TurnCount].IDEnd;
    OldPiece = Squares[InitFile][InitRank];
    Squares[InitFile][InitRank] = 0 - CaptPiece;
  } else {
   if(Pieces[abs(Squares[InitFile][InitRank])].Promotes == 0 && Pieces[abs(Squares[InitFile][InitRank])].PrGraphic > 0 ) {
    CaptPiece = Pieces[abs(Squares[InitFile][InitRank])].PrGraphic;
    if(Squares[InitFile][InitRank] > 0 ) CaptPiece = 0 - CaptPiece;
   } else {
    CaptPiece = 0 - Squares[InitFile][InitRank];
   }
  }
  for(Z = 1; Z <= Capture; Z++) {
    if((Squares[InitFile][InitRank] < 0 && Reverse == 0) || (Squares[InitFile][InitRank] > 0 && Reverse == 1) ) {
	if(CapRef[Z] == CaptPiece ) {
	    InHand[Z] = InHand[Z] - 1;
	    if(InHand[Z] < 2 ) strcpy(Board.Held[Z].Caption, ""); else sprintf(Board.Held[Z].Caption, "%d", InHand[Z]);
	    if(InHand[Z] < 1 ) Board.HandPic[Z].Visible = False;
	    if(!strcmp(Choice, "Micro") ) {
		I = Z; MicroDrop();
	    }
	}
    } else {
	if(CapRef[Z] == 0 - CaptPiece ) {
	    InHand[Capture + Z] = InHand[Capture + Z] - 1;
	    if(InHand[Capture + Z] < 2 ) strcpy(Board.Held[Capture + Z].Caption, ""); else sprintf(Board.Held[Capture + Z].Caption, "%d", InHand[Capture + Z]);
	    if(InHand[Capture + Z] < 1 ) Board.HandPic[Capture + Z].Visible = False;
	    if(!strcmp(Choice, "Micro") ) {
		I = Capture + Z; MicroDrop();
	    }
	 }
    }
  } // Next Z;
  if(Replaying == 1 ) Squares[InitFile][InitRank] = OldPiece;
}

void ReduceHand2 ()
{
  INT Z, OldPiece;
  if(Replaying == 1 ) {
    CaptPiece = Score[TurnCount].IDEnd;
    OldPiece = Squares[InitFile][InitRank];
    Squares[InitFile][InitRank] = 0 - CaptPiece;
  } else {
   if(Pieces[abs(Squares[InitFile][InitRank])].Promotes == 0 && Pieces[abs(Squares[InitFile][InitRank])].PrGraphic > 0 ) {
    CaptPiece = Pieces[abs(Squares[InitFile][InitRank])].PrGraphic;
    if(Squares[InitFile][InitRank] > 0 ) CaptPiece = 0 - CaptPiece;
   } else {
    CaptPiece = 0 - Squares[InitFile][InitRank];
   }
  }
  for(Z = 1; Z <= Capture; Z++) {
    if((Squares[InitFile][InitRank] < 0 && Reverse == 0) || (Squares[InitFile][InitRank] > 0 && Reverse == 1) ) {
	if(CapRef[Z] == CaptPiece ) {
	    InHand[Z] = InHand[Z] - 1;
	    if(!strcmp(Choice, "Micro") ) {
		I = Z;
		if((I < 5) || (I > 8 && I < 13) ) InHand[I + 4] = InHand[I + 4] - 1; else InHand[I - 4] = InHand[I - 4] - 1;
	    }
	}
    } else {
	if(CapRef[Z] == 0 - CaptPiece ) {
	    InHand[Capture + Z] = InHand[Capture + Z] - 1;
	    if(!strcmp(Choice, "Micro") ) {
		I = Capture + Z;
		if((I < 5) || (I > 8 && I < 13) ) InHand[I + 4] = InHand[I + 4] - 1; else InHand[I - 4] = InHand[I - 4] - 1;
	    }
	 }
    }
  } // Next Z;
  if(Replaying == 1 ) Squares[InitFile][InitRank] = OldPiece;
}

void RemovePiece ()
{
  INT K, L, Found=0; // FIXME: was uninitialized?
  STRING PieceName, VictimSTR;
  if(Selection != 0 || MovePiece == 1 ) {
    SquareReplace();
  } else {
   K = 1;
   do {
    L = 1;
    do {
	if(Grafix[L][K] == I ) {
	    strcpy(PieceName, Pieces[abs(Squares[L][K])].Name);
	    strcpy(Board.PieceID.Caption, PieceName);
	    File = L;
	    Rank = K;
	}
	L = L + 1;
    } while( !(L > BoardSizeX || Found == 1) );
    K = K + 1;
   } while( !(K > BoardSizeY || Found == 1) );
   Found = 0;
   if(Handicap == 1 ) {
    if(Squares[File][Rank] > 0 ) {
	Board.PieceID.ForeColor = 0xFFL;
	strcpy(Board.PieceID.Caption, "Select a White piece");
	Notice = 1;
    }
    if(Squares[File][Rank] < 0 ) {
	strcpy(VictimSTR, Pieces[abs(Squares[File][Rank])].Name);
	if(abs(Squares[File][Rank]) == 1 || !strcmp(VictimSTR, "Emperor") || !strcmp(VictimSTR, "Prince") || !strcmp(VictimSTR, "Crown Prince") ) {
	    Board.PieceID.ForeColor = 0xFFL;
	    sprintf(Board.PieceID.Caption, "The White %scan not be removed", VictimSTR);
	    Notice = 1;
	} else {
	    sprintf(Board.PieceID.Caption, "%s removed", Pieces[abs(Squares[File][Rank])].Name);
	    Notice = 1;
	    Squares[File][Rank] = 0;
	    Board.showpic[Grafix[File][Rank]].Visible = False;
	    MOVE(1, Board.showpic[Grafix[File][Rank]], 0, 0);
	    Grafix[File][Rank] = -1;
	    strcpy(Turn, "White");
	    strcpy(Board.NextMove.Caption, "White to Move");
	    MoveCount = 1; HandGame = 1;
	}
    }
   } else {
    strcpy(Board.PieceID.Caption, "");
    strcpy(VictimSTR, Pieces[abs(Squares[File][Rank])].Name);
    if(Squares[File][Rank] > 0 && (!strcmp(VictimSTR, "Prince") || !strcmp(VictimSTR, "Crown Prince")) ) BlackPrince = BlackPrince - 1;
    if(Squares[File][Rank] < 0 && (!strcmp(VictimSTR, "Prince") || !strcmp(VictimSTR, "Crown Prince")) ) WhitePrince = WhitePrince - 1;
    Squares[File][Rank] = 0;
    Board.showpic[Grafix[File][Rank]].Visible = False;
    MOVE(1, Board.showpic[Grafix[File][Rank]], 0, 0);
    Grafix[File][Rank] = -1;
    MoveCount = 0; TurnCount = 0;
    if(!strcmp(Turn, "White") ) MoveCount = 1;
    strcpy(Board.LastMove.Caption, "");
   }
  }
}

void ReorderMoves ()
{
  INT XX, YY;
  if(Level > 1 ) {
    for(XX = 1; XX <= TestDepth; XX++) {
	if(BestTally[Level + 1] > BestTally[XX] ) {
	    for(YY = TestDepth - 1; YY >= XX; YY--) {
		BestTally[YY + 1] = BestTally[YY];
		MoveList[YY + 1].StartPiece = MoveList[YY].StartPiece;
		MoveList[YY + 1].EndPiece = MoveList[YY].EndPiece;
		MoveList[YY + 1].StartFile = MoveList[YY].StartFile;
		MoveList[YY + 1].EndFile = MoveList[YY].EndFile;
		MoveList[YY + 1].StartRank = MoveList[YY].StartRank;
		MoveList[YY + 1].EndRank = MoveList[YY].EndRank;
		OldKingTally[YY + 1] = OldKingTally[YY];
		if(!strcmp(Choice, "Dai") || !strcmp(Choice, "Chu") || !strcmp(Choice, "DaiDai") || !strcmp(Choice, "Maka") || !strcmp(Choice, "Tenjiku") || !strcmp(Choice, "Tai") ) {
		    ECapture[YY + 1].Piece = ECapture[YY].Piece;
		    ECapture[YY + 1].File = ECapture[YY].File;
		    ECapture[YY + 1].Rank = ECapture[YY].Rank;
		}
	    } // Next YY;
	    BestTally[XX] = BestTally[Level + 1];
	    MoveList[XX].StartPiece = CompLegal[BestMove].StartPiece;
	    MoveList[XX].EndPiece = CompLegal[BestMove].EndPiece;
	    MoveList[XX].StartFile = CompLegal[BestMove].StartFile;
	    MoveList[XX].EndFile = CompLegal[BestMove].EndFile;
	    MoveList[XX].StartRank = CompLegal[BestMove].StartRank;
	    MoveList[XX].EndRank = CompLegal[BestMove].EndRank;
	    OldKingTally[XX] = KingTally[BestMove];
	    if(!strcmp(Choice, "Dai") || !strcmp(Choice, "Chu") || !strcmp(Choice, "DaiDai") || !strcmp(Choice, "Maka") || !strcmp(Choice, "Tenjiku") || !strcmp(Choice, "Tai") ) {
		ECapture[XX].Piece = ExtraCapture[BestMove].Piece;
		ECapture[XX].File = ExtraCapture[BestMove].File;
		ECapture[XX].Rank = ExtraCapture[BestMove].Rank;
	    }
	    return;
	}
	DoEvents();
    } // Next XX;
  }
}

void Replay ()
{ // Menu function.
  INT V, Location;
  if((LegalMoves == 0 || Checked == 2) && LionPiece != I && XA == 0 ) {
   Notice = 0;
   strcpy(Board.PieceID.Caption, "");
   if(Forwards != 1 ) strcpy(Board.Caption, Cap);
   BugFix();
   if(TurnCount >= EndTurn ) {
    Board.PieceID.ForeColor = 0xFFL;
    strcpy(Board.PieceID.Caption, "No moves to replay!");
    Notice = 1;
   } else {
    Replaying = 1;
    for(V = 1; V <= Captures[TurnCount].number; V++) {
       Location = Captures[TurnCount].Positions[V];
       InitRank = Int(Location / (BoardSizeX + 1));
       InitFile = Location - (InitRank * (BoardSizeX + 1));
       NewGraf = Grafix[InitFile][InitRank];
       Grafix[InitFile][InitRank] = -1;
       Board.showpic[NewGraf].Visible = False;
       MOVE(1, Board.showpic[NewGraf], 0, 0);
       Squares[InitFile][InitRank] = 0;
    } // Next V;
    if(Drop == 1 && Captures[TurnCount].number > 0 ) {
	 CaptPiece = Captures[TurnCount].PieceNum[1];
	 if(Pieces[abs(CaptPiece)].Promotes == 0 && Pieces[abs(CaptPiece)].PrGraphic > 0 ) {
	    CaptPiece = Pieces[abs(CaptPiece)].PrGraphic;
	    if(Captures[TurnCount].PieceNum[1] > 0 ) CaptPiece = 0 - CaptPiece;
	 } else {
	    CaptPiece = 0 - CaptPiece;
	 }
	 AddHand();
    }
    Location = Score[TurnCount].PosStart;
    if(Location > 0 ) {
	Rank = Int(Location / (BoardSizeX + 1));
	File = Location - (Rank * (BoardSizeX + 1));
	Squares[File][Rank] = 0;
	NewGraf = Grafix[File][Rank];
	Grafix[File][Rank] = -1;
	Board.showpic[NewGraf].Visible = False;
	MOVE(1, Board.showpic[NewGraf], 0, 0);
    }
    Location = Score[TurnCount].PosEnd;
    if(Location > 0 ) {
	InitRank = Int(Location / (BoardSizeX + 1));
	InitFile = Location - (InitRank * (BoardSizeX + 1));
	Squares[InitFile][InitRank] = Score[TurnCount].IDStart;
	if(Sgn(Score[TurnCount].IDStart) == Sgn(Score[TurnCount].IDEnd) && Score[TurnCount].IDEnd != Score[TurnCount].IDStart ) Squares[InitFile][InitRank] = Score[TurnCount].IDEnd;
	if(Squares[InitFile][InitRank] < 0 ) {
	    if(!strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "Emperor") ) { WhiteEmpX = InitFile; WhiteEmpY = InitRank; }
	    if(!strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "King") ) { WhiteKingX = InitFile; WhiteKingY = InitRank; }
	}
	if(Squares[InitFile][InitRank] > 0 ) {
	    if(!strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "Emperor") ) { BlackEmpX = InitFile; BlackEmpY = InitRank; }
	    if(!strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "King") ) { BlackKingX = InitFile; BlackKingY = InitRank; }
	}
	if(Score[TurnCount].IDEnd != 0 ) {
	    NewGraf = 0;
	    while( Board.showpic[NewGraf].Visible == True ) {
		NewGraf = NewGraf + 1;
	    }
	    SetGrafix();
	}
    }
    if(Drop == 1 && Score[TurnCount].PosStart == 0 ) ReduceHand();
    if(!strcmp(Turn, "Black") ) {
	strcpy(Turn, "White");
	strcpy(Board.NextMove.Caption, "White to Move");
	MoveCount = MoveCount + 1;
    } else {
	strcpy(Turn, "Black");
	strcpy(Board.NextMove.Caption, "Black to Move");
    }
    TurnCount = TurnCount + 1;
    Board.MnuHandicap.Enabled = False;
    EnableNamedMenuItem("Setup.Handicap", False); // [HGM] added
    if(Asc(Score[TurnCount].Caption) > 47 && Asc(Score[TurnCount].Caption) < 58 ) {
	strcpy(Board.LastMove.Caption, Score[TurnCount].Caption);
    } else {
	sprintf(Board.LastMove.Caption, "%d. %s", MoveCount, Score[TurnCount].Caption); // Format$(MoveCount) + ". " + Score[TurnCount].Caption;
    }
    Backwards = 1;
    SetKings();
    Location = Score[TurnCount - 1].PosEnd;
    if(Location > 0 ) {
	Rank = Int(Location / (BoardSizeX + 1));
	File = Location - (Rank * (BoardSizeX + 1));
    }
    LastPieceX = File; LastPieceY = Rank;
    if(!strcmp(Computer, "Black") || !strcmp(Computer, "White") ) {
	sprintf(Board.Caption, "Move Replayed (%d of %d ) ; Press [ESC] to continue play.", TurnCount, EndTurn);
    } else {
	sprintf(Board.Caption, "Move Replayed (%d of %d )", TurnCount, EndTurn);
    }
   }
   Replaying = 0;
  }
}

void Replay2 ()
{
  INT V, Location;
  if((LegalMoves == 0 || Checked == 2) && LionPiece != I && XA == 0 ) {
   if(TurnCount < EndTurn ) {
    Replaying = 1;
    for(V = 1; V <= Captures[TurnCount].number; V++) {
       Location = Captures[TurnCount].Positions[V];
       InitRank = Int(Location / (BoardSizeX + 1));
       InitFile = Location - (InitRank * (BoardSizeX + 1));
       Squares[InitFile][InitRank] = 0;
    } // Next V;
    if(Drop == 1 && Captures[TurnCount].number > 0 ) {
	 CaptPiece = Captures[TurnCount].PieceNum[1];
	 if(Pieces[abs(CaptPiece)].Promotes == 0 && Pieces[abs(CaptPiece)].PrGraphic > 0 ) {
	    CaptPiece = Pieces[abs(CaptPiece)].PrGraphic;
	    if(Captures[TurnCount].PieceNum[1] > 0 ) CaptPiece = 0 - CaptPiece;
	 } else {
	    CaptPiece = 0 - CaptPiece;
	 }
	 AddHand3();
    }
    Location = Score[TurnCount].PosStart;
    if(Location > 0 ) {
	Rank = Int(Location / (BoardSizeX + 1));
	File = Location - (Rank * (BoardSizeX + 1));
	Squares[File][Rank] = 0;
    }
    Location = Score[TurnCount].PosEnd;
    if(Location > 0 ) {
	InitRank = Int(Location / (BoardSizeX + 1));
	InitFile = Location - (InitRank * (BoardSizeX + 1));
	Squares[InitFile][InitRank] = Score[TurnCount].IDStart;
	if(Sgn(Score[TurnCount].IDStart) == Sgn(Score[TurnCount].IDEnd) && Score[TurnCount].IDEnd != Score[TurnCount].IDStart ) Squares[InitFile][InitRank] = Score[TurnCount].IDEnd;
	if(Squares[InitFile][InitRank] < 0 ) {
	    if(!strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "Emperor") ) { WhiteEmpX = InitFile; WhiteEmpY = InitRank; }
	    if(!strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "King") ) { WhiteKingX = InitFile; WhiteKingY = InitRank; }
	}
	if(Squares[InitFile][InitRank] > 0 ) {
	    if(!strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "Emperor") ) { BlackEmpX = InitFile; BlackEmpY = InitRank; }
	    if(!strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "King") ) { BlackKingX = InitFile; BlackKingY = InitRank; }
	}
    }
    if(Drop == 1 && Score[TurnCount].PosStart == 0 ) ReduceHand2();
    if(!strcmp(Turn, "Black") ) {
	strcpy(Turn, "White");
	strcpy(Board.NextMove.Caption, "White to Move");
	MoveCount = MoveCount + 1;
    } else {
	strcpy(Turn, "Black");
	strcpy(Board.NextMove.Caption, "Black to Move");
    }
    TurnCount = TurnCount + 1;
    Board.MnuHandicap.Enabled = False;
    EnableNamedMenuItem("Setup.Handicap", False); // [HGM] added
    if(Asc(Score[TurnCount].Caption) > 47 && Asc(Score[TurnCount].Caption) < 58 ) {
	strcpy(Board.LastMove.Caption, Score[TurnCount].Caption);
    } else {
	sprintf(Board.LastMove.Caption, "%d. %s", MoveCount, Score[TurnCount].Caption); // Format$(MoveCount) + ". " + Score[TurnCount].Caption;
    }
    Board.MnuHandicap.Enabled = False;
    EnableNamedMenuItem("Setup.Handicap", False); // [HGM] added
    Backwards = 1;
    if(Location > 0 ) {
	Rank = Int(Location / (BoardSizeX + 1));
	File = Location - (Rank * (BoardSizeX + 1));
    }
    LastPieceX = File; LastPieceY = Rank;
   }
   Replaying = 0;
  }
}

void ReplayAll ()
{ // Menu function.
  if(LegalMoves == 0 ) {
    while( TurnCount < EndTurn) {
	Replay2();
    }
    strcpy(Board.Caption, Cap);
    RestoreGrafix();
    SetKings();
  }
}

void ResetBoard ()
{
  INT W;
  Board.Hide();
  if(!strcmp(Choice, "Wa") ) {
    for(W = 1; W <= Capture * 2; W++) {
       strcpy(Board.Held[W].Caption, "");
       Board.HandPic[W].Visible = False;
    } // Next W;
  }
  NewGame = 1;
  SetPieces();
}

void ResetHand ()
{
  INT X;
  for(X = 1; X <= Capture; X++) {
    if(InHand[X] < 1 ) Board.HandPic[X].Visible = False;
    if(InHand[X] < 2 ) strcpy(Board.Held[X].Caption, "");
    if(InHand[Capture + X] < 1 ) Board.HandPic[Capture + X].Visible = False;
    if(InHand[Capture + X] < 2 ) strcpy(Board.Held[Capture + X].Caption, "");
  } // Next X;
}

void ResetLegal ()
{
  INT MM, NN;
  for(MM = 1; MM <= BoardSizeY; MM++) {
    for(NN = 1; NN <= BoardSizeX; NN++) {
	Legal[NN][MM] = 0;
    } // Next NN;
  } // Next MM;
}

void RestoreGrafix ()
{
  INT ABC, DEF;
  Board.Hide();
  Count = 0; M = 0;
  for(J = 1; J <= BoardSizeY; J++) {
    for(I = 1; I <= BoardSizeX; I++) {
	Grafix[I][J] = -1;
	if(Squares[I][J] != 0 ) {
	    Graphnum = Pieces[abs(Squares[I][J])].Graphic;
	    if((Squares[I][J] < 0 && Reverse == 0) || (Squares[I][J] > 0 && Reverse == 1) ) Graphnum = Graphnum + (TotGraph / 2);
/*	    if(!strcmp(Choice, "Tai") ) Board.showpic[COUNT] = TaiPieces.Pix[Graphnum - 1]; else */ Board.showpic[COUNT] = Board.Pix[Graphnum - 1];
	    if(Reverse == 0 ) {
		MOVE(0, Board.showpic[COUNT], XStart + ((I - 1) * Pixels), 11 + ((J - 1) * Pixels));
	    } else {
		MOVE(0, Board.showpic[COUNT], XStart + ((BoardSizeX - I) * Pixels), 11 + ((BoardSizeY - J) * Pixels));

	    }
	    Board.showpic[COUNT].Visible = True;
	    Grafix[I][J] = Count;
	    Count = Count + 1;
	}
    } // Next I;
  } // Next J;
  if(Drop == 1 || !strcmp(Choice, "Wa") ) {
     for(I = 1; I <= Capture * 2; I++) {
	Graphnum = Pieces[abs(CapRef[I])].Graphic;
	if(I > Capture ) Graphnum = Graphnum + (TotGraph / 2);
	Board.HandPic[I].Picture = Board.Pix[Graphnum - 1].Picture; // [HGM] only picture, not coords!
	Board.HandPic[I].Visible = False;
	strcpy(Board.Held[I].Caption, "");
	if(InHand[I] > 0 ) Board.HandPic[I].Visible = True;
	if(InHand[I] > 1 ) sprintf(Board.Held[I].Caption, "%d", InHand[I]);
    } // Next I;
  }
  BugFix();
  Board.Show();
  if(!strcmp(Choice, "Tai") || !strcmp(Choice, "Maka") ) {
    for(ABC = 1; ABC <= BoardSizeY; ABC++) {
	for(DEF = 1; DEF <= BoardSizeX; DEF++) {
	    if(Squares[DEF][ABC] > 0 ) {
		if(!strcmp(Pieces[abs(Squares[DEF][ABC])].Name, "Emperor") ) { BlackEmpX = DEF; BlackEmpY = ABC; BlackEmperor = 1; }
	    }
	    if(Squares[DEF][ABC] < 0 ) {
		if(!strcmp(Pieces[abs(Squares[DEF][ABC])].Name, "Emperor") ) { WhiteEmpX = DEF; WhiteEmpY = ABC; WhiteEmperor = 1; }
	    }
	} // Next DEF;
    } // Next ABC;
  }
}

void RestoreMap ()
{
  INT JJ, KK, LL;
  for(JJ = 1; JJ <= BoardSizeY; JJ++) {
    for(KK = 1; KK <= BoardSizeX; KK++) {
	BanMap[KK][JJ].WhiteNum = BackMap[KK][JJ].WhiteNum;
	BanMap[KK][JJ].BlackNum = BackMap[KK][JJ].BlackNum;
	Attacker[KK][JJ] = OldAttack[KK][JJ];
	for(LL = 1; LL <= OldAttack[KK][JJ]; LL++) {
	    BanMap[KK][JJ].Info[LL].File = BackMap[KK][JJ].Info[LL].File;
	    BanMap[KK][JJ].Info[LL].Rank = BackMap[KK][JJ].Info[LL].Rank;
	    BanMap[KK][JJ].Info[LL].Piece = BackMap[KK][JJ].Info[LL].Piece;
	} // Next LL;
    } // Next KK;
    DoEvents();
  } // Next JJ;
}

void Rotate ()
{ // Menu function.
  if(LegalMoves == 0 ) {
   Board.Picture = LoadPicture((sprintf(StringTmp, "%s/boards/%s", Direct, Boardbmp), StringTmp));
   if(Reverse == 1 ) {
    Reverse = 0;
    Board.White.Picture = LoadPicture((sprintf(StringTmp, "%s/%s", Direct, "WhiteDn.bmp"), StringTmp));
    Board.Black.Picture = LoadPicture((sprintf(StringTmp, "%s/%s", Direct, "BlackUp.bmp"), StringTmp));
   } else {
    Reverse = 1;
    Board.White.Picture = LoadPicture((sprintf(StringTmp, "%s/%s", Direct, "WhiteUp.bmp"), StringTmp));
    Board.Black.Picture = LoadPicture((sprintf(StringTmp, "%s/%s", Direct, "BlackDn.bmp"), StringTmp));
   }
   for(J = 1; J <= BoardSizeY; J++) {
    for(I = 1; I <= BoardSizeX; I++) {
	if(Squares[I][J] != 0 ) {
	    Board.showpic[Grafix[I][J]].Visible = False;
	    Graphnum = Pieces[abs(Squares[I][J])].Graphic;
	    if((Squares[I][J] < 0 && Reverse == 0) || (Squares[I][J] > 0 && Reverse == 1) ) Graphnum = Graphnum + (TotGraph / 2);
/*	    if(!strcmp(Choice, "Tai") ) Board.showpic[Grafix[I][J]] = TaiPieces.Pix[Graphnum - 1]; else */ Board.showpic[Grafix[I][J]] = Board.Pix[Graphnum - 1];
	    if(Reverse == 0 ) {
		MOVE(0, Board.showpic[Grafix[I][J]], XStart + ((I - 1) * Pixels), 11 + ((J - 1) * Pixels));
	    } else {
		MOVE(0, Board.showpic[Grafix[I][J]], XStart + ((BoardSizeX - I) * Pixels), 11 + ((BoardSizeY - J) * Pixels));
	    }
	    Board.showpic[Grafix[I][J]].Visible = True;
	}
    } // Next I;
   } // Next J;
   if(Drop == 1 ) {
// TODO    ReDim TempHand(Capture * 2);
    for(I = 1; I <= Capture * 2; I++) {
	Board.HandPic[I].Visible = False;
	strcpy(Board.Held[I].Caption, "");
	CapRef[I] = 0 - CapRef[I];
	if(I > Capture ) TempHand[I] = InHand[I - Capture]; else TempHand[I] = InHand[I + Capture];
    } // Next I;
    for(I = 1; I <= Capture * 2; I++) {
	InHand[I] = TempHand[I];
	if(InHand[I] > 0 ) Board.HandPic[I].Visible = True;
	if(InHand[I] > 1 ) sprintf(Board.Held[I].Caption, "%d", InHand[I]);
    } // Next I;
   }
   NotSet();
  }
}

void SaveGame ()
{ // Menu function.
  INT V, W, Z;
  STRING AnswerSTR;
  FILE *f2;
// TODO:  On Error Resume Next;
  strcpy(Board.CMSave.DialogTitle, "Save Game");
  Board.CMSave.Flags = 0x400L | 0x800L | 0x4L;
  Board.CMSave.Action = 2;
  if(Err == 32755 ) return; // TODO This is for if we cancel the file-browse dialog?
  strcpy(SavedSTR, Board.CMSave.Filename);
  f2 = fopen(SavedSTR, "w");
  fprintf(f2, "\"%s\",%d,%d,\"%s\",%d,%d,%d,%d,%d\n", Choice, Drop, MoveCount, Turn, Notate, SeeMove, TurnCount, WhiteKing, BlackKing);
  fprintf(f2, "%d,%d,%d,%d,%d,%d,\"%s\"\n", WhiteLion, BlackLion, WhitePrince, BlackPrince, WhiteEmperor, BlackEmperor, Board.LastMove.Caption);
  fprintf(f2, "\"%s\",\"%s\",%d,%d\n", Board.WhiteClock.Caption, Board.BlackClock.Caption, HandGame, Reverse);
  for(W = 0; W <= TurnCount; W++) {
    fprintf(f2, "\"%s\",%d,%d,%d,%d\n", Score[W].Caption, Score[W].IDStart, Score[W].IDEnd, Score[W].PosStart, Score[W].PosEnd);
    fprintf(f2, "%d\n", Captures[W].number);
    for(V = 1; V <= Captures[W].number; V++) {
	fprintf(f2, "%d\n", Captures[W].Positions[V]);
	fprintf(f2, "%d\n", Captures[W].PieceNum[V]);
    } // Next V;
  } // Next W;
  for(W = 1; W <= BoardSizeY; W++) {
    for(Z = 1; Z <= BoardSizeX; Z++) {
	fprintf(f2, "%d\n", Squares[Z][W]);
    } // Next Z;
  } // Next W;
  if(Drop == 1 ) {
    for(W = 1; W <= Capture * 2; W++) {
	fprintf(f2, "%d\n", InHand[W]);
    } // Next W;
  }
  strcpy(AnswerSTR, InputBoxSTR("Please enter a short description of the game.", "Saved Game Description", SaveTitleSTR));
  if(strcmp(AnswerSTR, "") ) fprintf(f2, "\"%s\"\n", AnswerSTR); else fprintf(f2, "\"Saved Game - No Title\"\n");
  fprintf(f2, "\"%s\"\n", Computer);
  fprintf(f2, "%d\n", Level);
  fprintf(f2, "%d\n", GameOver);
  fprintf(f2, "\"%s\"\n", Threat);
  fclose(f2);
}

void SeeMoves ()
{ // Menu function. SeeMoves = 0 (off) or 1 (on)
  if(Board.MnuShowOn.Enabled == False ) Board.MnuShowOn.Enabled = True; else Board.MnuShowOn.Enabled = False;
  if(Board.MnuShowOff.Enabled == False ) Board.MnuShowOff.Enabled = True; else Board.MnuShowOff.Enabled = False;
  if(Board.MnuShowOn.Checked == False ) Board.MnuShowOn.Checked = True; else Board.MnuShowOn.Checked = False;
  if(Board.MnuShowOff.Checked == False ) Board.MnuShowOff.Checked = True; else Board.MnuShowOff.Checked = False;
  if(SeeMove == 1 ) {
    strcpy(Board.PieceID.Caption, "Show Legal Moves - On");
    Board.MnuThreatOn.Enabled = True;
    EnableNamedMenuItem("Moves.ShowThreat", True); // [HGM] added
    if(!strcmp(OldThreat, "On") ) {
	strcpy(Threat, "On");
	SetThreat();
	strcpy(OldThreat, "None");
	MarkMenuItem("Moves.ShowThreat", True); // [HGM] added
    } else {
	strcpy(Threat, "Off");
	Board.MnuThreatOn.Enabled = False;
	SetThreat();
	strcpy(OldThreat, "None");
    }
  } else {
    strcpy(Board.PieceID.Caption, "Show Legal Moves - Off");
    strcpy(OldThreat, Threat);
    strcpy(Threat, "Off");
    SetThreat();
    Board.MnuThreatOn.Enabled = False;
    MarkMenuItem("Moves.ShowThreat", False); EnableNamedMenuItem("Moves.ShowThreat", False); // [HGM] added
  }
  Notice = 1;
}

void SetAutoPromote ()
{ // Menu function. Autopromote = 0 (off) or 1 (on)
  if(Board.MnuAutoOn.Enabled == False ) Board.MnuAutoOn.Enabled = True; else Board.MnuAutoOn.Enabled = False;
  if(Board.MnuAutoOff.Enabled == False ) Board.MnuAutoOff.Enabled = True; else Board.MnuAutoOff.Enabled = False;
  if(Board.MnuAutoOn.Checked == False ) Board.MnuAutoOn.Checked = True; else Board.MnuAutoOn.Checked = False;
  if(Board.MnuAutoOff.Checked == False ) Board.MnuAutoOff.Checked = True; else Board.MnuAutoOff.Checked = False;
  if(AutoPromote == 1 ) {
    strcpy(Board.PieceID.Caption, "Auto-Promote On");
  } else {
    strcpy(Board.PieceID.Caption, "Auto-Promote Off");
  }
  Notice = 1;
}

void SetBlackPlayer ()
{ // Menu function. Called if black player changes (which sets Computer before calling this).
  if(Board.MnuBlackPlayer.Enabled == False ) Board.MnuBlackPlayer.Enabled = True; else Board.MnuBlackPlayer.Enabled = False;
  if(Board.MnuBlackComp.Enabled == False ) Board.MnuBlackComp.Enabled = True; else Board.MnuBlackComp.Enabled = False;
  if(Board.MnuBlackPlayer.Checked == False ) Board.MnuBlackPlayer.Checked = True; else Board.MnuBlackPlayer.Checked = False;
  if(Board.MnuBlackComp.Checked == False ) Board.MnuBlackComp.Checked = True; else Board.MnuBlackComp.Checked = False;
  if(!strcmp(Choice, "Tai") || !strcmp(Choice, "Maka") ) {
    if(!strcmp(Computer, "White") || !strcmp(Computer, "Black") || !strcmp(Computer, "Both") ) CompTeach(); else NoCompTeach();
  }
  if(!strcmp(Computer, "Both") ) CompVComp();
}

void SetClock ()
{
  INT Mins, Secs, Hrs;
  if(Notice > 0 ) Notice = Notice + 1;
  if(Notice == 4 && Checked != 1 ) {
    strcpy(Board.PieceID.Caption, "");
    Notice = 0;
    if(strcmp(Board.Caption, SaveTitleSTR) ) strcpy(Board.Caption, Cap);
  }
  sscanf(ElapsedSTR, "%d:%d:%d", &Hrs, &Mins, &Secs);
//  Secs = Val(Right$(Elapsed$, 2))
//  Mins = Val(Mid$(Elapsed$, 4, 2))
//  Hrs = Val(Left$(Elapsed$, 2))
  Secs = Secs + 1;
  if(Secs == 60 ) { Secs = 0; Mins = Mins + 1; }
  if(Mins == 60 ) { Mins = 0; Hrs = Hrs + 1; }
  if(Hrs == 100 ) Hrs = 0;
//  Hours$ = LTrim$(Str$(Hrs))
//  Minutes$ = LTrim$(Str$(Mins))
//  Seconds$ = LTrim$(Str$(Secs))
//  if(Len(Hours$) = 1 Then Hours$ = "0" + Hours$
//  if(Len(Minutes$) = 1 Then Minutes$ = "0" + Minutes$
//  if(Len(Seconds$) = 1 Then Seconds$ = "0" + Seconds$
//  Elapsed$ = Hours$ + ":" + Minutes$ + ":" + Seconds$
  sprintf(ElapsedSTR, "%02d:%02d:%02d", Hrs, Mins, Secs);
}

void SetDifficulty ()
{ // Menu function. Grade = "Best" / "LessWeak" / "Weak depending on menu item.
  if(!strcmp(Grade, "Weak") ) {
    Board.MnuWeak.Enabled = False;
    Board.MnuWeak.Checked = True;
    Board.MnuBest.Visible = False;
    Board.MnuLessWeak.Enabled = True;
    Board.MnuLessWeak.Checked = False;
  }
  if(!strcmp(Choice, "Chu") || !strcmp(Choice, "Dai") || !strcmp(Choice, "DaiDai") || !strcmp(Choice, "Maka") || !strcmp(Choice, "Tai") ) Board.MnuLessWeak.Visible = False, EnableNamedMenuItem("Setup.LessWeak", False); // [HGM] added
  else EnableNamedMenuItem("Setup.LessWeak", True); // [HGM] added
  if(!strcmp(Grade, "LessWeak") ) {
    Board.MnuWeak.Enabled = True;
    Board.MnuWeak.Checked = False;
    Board.MnuBest.Visible = False;
    Board.MnuLessWeak.Enabled = False;
    Board.MnuLessWeak.Checked = True;
  }
  Board.MnuBest.Visible = False;
  SetGrade();
}

void SetEval ()
{ // Menu function. Eval = 0 (off) or 1 (on)
  if(Board.MnuEvalOn.Enabled == False ) Board.MnuEvalOn.Enabled = True; else Board.MnuEvalOn.Enabled = False;
  if(Board.MnuEvalOff.Enabled == False ) Board.MnuEvalOff.Enabled = True; else Board.MnuEvalOff.Enabled = False;
  if(Board.MnuEvalOn.Checked == False ) Board.MnuEvalOn.Checked = True; else Board.MnuEvalOn.Checked = False;
  if(Board.MnuEvalOff.Checked == False ) Board.MnuEvalOff.Checked = True; else Board.MnuEvalOff.Checked = False;
}

void SetGeneral ()
{ // Menu function.
#if 0
  INT Z;
// TODO:  if(GeneralInfo == 0 ) Unload RulesHelp;
  GeneralInfo = 1;
  for(Z = 0; Z <= 17; Z++) {
    RulesHelp.Title[Z].Visible = False;
  } // Next Z;
  RulesHelp.Title[9].Visible = True;
  RulesHelp.CmdPiece.Visible = False;
  RulesHelp.Show();
#else
  static char text[20000];
  STRING name;
  FILE *f;
  sprintf(name, "%s/General.rul", Direct);
  if((f = fopen(name, "r"))) {
    fread(text, 1, 20000, f);
    TagsPopUp(text+1, "General");
    fclose(f);
  }
#endif
}

void SetGrade ()
{
  if(!strcmp(Grade, "Weak") ) Level = 1;
  if(!strcmp(Grade, "LessWeak") ) Level = 5;
  if(!strcmp(Grade, "LessWeak") ) {
    if(!strcmp(Choice, "Micro") || !strcmp(Choice, "Mini")) Level = 12;
    if(!strcmp(Choice, "Judkin") || !strcmp(Choice, "Whale") || !strcmp(Choice, "Yari") || !strcmp(Choice, "Tori") || !strcmp(Choice, "HeianSho")) Level = 7;
    if(!strcmp(Choice, "Wa") || !strcmp(Choice, "Chu") || !strcmp(Choice, "Dai") || !strcmp(Choice, "DaiDai") || !strcmp(Choice, "Maka") || !strcmp(Choice, "Tai")) Level = 4;
    if(!strcmp(Choice, "Tenjiku")) Level = 3;
  }
}

void SetGrafix ()
{
  Grafix[InitFile][InitRank] = NewGraf;
  Graphnum = Pieces[abs(Squares[InitFile][InitRank])].Graphic;
  if((Squares[InitFile][InitRank] < 0 && Reverse == 0) || (Squares[InitFile][InitRank] > 0 && Reverse == 1) ) Graphnum = Graphnum + (TotGraph / 2);
/*  if(!strcmp(Choice, "Tai") ) Board.showpic[NewGraf] = TaiPieces.Pix[Graphnum - 1]; else */ Board.showpic[NewGraf] = Board.Pix[Graphnum - 1];
  if(Reverse == 0 ) {
    MOVE(1, Board.showpic[NewGraf], XStart + ((InitFile - 1) * Pixels), 11 + ((InitRank - 1) * Pixels));
  } else {
    MOVE(1, Board.showpic[NewGraf], XStart + ((BoardSizeX - InitFile) * Pixels), 11 + ((BoardSizeY - InitRank) * Pixels));
  }
  Board.showpic[NewGraf].Visible = True;
}

void SetHandicap ()
{ // Menu function. (Calls EndSetup before.) Reduce = 1 for minus item
  if(Reduce != 1 ) {
    strcpy(Board.Caption, "Select Handicap Pieces - (Press Right Mouse Button to Start Game)");
  } else {
    strcpy(Board.Caption, "Select Pieces to Remove - (Press Right Mouse Button When Finished)");
  }
  if(Reduce != 1 ) Handicap = 1;
  strcpy(Board.BlackClock.Caption, "00:00:00");
  strcpy(Board.WhiteClock.Caption, "00:00:00");
  Board.Timer1.Enabled = False;
}

void SetKings ()
{
  INT GG, HH;
  BlackPrince = 0; BlackKing = 1; BlackEmperor = 0;
  WhitePrince = 0; WhiteKing = 1; WhiteEmperor = 0;
  for(GG = 1; GG <= BoardSizeY; GG++) {
    for(HH = 1; HH <= BoardSizeX; HH++) {
	if(Squares[HH][GG] == 1 ) { BlackKing = 0; BlackKingX = HH; BlackKingY = GG; }
	if(Squares[HH][GG] == -1 ) { WhiteKing = 0; WhiteKingX = HH; WhiteKingY = GG; }
	if(Squares[HH][GG] > 0 ) {
	    if(!strcmp(Pieces[abs(Squares[HH][GG])].Name, "Prince") || !strcmp(Pieces[abs(Squares[HH][GG])].Name, "Crown Prince") ) BlackPrince = BlackPrince + 1;
	    if(!strcmp(Pieces[abs(Squares[HH][GG])].Name, "Emperor") ) { BlackEmperor = 1; BlackEmpX = HH; BlackEmpY = GG; }
	}
	if(Squares[HH][GG] < 0 ) {
	    if(!strcmp(Pieces[abs(Squares[HH][GG])].Name, "Prince") || !strcmp(Pieces[abs(Squares[HH][GG])].Name, "Crown Prince") ) WhitePrince = WhitePrince + 1;
	    if(!strcmp(Pieces[abs(Squares[HH][GG])].Name, "Emperor") ) { WhiteEmperor = 1; WhiteEmpX = HH; WhiteEmpY = GG; }
	}
    } // Next HH;
  } // Next GG;
}

void SetLastMove ()
{ // Menu function. ShowLast = 0 (off) or 1 (on).
  if(Board.MnuShowLastOn.Enabled == False ) Board.MnuShowLastOn.Enabled = True; else Board.MnuShowLastOn.Enabled = False;
  if(Board.MnuShowLastOff.Enabled == False ) Board.MnuShowLastOff.Enabled = True; else Board.MnuShowLastOff.Enabled = False;
  if(Board.MnuShowLastOn.Checked == False ) Board.MnuShowLastOn.Checked = True; else Board.MnuShowLastOn.Checked = False;
  if(Board.MnuShowLastOff.Checked == False ) Board.MnuShowLastOff.Checked = True; else Board.MnuShowLastOff.Checked = False;
}

void SetLionHawk ()
{   // Menu function. LionHawkVer = 1 or 2 depending on item. (Sets piece values before.)
    if(Board.MnuLVer1.Enabled == False ) Board.MnuLVer1.Enabled = True; else Board.MnuLVer1.Enabled = False;
    if(Board.MnuLVer2.Enabled == False ) Board.MnuLVer2.Enabled = True; else Board.MnuLVer2.Enabled = False;
    if(Board.MnuLVer1.Checked == False ) Board.MnuLVer1.Checked = True; else Board.MnuLVer1.Checked = False;
    if(Board.MnuLVer2.Checked == False ) Board.MnuLVer2.Checked = True; else Board.MnuLVer2.Checked = False;
}

void SetPieces ()
{  // Called from New menu with NewGame = 1 if LegalMoves == 0.
   INT K, V, W, X, Y;
   STRING LastGoSTR, WhiteTimeSTR, BlackTimeSTR;
   FILE *f1;
   Randomize();
   XA = 0; GameOver = 0; LegalMoves = 0; strcpy(OldThreat, "None");
   if(Reload != 1 ) ConfigLoad(); else ConfigLoad2();
   Reload = 1; Influence = 0; CompMove = 0; Evaluate = 0;
   LionPiece = -1; CheckTest = 0; Checked = 0;
   sprintf(Datafile, "%s/%s.dat",  Direct, Choice);
   strcpy(Turn, "Black"); LionPiece = -1; WhiteKing = 0; BlackKing = 0; WhitePrince = 0; BlackPrince = 0; RealLion = 0;
   HandGame = 0; WhiteLion = 0; BlackLion = 0; WhiteEmperor = 0; BlackEmperor = 0; Taken = 0; Mate = 0;
   strcpy(CMoveSTR, ""); TurnCount = 0; EndTurn = 0; MoveCount = 0;
   strcpy(Board.LastMove.Caption, ""); strcpy(Board.NextMove.Caption, "Black to Move");
   if(Reverse != 1 ) Reverse = 0;
   XStart = 110;
   if(!strcmp(Choice, "Tenjiku") ) strcpy(Computer, "None");
   if(!strcmp(Choice, "Chu") || !strcmp(Choice, "Dai") || !strcmp(Choice, "DaiDai") || !strcmp(Choice, "Maka") || !strcmp(Choice, "Tai") ) { strcpy(Grade, "Weak"); SetDifficulty(); }
   if(!strcmp(Choice, "Whale") || !strcmp(Choice, "Judkin") ) XStart = 140;
   if(!strcmp(Choice, "Mini") ) XStart = 170;
   if(!strcmp(Choice, "Micro") ) XStart = 200;
   if(!strcmp(Choice, "Yari") ) XStart = 157;
   YStart = 0; // FIXME: this & next line: 3 variants need a correction, but only for flipped pieces. Why???
   if(!strcmp(Choice, "Chu") || !strcmp(Choice, "Dai") || !strcmp(Choice, "Tenjiku") ) YStart = 4;
//   ReDim ShortScore[4000];
//   ReDim Score[4000];
//   ReDim Captures[4000];

   f1 = fopen(Datafile, "r");

    //  load general game data;

     fscanf(f1, "%[^\n\r] %s %d,%d,%d,%d", Cap, Boardbmp, &Drop, &Boardsize, &Prom, &PromDotY);
     fscanf(f1, ",%d,%d,%d,%d,%d,%d", &XCorner, &YCorner, &Pixels, &Totpiece, &PieceNum, &TotGraph);
     BoardSizeX = Boardsize; BoardSizeY = Boardsize;
     if(!strcmp(Choice, "HShogi") ) BoardSizeY = 8;
     if(!strcmp(Choice, "Micro") ) BoardSizeY = 5;
     if(!strcmp(Choice, "Yari") ) { BoardSizeY = 9; BoardSizeX = 7; }
     if(Loading == 1 ) {
	 fscanf(f2, "%d,%d,\"%[^\"]\",%d,%d,%d,%d,%d", &Drop, &MoveCount, Turn, &Notate, &SeeMove, &TurnCount, &WhiteKing, &BlackKing);
	 fscanf(f2, "%d,%d,%d,%d,%d,%d,\"%[^\"]\"", &WhiteLion, &BlackLion, &WhitePrince, &BlackPrince, &WhiteEmperor, &BlackEmperor, LastGoSTR);
	 fscanf(f2, " \"%[^\"]\",\"%[^\"]\",%d,%d", WhiteTimeSTR, BlackTimeSTR, &HandGame, &Reverse);
	 EndTurn = TurnCount;
	 strcpy(Board.LastMove.Caption, LastGoSTR);
	 strcpy(Board.WhiteClock.Caption, WhiteTimeSTR);
	 strcpy(Board.BlackClock.Caption, BlackTimeSTR);
	 if(GameOver == 1 ) {
	    strcpy(Board.NextMove.Caption, "Game Ended");
	 } else {
	    if(!strcmp(Turn, "White") ) strcpy(Board.NextMove.Caption, "White to Move");
	 }
	 for(W = 0; W <= TurnCount; W++) {
	     if(!fscanf(f2, " \"%[^\"]\",", Score[W].Caption)) fscanf(f2, "\","), Score[W].Caption[0] = '\0'; // scanf chokes on empty string!
	     fscanf(f2, "%d,%d,%d,%d", &Score[W].IDStart, &Score[W].IDEnd, &Score[W].PosStart, &Score[W].PosEnd);
	     fscanf(f2, "%d", &Captures[W].number);
	     for(V = 1; V <= Captures[W].number; V++) {
		 fscanf(f2, "%d", &Captures[W].Positions[V]);
		 fscanf(f2, "%d", &Captures[W].PieceNum[V]);
	     } // Next V;
	 } // Next W;
     } else {
	 Board.MnuHandicap.Enabled = True;
     }
//     ReDim Pieces(1 To PieceNum / 2) As Piece;
//     ReDim TestBoard(BoardSizeX, BoardSizeY);
//     ReDim Grafix[BoardSizeX][BoardSizeY] As Integer;
//     ReDim Squares[BoardSizeX][BoardSizeY] As Integer;
//     ReDim Legal[BoardSizeX][BoardSizeY] As Integer;
//     ReDim OldLegal[BoardSizeX][BoardSizeY] As Integer;
//     ReDim AreaOK[BoardSizeX][BoardSizeY] As Integer;
//     ReDim Camps[BoardSizeX][BoardSizeY] As Integer;
//     ReDim Comp[BoardSizeX][BoardSizeY] As Integer;
//     ReDim Clearing(200) As Empty;
//     ReDim BanMap[BoardSizeX][BoardSizeY] As Map;
//     ReDim BackMap[BoardSizeX][BoardSizeY] As Map;
//     ReDim Attacker[BoardSizeX][BoardSizeY] As Integer;
//     ReDim OldAttack(BoardSizeX, BoardSizeY) As Integer;
//     ReDim PieceMask(6, Pixels)
//     ReDim CompLegal[2000] As LegalList;
//     ReDim KingTally[2000] As Long;
//     ReDim LowBlack[BoardSizeX][BoardSizeY] As Integer;
//     ReDim LowWhite[BoardSizeX][BoardSizeY] As Integer;
     if(!strcmp(Choice, "Wa") && Loading != 1 ) WaDrop();
     if(!strcmp(Choice, "Wa") && Drop == 1 ) strcpy(Boardbmp, "Waboard2.bmp");
     Board.Picture = LoadPicture((sprintf(StringTmp, "%s/boards/%s", Direct, Boardbmp), StringTmp));
     strcpy(Board.Caption, Cap);
     
     // load piece data;

     for(I = 1; I <= PieceNum / 2; I++) {
	fscanf(f1, "%d,%[^,]", &Pieces[I].number, Pieces[I].Name);
	fscanf(f1, ",%[^,],%d", Pieces[I].sname, &Pieces[I].Value);
	fscanf(f1, ",%d,%d,%d", &Pieces[I].PrValue, &Pieces[I].Promotes, &Pieces[I].Graphic);
	fscanf(f1, ",%d", &Pieces[I].PrGraphic);
	for(J = 1; J <= 8; J++) {
	    fscanf(f1, ",%d", &Pieces[I].Moves[J]);
	} // Next J;
	fscanf(f1, ",%c", &Pieces[I].special);
	fscanf(f1, ",%d", &Pieces[I].Mask);
	fscanf(f1, ",%d", &Pieces[I].Range); // WARNING: original .dat files leave this out if 0
     } // Next I;

     // load board data;

     Count = 0; M = 0;
     for(J = 1; J <= BoardSizeY; J++) {
	for(I = 1; I <= BoardSizeX; I++) {
	    fscanf(f1, "%d,", &Squares[I][J]);
	    if(Loading == 1 ) fscanf(f2, "%d", &Squares[I][J]);
	    Grafix[I][J] = -1;
	    if(Squares[I][J] != 0 ) {
		Graphnum = Pieces[abs(Squares[I][J])].Graphic;
		if((Squares[I][J] < 0 && Reverse == 0) || (Squares[I][J] > 0 && Reverse == 1) ) Graphnum = Graphnum + (TotGraph / 2);
/*		if(!strcmp(Choice, "Tai") ) Board.showpic[COUNT] = TaiPieces.Pix[Graphnum - 1]; else */ Board.showpic[COUNT] = Board.Pix[Graphnum - 1];
		if(Reverse == 0 ) {
		    MOVE(0, Board.showpic[COUNT], XStart + ((I - 1) * Pixels), 11 + ((J - 1) * Pixels));
		} else {
		    MOVE(0, Board.showpic[COUNT], XStart + ((BoardSizeX - I) * Pixels), 11 + ((BoardSizeY - J) * Pixels));
		}
		Board.showpic[COUNT].Visible = True;
		Grafix[I][J] = Count;
		Count = Count + 1;
	    }
	} // Next I;
     } // Next J;

//Load Drop Data;

  if(Drop == 1 || !strcmp(Choice, "Wa") ) {
     if(Loading == 1 ) {
	 for(K = Count; K <= Totpiece; K++) {
	     Board.showpic[K].Visible = False;
	 } // Next K;
     }
     fscanf(f1, "%d,", &Capture);
//     ReDim CapRef(Capture * 2) As Integer;
//     ReDim InHand(Capture * 2) As Integer;
//     ReDim CompHeld(Capture * 2) As Integer;
//     ReDim OldHand(Capture * 2) As Integer;
     for(I = 1; I <= Capture; I++) {
	fscanf(f1, "%d,", &CapRef[I]);
	CapRef[I + Capture] = 0 - CapRef[I];
     } // Next I;
     if(Reverse == 0 ) {
	 Board.White.Picture = LoadPicture((sprintf(StringTmp, "%s/%s", Direct, "WhiteDn.bmp"), StringTmp));
	 Board.Black.Picture = LoadPicture((sprintf(StringTmp, "%s/%s", Direct, "BlackUp.bmp"), StringTmp));
     } else {
	 Board.White.Picture = LoadPicture((sprintf(StringTmp, "%s/%s", Direct, "WhiteUp.bmp"), StringTmp));
	 Board.Black.Picture = LoadPicture((sprintf(StringTmp, "%s/%s", Direct, "BlackDn.bmp"), StringTmp));
	 for(I = 1; I <= Capture * 2; I++) {
	    CapRef[I] = 0 - CapRef[I];
	 } // Next I;
     }
     for(I = 1; I <= Capture * 2; I++) {
	InHand[I] = 0;
	if(Loading == 1 ) fscanf(f2, "%d", &InHand[I]);
	Graphnum = Pieces[abs(CapRef[I])].Graphic;
	if(I > Capture ) Graphnum = Graphnum + (TotGraph / 2);
	Board.HandPic[I] = Board.Pix[Graphnum - 1];
	Board.HandPic[I].Visible = False;
	strcpy(Board.Held[I].Caption, "");
	if(Loading == 1 && InHand[I] > 0 ) Board.HandPic[I].Visible = True;
	if(Loading == 1 && InHand[I] > 1 ) sprintf(Board.Held[I].Caption, "%d", InHand[I]);
	if(strcmp(Choice, "Wa") && strcmp(Choice, "Micro") ) {
	    if(I <= Capture ) {
		X = 629 - Pixels;
		Y = 11 + (I - 1) * Pixels;
	    } else {
		X = 10;
		Y = 11 + (Capture - I + Capture) * Pixels;
	    }
	    MOVE(0, Board.HandPic[I], X, Y);
	}
	if(!strcmp(Choice, "Wa") || !strcmp(Choice, "Micro") ) {
	    if(I <= Capture ) {
		if(I <= Capture / 2 ) {
		    X = 629 - Pixels;
		    Y = 11 + (I - 1) * Pixels;
		} else {
		    X = 629 - (Pixels * 2);
		    Y = 11 + (I - (Capture / 2) - 1) * Pixels;
		}
	    } else {
		if(I <= Capture * 1.5 ) {
		    X = 10;
		    Y = 11 + (Capture - I + (Capture / 2)) * Pixels;
		} else {
		    X = 10 + Pixels;
		    Y = 11 + (Capture - (I - (Capture / 2)) + (Capture / 2)) * Pixels;
		}
	    }
	    MOVE(0, Board.HandPic[I], X, Y);
	}
     } // Next I;
  }
  fscanf(f1, "%d", &PieceSizes);
  for(J = 1; J <= PieceSizes; J++) {
      for(K = 1; K <= Pixels-1; K++) {
	  fscanf(f1, "%d,", &PieceMask[J][K]);
      } // Next K;
  } // Next J;
  fclose(f1);
  SetKings();
  NotSet();
  Notation();
  if(Board.WindowState != 2 ) {
      if(strcmp(Choice, "Tai") ) {
	Board.Left = (screen.Width / 2) - 4860;
	Board.Top = (screen.Height / 2) - 3650;
      } else {
	Board.Left = (screen.Width / 2) - 6075;
	Board.Top = (screen.Height / 2) - 4562;
      }
  }
  if((Display == 640 || !strcmp(Choice, "Tai")) && NewGame != 1 ) {
    Board.Left = 5;
    Board.Top = 5;
    Board.WindowState = 2;
  } else {
    Board.WindowState = 0;
  }
  if(!strcmp(Choice, "Tai") ) Board.WindowState = 2;
  if(Loading != 1 && Timing != 1 ) ClocksOn();
  if(strcmp(Board.WhiteClock.Caption, "00:00:00") || strcmp(Board.BlackClock.Caption, "00:00:00") ) Board.Timer1.Enabled = True;
  if(Timing == 1 ) ClocksOff();
  if(!strcmp(Choice, "Tai") || !strcmp(Choice, "Maka") ) {
    if(!strcmp(Computer, "White") || !strcmp(Computer, "Black") || !strcmp(Computer, "Both") ) CompTeach(); else NoCompTeach();
  }
  Board.MnuBest.Visible = False;
  Board.Show();
  Start.MousePointer = 0;
//  Start.Hide();
  if(!strcmp(Computer, "White") || !strcmp(Computer, "Black") ) {
    Board.MnuSwitch.Enabled = True;
    EnableNamedMenuItem("Setup.SwitchSides", True); // [HGM] added
    sprintf(Board.PieceID.Caption, "Computer plays %s", Computer); Notice = 1;
  } else {
    Board.MnuSwitch.Enabled = False;
    EnableNamedMenuItem("Setup.SwitchSides", False); // [HGM] added
  }
  if(!strcmp(Computer, Turn) || !strcmp(Computer, "Both") ) CompMain();
}

void SetRules ()
{ // Menu function.
#if 0
  INT Z;
// TODO:  if(GeneralInfo == 1 ) Unload RulesHelp;
  GeneralInfo = 0;
  for(Z = 0; Z <= 17; Z++) {
    RulesHelp.Title[Z].Visible = False;
  } // Next Z;
  if(GameNo < 10 ) RulesHelp.Title[GameNo - 1].Visible = True; else RulesHelp.Title[GameNo].Visible = True;
  RulesHelp.CmdPiece.Visible = True;
  RulesHelp.Show();
#else
  static char text[20000];
  STRING name;
  FILE *f;
  sprintf(name, "%s/%s.rul", Direct, Choice);
  if((f = fopen(name, "r"))) {
    fread(text, 1, 20000, f);
    TagsPopUp(text+1, NULL);
    fclose(f);
  }
#endif
}

void SetSuggest ()
{ // Menu function.
  Suggest = 1;
  OldSeeMove = SeeMove;
  strcpy(OldComputer, Computer);
  strcpy(Computer, Turn);
  strcpy(Board.Caption, "The Computer suggests...."); Notice = 5;
  RealLevel = Level; Level = 1;
  CompMain();
  Level = RealLevel;
  LegalMoves = 0;
}

void SetTeach ()
{
    if(Board.MnuVer1.Enabled == False ) Board.MnuVer1.Enabled = True; else Board.MnuVer1.Enabled = False;
    if(Board.MnuVer2.Enabled == False ) Board.MnuVer2.Enabled = True; else Board.MnuVer2.Enabled = False;
    if(Board.MnuVer1.Checked == False ) Board.MnuVer1.Checked = True; else Board.MnuVer1.Checked = False;
    if(Board.MnuVer2.Checked == False ) Board.MnuVer2.Checked = True; else Board.MnuVer2.Checked = False;
}

void SetThreat ()
{   // Menu function. Threat = "On" or "Off" depending on the item.
    if(Board.MnuThreatOn.Enabled == False ) Board.MnuThreatOn.Enabled = True; else Board.MnuThreatOn.Enabled = False;
    if(Board.MnuThreatOff.Enabled == False ) Board.MnuThreatOff.Enabled = True; else Board.MnuThreatOff.Enabled = False;
    if(Board.MnuThreatOn.Checked == False ) Board.MnuThreatOn.Checked = True; else Board.MnuThreatOn.Checked = False;
    if(Board.MnuThreatOn.Checked == False ) Board.MnuThreatOff.Checked = True; else Board.MnuThreatOff.Checked = False;
    if(SeeMove == 1 && strcmp(OldThreat, "On") && strcmp(OldThreat, "Off") ) {
	if(Board.MnuThreatOff.Enabled == False ) strcpy(Board.PieceID.Caption, "Show Influence or Threats Off");
    }
    if(strcmp(OldThreat, "On") && strcmp(OldThreat, "Off") ) {
	if(Board.MnuThreatOff.Enabled == True ) strcpy(Board.PieceID.Caption, "Show Influence or Threats On");
    }
    Notice = 1;
}

void SetWhitePlayer ()
{ // Menu function, called when white player changes, after setting Computer.
  if(Board.MnuWhitePlayer.Enabled == False ) Board.MnuWhitePlayer.Enabled = True; else Board.MnuWhitePlayer.Enabled = False;
  if(Board.MnuWhiteComp.Enabled == False ) Board.MnuWhiteComp.Enabled = True; else Board.MnuWhiteComp.Enabled = False;
  if(Board.MnuWhitePlayer.Checked == False ) Board.MnuWhitePlayer.Checked = True; else Board.MnuWhitePlayer.Checked = False;
  if(Board.MnuWhiteComp.Checked == False ) Board.MnuWhiteComp.Checked = True; else Board.MnuWhiteComp.Checked = False;
  if(!strcmp(Choice, "Tai") || !strcmp(Choice, "Maka") ) {
    if(!strcmp(Computer, "White") || !strcmp(Computer, "Black") || !strcmp(Computer, "Both") ) CompTeach(); else NoCompTeach();
  }
  if(!strcmp(Computer, "Both") ) CompVComp();
}

void ShowMove ()
{
  if(GameOver == 1 || Checked > 0 ) return;
  if(Squares[NewFile][NewRank] == 0 && (Range != 1 || Influence > 0) ) {
    if(Pieces[abs(Squares[InitFile][InitRank])].special != 'T' || M != 1 ) {
	if(Pieces[abs(Squares[InitFile][InitRank])].special == 'T' ) Tetrarchs();
	if(Blocked != 1 ) {
	    Legal[NewFile][NewRank] = 1;
	    Board.FillColor = 0xFFFFFF;
	    if(SeeMove == 1 || CompMove == 1 || Evaluate == 1 ) { SeeFile = NewFile; SeeRank = NewRank; LookMove(); }
	    if(Demon == 1 ) FireDemon();
	}
	Blocked = 0;
    }
    if(!strcmp(Choice, "Tenjiku") ) CheckBurn();
  } else {
    if(Pieces[abs(Squares[InitFile][InitRank])].special != 'T' || M != 1 ) Last = 1;
    if(Pieces[abs(Squares[InitFile][InitRank])].special == 'T' ) Tetrarchs();
    if(Blocked != 1 ) {
	if((Sgn(Squares[InitFile][InitRank]) != Sgn(Squares[NewFile][NewRank]) || Influence > 0) && Legal[NewFile][NewRank] != 4 && Squares[NewFile][NewRank] != 0 ) {
	    if(Squares[NewFile][NewRank] != 0 && Range == 1 ) TestStrength();
	    if(!strcmp(Choice, "Chu") && !strcmp(Pieces[abs(Squares[NewFile][NewRank])].Name, "Lion") && strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "Lion") ) {
		NoLionCapture = 0;
		if(!strcmp(Turn, "Black") ) {
		    if(WhiteLion == 1 ) NoLionCapture = 1;
		} else {
		    if(BlackLion == 1 ) NoLionCapture = 1;
		}
	    }
	    if(Weaker != 1 && NoLionCapture != 1 ) {
		Board.FillColor = 0xFFL;
		Legal[NewFile][NewRank] = 1;
		if(SeeMove == 1 ) { SeeFile = NewFile; SeeRank = NewRank; LookMove(); }
		if(Demon == 1 ) FireDemon();
		if(!strcmp(Choice, "Tenjiku") ) CheckBurn();
	    }
	    NoLionCapture = 0;
	}
    }
    Blocked = 0;
  }
}

void ShowProm ()
{ // Button2-Down handler for Form & board piece. (NewX, NewY) is coords.
  // CCC = 1 and ClickPiece = 0 (Form) or 1 (piece). NewButton is button nr for piece
  STRING PromName;
  INT K, L;
  if(strcmp(Computer, Turn) && strcmp(Computer, "Both") && Level != 0 && GameOver != 1 ) {
   if(Selection != 0 || MovePiece == 1 ) {
    EndSelect();
   } else {
    if(Handicap == 1 || Reduce == 1 ) {
	strcpy(Board.Caption, Cap);
	Handicap = 0; Reduce = 0;
	Board.Timer1.Enabled = True;
    } else {
	if(ClickPiece == 1 ) {
	    RightClick = 1;
	    strcpy(Board.PieceID.Caption, "");
	    Board.PieceID.ForeColor = 0x8000L;
	    for(K = 1; K <= BoardSizeY; K++) {
		for(L = 1; L <= BoardSizeX; L++) {
		    if(Grafix[L][K] == I ) {
			if(Pieces[abs(Squares[L][K])].Promotes > 0 && strcmp(Pieces[abs(Squares[L][K])].Name, "Killer Whale") ) sprintf(PromName, "Promotes to %s", Pieces[abs(Pieces[abs(Squares[L][K])].Promotes)].Name); else strcpy(PromName, "Does not promote");
			if(!strcmp(Pieces[abs(Squares[L][K])].Name, "Porpoise") ) strcpy(PromName, "Promotes to Killer Whale");
			strcpy(Board.PieceID.Caption, PromName);
			InitFile = L; InitRank = K;
			if(SeeMove == 1 && !strcmp(Threat, "On") ) ActingPieces();
		    }
		} // Next L;
	    } // Next K;
	    ClickPiece = 0;
	} else {
	    FormActing();
	}
    }
   }
  }
}

void SingleStep ()
{
  Last = 0; M = 0; NewFile = InitFile; NewRank = InitRank;
  while( NewFile + FileInc > 0 && NewFile + FileInc <= BoardSizeX && NewRank + RankInc > 0 && NewRank + RankInc <= BoardSizeY && M < MoveTest && Last == 0) {
    NewFile = NewFile + FileInc;
    NewRank = NewRank + RankInc;
    M = M + 1;
    ShowMove();
    if(NewRank <= BoardSizeY && NewRank > 0 && NewFile > 0 && NewFile <= BoardSizeX ) {
	if(Range == 1 && Squares[NewFile][NewRank] != 0 ) TestStrength();
	if(Range == 1 && Weaker == 0 ) Last = 0;
    }
    if(Hook == 1 && Last != 1 ) HookMove();
  }
  Range = 0; Weaker = 0; Hook = 0;
}

void SpecialMove ()
{
  switch( Pieces[abs(Squares[InitFile][InitRank])].special) {
    case 'L': if(LionPiece == I ) Lion2(); else Lion(); break;
    case 'T': Igui(); break;
    case 'D': Area = 2; P = InitFile; N = InitRank; AreaMove(); break;
    case 'F': Area = 3; P = InitFile; N = InitRank; AreaMove(); break;
    //case "E";  Emperor();
  }
  if(!strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "Vice General") ) {
    Area = 3; N = InitRank; P = InitFile;
    AreaMove();
  }
}

void SquareReplace ()
{
  INT K, L, Found;
  STRING VictimSTR;
  if(MovePiece == 1 ) {
    Move2();
  } else {
  K = 1;
  do {
    L = 1;
    do {
	if(Grafix[L][K] == I ) {
	    InitFile = L;
	    InitRank = K;
	    Found = 1;
       }
       L = L + 1;
    } while( !(L > BoardSizeX || Found == 1) );
    K = K + 1;
  } while( !(K > BoardSizeY || Found == 1) );
  Found = 0;
  strcpy(VictimSTR, Pieces[abs(Squares[InitFile][InitRank])].Name);
  if(Squares[InitFile][InitRank] > 0 && (!strcmp(VictimSTR, "Prince") || !strcmp(VictimSTR, "Crown Prince")) ) BlackPrince = BlackPrince - 1;
  if(Squares[InitFile][InitRank] < 0 && (!strcmp(VictimSTR, "Prince") || !strcmp(VictimSTR, "Crown Prince")) ) WhitePrince = WhitePrince - 1;
  Squares[InitFile][InitRank] = Selection;
  Board.showpic[Grafix[InitFile][InitRank]].Visible = False;
  MOVE(1, Board.showpic[Grafix[InitFile][InitRank]], 0, 0);
  NewGraf = Grafix[InitFile][InitRank];
  SetGrafix();
  Reduce = 0; MovePiece = 0;
  }
}

void StartUp ()
{
  if(!strcmp(Choice, "Micro") ) { strcpy(GameName, "Micro"); GameNo = 14; } // Load Micro; Set Board = Micro; }
  if(!strcmp(Choice, "Whale") ) { strcpy(GameName, "Whale"); GameNo = 15; } // Load Whale; Set Board = Whale; }
  if(!strcmp(Choice, "Shogi") ) { strcpy(GameName, ""); GameNo = 2; } // Load Shogi; Set Board = Shogi; }
  if(!strcmp(Choice, "Tori") ) { strcpy(GameName, "Bird"); GameNo = 1; } // Load Tori; Set Board = Tori; }
  if(!strcmp(Choice, "Sho") ) { strcpy(GameName, "Little"); GameNo = 11; } // Load Shogi; Set Board = Shogi; }
  if(!strcmp(Choice, "Wa") ) { strcpy(GameName, "Wa"); GameNo = 3; } // Load Wa; Set Board = Wa; }
  if(!strcmp(Choice, "Chu") ) { strcpy(GameName, "Middle"); GameNo = 4; } // } // Load Chu; Set Board = Chu; }
  if(!strcmp(Choice, "Dai") ) { strcpy(GameName, "Great"); GameNo = 5; } // Load Dai; Set Board = Dai; }
  if(!strcmp(Choice, "Tenjiku") ) { strcpy(GameName, "Exotic"); GameNo = 6; } // Load Tenjiku; Set Board = Tenjiku; }
  if(!strcmp(Choice, "DaiDai") ) { strcpy(GameName, "Great Great"); GameNo = 7; } // Load DaiDai; Set Board = DaiDai; }
  if(!strcmp(Choice, "Maka") ) { strcpy(GameName, "Ultra Great Great"); GameNo = 8; } // Load Maka; Set Board = Maka; }
  if(!strcmp(Choice, "Tai") ) { strcpy(GameName, "Grand"); GameNo = 9; } // Load Tai; Set Board = Tai; }
  if(!strcmp(Choice, "HShogi") ) { strcpy(GameName, "Early"); GameNo = 12; } // Load HeianSho; Set Board = HeianSho; }
  if(!strcmp(Choice, "Heian") ) { strcpy(GameName, "Early Great"); GameNo = 10; } // Load Heian; Set Board = Heian; }
  if(!strcmp(Choice, "Mini") ) { strcpy(GameName, "Mini"); GameNo = 13; } // Load Mini; Set Board = Mini; }
  if(!strcmp(Choice, "Yari") ) { strcpy(GameName, "Yari"); GameNo = 16; } // Load Yari; Set Board = Yari; }
  if(!strcmp(Choice, "Judkin") ) { strcpy(GameName, "Judkin's"); GameNo = 17; } // Load Judkin; Set Board = Judkin; }
  LoadBoard(Choice); // replaces all the commented-out loads above
  SetPieces();
  DrawBoard(); // [HGM]added to redraw the board with pieces
#if 0
// Unload is not needed
  if(strcmp(OldChoice, Choice) ) {
    if(!strcmp(OldChoice, "Micro")) Unload Micro;
    if(!strcmp(OldChoice, "Whale")) Unload Whale;
    if(!strcmp(OldChoice, "Sho")) Unload Shogi;
    if(!strcmp(OldChoice, "Shogi")) Unload Shogi;
    if(!strcmp(OldChoice, "Tori")) Unload Tori;
    if(!strcmp(OldChoice, "Wa")) Unload Wa;
    if(!strcmp(OldChoice, "Chu")) Unload Chu;
    if(!strcmp(OldChoice, "Dai")) Unload Dai;
    if(!strcmp(OldChoice, "Tenjiku")) Unload Tenjiku;
    if(!strcmp(OldChoice, "DaiDai")) Unload DaiDai;
    if(!strcmp(OldChoice, "Maka")) Unload Maka;
    if(!strcmp(OldChoice, "Tai")) { Unload Tai; Unload TaiPieces; }
    if(!strcmp(OldChoice, "Heian")) Unload Heian;
    if(!strcmp(OldChoice, "HShogi")) Unload HeianSho;
    if(!strcmp(OldChoice, "Mini")) Unload Mini;
    if(!strcmp(OldChoice, "Yari")) Unload Yari;
    if(!strcmp(OldChoice, "Judkin")) Unload Judkin;
  } else {
    Board.MousePointer = 0;
  }
#endif
}

void SuggestMove ()
{
  INT X, Y, GG, HH, II, MM;
  SeeMove = OldSeeMove;
  Suggest = 0;
  strcpy(Computer, OldComputer);
  Board.ForeColor = 0xFFL;
  Board.FillStyle = 1;
  Board.DrawWidth = 2;
  Board.ForeColor = 0xFFFFFF;
  if(CompLegal[BestMove].StartFile != 0 ) {
    if(Reverse == 0 ) {
	Board.Line(XStart - 1 + (CompLegal[BestMove].StartFile - 1) * Pixels, 10 + (CompLegal[BestMove].StartRank - 1) * Pixels, XStart - 1 + (CompLegal[BestMove].StartFile * Pixels), 10 + (CompLegal[BestMove].StartRank * Pixels), NOCOLOR, "B");
    } else {
	Board.Line(XStart - 1 + (BoardSizeX - CompLegal[BestMove].StartFile) * Pixels, 10 + (BoardSizeY - CompLegal[BestMove].StartRank) * Pixels, XStart - 1 + (BoardSizeX - CompLegal[BestMove].StartFile + 1) * Pixels, 10 + (BoardSizeY - CompLegal[BestMove].StartRank + 1) * Pixels, NOCOLOR, "B");
    }
  } else {
    for(HH = 1; HH <= Capture * 2; HH++) {
	if(CapRef[HH] == CompLegal[BestMove].EndPiece ) II = HH;
    } // Next HH;
    if(strcmp(Choice, "Wa") && strcmp(Choice, "Micro") ) {
	if(II <= Capture ) {
	    X = 629 - Pixels;
	    Y = 11 + (II - 1) * Pixels;
	} else {
	    X = 10;
	    Y = 11 + (Capture - II + Capture) * Pixels;
	}
	Board.Line(X, Y, X + Pixels, Y + Pixels, NOCOLOR, "B");
    }
    if(!strcmp(Choice, "Wa") || !strcmp(Choice, "Micro") ) {
    if(II <= Capture ) {
	if(II <= Capture / 2 ) {
	    X = 629 - Pixels;
	    Y = 11 + (II - 1) * Pixels;
	} else {
	    X = 629 - (Pixels * 2);
	    Y = 11 + (II - (Capture / 2) - 1) * Pixels;
	}
    } else {
	if(II <= Capture * 1.5 ) {
	    X = 10;
	    Y = 11 + (Capture - II + (Capture / 2)) * Pixels;
	} else {
	    X = 10 + Pixels;
	    Y = 11 + (Capture - (II - (Capture / 2)) + (Capture / 2)) * Pixels;
	}
    }
    Board.Line(X, Y, X + Pixels, Y + Pixels, NOCOLOR, "B");
  }
  }
  Board.FillStyle = 0;
  Board.DrawWidth = 1;
  Board.ForeColor = 0x0L;
  if(Reverse == 0 ) {
    Board.Circle(XStart + (CompLegal[BestMove].EndFile - 1) * Pixels + (Pixels / 2), 11 + (CompLegal[BestMove].EndRank - 1) * Pixels + (Pixels / 2), Pixels / 4);
  } else {
    Board.Circle(XStart + (BoardSizeX - CompLegal[BestMove].EndFile) * Pixels + (Pixels / 2), 11 + (BoardSizeY - CompLegal[BestMove].EndRank) * Pixels + (Pixels / 2), Pixels / 4);
  }
  for(MM = 1; MM <= BoardSizeY; MM++) {
    for(GG = 1; GG <= BoardSizeX; GG++) {
	Squares[GG][MM] = Comp[GG][MM];
    } // Next GG;
  } // Next MM;
  ResetLegal();
}

void SwitchCompPlayer ()
{ // Menu function.
  if(LegalMoves == 0 ) {
    if(!strcmp(Computer, "White") ) {
	strcpy(Computer, "Black");
	Board.MnuWhitePlayer.Enabled = False; Board.MnuBlackPlayer.Enabled = True;
	Board.MnuWhitePlayer.Checked = True;  Board.MnuBlackPlayer.Checked = False;
	Board.MnuWhiteComp.Enabled = True; Board.MnuBlackComp.Enabled = False;
	Board.MnuBlackComp.Checked = True; Board.MnuWhiteComp.Checked = False;
	strcpy(Board.PieceID.Caption, "Computer plays Black"); Notice = 1;
	if(!strcmp(Turn, "Black") ) {
	    FirstSeeMove = SeeMove;
	    SeeMove = 1;
	    CompMain();
	    SeeMove = FirstSeeMove;
	}
    } else {
	strcpy(Computer, "White");
	Board.MnuWhitePlayer.Enabled = True; Board.MnuBlackPlayer.Enabled = False;
	Board.MnuWhitePlayer.Checked = False;  Board.MnuBlackPlayer.Checked = True;
	Board.MnuWhiteComp.Enabled = False; Board.MnuBlackComp.Enabled = True;
	Board.MnuBlackComp.Checked = False; Board.MnuWhiteComp.Checked = True;
	strcpy(Board.PieceID.Caption, "Computer plays White"); Notice = 1;
	if(!strcmp(Turn, "White") ) {
	    FirstSeeMove = SeeMove;
	    SeeMove = 1;
	    CompMain();
	    SeeMove = FirstSeeMove;
	}
    }
  }
}

void Take2 ()
{
  INT V, Location;
  if((LegalMoves == 0 || Checked == 2) && LionPiece != I && XA == 0 ) {
   if(TurnCount > 0 ) {
    GameOver = 0;
    TurnCount = TurnCount - 1;
    Location = Score[TurnCount].PosEnd;
    if(Location > 0 ) {
	Rank = Int(Location / (BoardSizeX + 1));
	File = Location - (Rank * (BoardSizeX + 1));
	Squares[File][Rank] = Score[TurnCount].IDEnd;
    }
    Location = Score[TurnCount].PosStart;
    if(Location > 0 ) {
	InitRank = Int(Location / (BoardSizeX + 1));
	InitFile = Location - (InitRank * (BoardSizeX + 1));
	Squares[InitFile][InitRank] = Score[TurnCount].IDStart;
	if(Squares[InitFile][InitRank] < 0 ) {
	    if(!strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "Emperor") ) { WhiteEmpX = InitFile; WhiteEmpY = InitRank; }
	    if(!strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "King") ) { WhiteKingX = InitFile; WhiteKingY = InitRank; }
	}
	if(Squares[InitFile][InitRank] > 0 ) {
	    if(!strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "Emperor") ) { BlackEmpX = InitFile; BlackEmpY = InitRank; }
	    if(!strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "King") ) { BlackKingX = InitFile; BlackKingY = InitRank; }
	}
   }
   if(InitFile != File || InitRank != Rank ) Squares[File][Rank] = 0;
   if(Score[TurnCount].IDEnd != 0 ) {
	if(Score[TurnCount].PosStart == 0 ) {
	    CaptPiece = Score[TurnCount].IDEnd;
	    Squares[File][Rank] = 0;
	    AddHand3();
	}
    }
    for(V = 1; V <= Captures[TurnCount].number; V++) {
	Location = Captures[TurnCount].Positions[V];
	InitRank = Int(Location / (BoardSizeX + 1));
	InitFile = Location - (InitRank * (BoardSizeX + 1));
	Squares[InitFile][InitRank] = Captures[TurnCount].PieceNum[V];
    } // Next V;
    if(Drop == 1 && Captures[TurnCount].number > 0 ) ReduceHand2();
    if(!strcmp(Turn, "Black") ) {
	strcpy(Turn, "White");
	strcpy(Board.NextMove.Caption, "White to Move");
    } else {
	MoveCount = MoveCount - 1; strcpy(Turn, "Black");
	strcpy(Board.NextMove.Caption, "Black to Move");
    }
    if(TurnCount == 0 ) {
	strcpy(Board.LastMove.Caption, "");
	if(HandGame != 1 && !strcmp(Turn, "Black") ) Board.MnuHandicap.Enabled = True;
	if(!strcmp(Turn, "Black") ) MoveCount = 0;
    } else {
	if(Asc(Score[TurnCount].Caption) > 47 && Asc(Score[TurnCount].Caption) < 58 ) {
	    strcpy(Board.LastMove.Caption, Score[TurnCount].Caption);
	} else {
	    sprintf(Board.LastMove.Caption, "%d. %s", MoveCount, Score[TurnCount].Caption);
	}
    }
    Backwards = 1;
   }
  }
}

void TakeAll ()
{ // Menu function.
  if(LegalMoves == 0 ) {
    while( TurnCount > 0 || (TurnCount > 1 && HandGame != 1) ) {
	Take2();
    }
    strcpy(Board.Caption, Cap);
    RestoreGrafix();
    SetKings();
  }
}

void TakeBack ()
{ // Menu function.
  INT V, Location;
  if((LegalMoves == 0 || Checked == 2) && LionPiece != I && XA == 0 ) {
   Notice = 0;
   strcpy(Board.PieceID.Caption, "");
   if(Tabbing != 1 ) strcpy(Board.Caption, Cap);
   BugFix();
   if(TurnCount < 1 ) {
    Board.PieceID.ForeColor = 0xFFL;
    strcpy(Board.PieceID.Caption, "No moves to take back!");
    Notice = 1;
   } else {
    GameOver = 0;
    TurnCount = TurnCount - 1;
    Location = Score[TurnCount].PosEnd;
    if(Location > 0 ) {
	Rank = Int(Location / (BoardSizeX + 1));
	File = Location - (Rank * (BoardSizeX + 1));
	Squares[File][Rank] = Score[TurnCount].IDEnd;
    }
    Location = Score[TurnCount].PosStart;
    if(Location > 0 ) {
	InitRank = Int(Location / (BoardSizeX + 1));
	InitFile = Location - (InitRank * (BoardSizeX + 1));
	Squares[InitFile][InitRank] = Score[TurnCount].IDStart;
	if(Squares[InitFile][InitRank] < 0 ) {
	    if(!strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "Emperor") ) { WhiteEmpX = InitFile; WhiteEmpY = InitRank; }
	    if(!strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "King") ) { WhiteKingX = InitFile; WhiteKingY = InitRank; }
	}
	if(Squares[InitFile][InitRank] > 0 ) {
	    if(!strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "Emperor") ) { BlackEmpX = InitFile; BlackEmpY = InitRank; }
	    if(!strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "King") ) { BlackKingX = InitFile; BlackKingY = InitRank; }
	}
   }
    if(InitFile != File || InitRank != Rank ) Squares[File][Rank] = 0;
    if(Score[TurnCount].IDEnd == 0 ) {
	NewGraf = 0;
	while( Board.showpic[NewGraf].Visible == True ) {
	    NewGraf = NewGraf + 1;
	}
	SetGrafix();
    } else {
	NewGraf = Grafix[File][Rank];
	Grafix[File][Rank] = -1;
	Board.showpic[NewGraf].Visible = False;
	MOVE(1, Board.showpic[NewGraf], 0, 0);
	if(Score[TurnCount].PosStart > 0 ) {
	    SetGrafix();
	} else {
	    CaptPiece = Score[TurnCount].IDEnd;
	    Squares[File][Rank] = 0;
	    AddHand();
	}
    }
    for(V = 1; V <= Captures[TurnCount].number; V++) {
	Location = Captures[TurnCount].Positions[V];
	InitRank = Int(Location / (BoardSizeX + 1));
	InitFile = Location - (InitRank * (BoardSizeX + 1));
	Squares[InitFile][InitRank] = Captures[TurnCount].PieceNum[V];
	NewGraf = 0;
	while( Board.showpic[NewGraf].Visible == True ) {
	    NewGraf = NewGraf + 1;
	}
	SetGrafix();
    } // Next V;
    if(Drop == 1 && Captures[TurnCount].number > 0 ) ReduceHand();
    if(!strcmp(Turn, "Black") ) {
	strcpy(Turn, "White");
	strcpy(Board.NextMove.Caption, "White to Move");
    } else {
	MoveCount = MoveCount - 1; strcpy(Turn, "Black");
	strcpy(Board.NextMove.Caption, "Black to Move");
    }
    if(TurnCount == 0 ) {
	strcpy(Board.LastMove.Caption, "");
	if(HandGame != 1 && !strcmp(Turn, "Black") ) Board.MnuHandicap.Enabled = True;
	if(!strcmp(Turn, "Black") ) MoveCount = 0;
    } else {
	if(Asc(Score[TurnCount].Caption) > 47 && Asc(Score[TurnCount].Caption) < 58 ) {
	    strcpy(Board.LastMove.Caption, Score[TurnCount].Caption);
	} else {
	    sprintf(Board.LastMove.Caption, "%d. %s", MoveCount, Score[TurnCount].Caption);
	}
    }
    Backwards = 1;
    SetKings();
    if(TurnCount > 0 ) {
	Location = Score[TurnCount - 1].PosEnd;
	if(Location > 0 ) {
	    Rank = Int(Location / (BoardSizeX + 1));
	    File = Location - (Rank * (BoardSizeX + 1));
	}
	LastPieceX = File; LastPieceY = Rank;
    } else {
	LastPieceX = -1; LastPieceY = -1;
    }
    if(!strcmp(Turn, Computer) && Tabbing != 1 ) {
	if(TurnCount > 0 ) {
	    TakeBack();
	} else {
	    FirstSeeMove = SeeMove;
	    SeeMove = 1;
	    CompMain();
	}
    }
    if(Tabbing == 1 ) {
	if(!strcmp(Computer, "Black") || !strcmp(Computer, "White") ) {
	    sprintf(Board.Caption, "Move Taken Back (% d of% d ) ; Press [ESC] to continue play.", TurnCount, EndTurn);
	} else {
	    sprintf(Board.Caption, "Move Taken Back (% d of% d )", TurnCount, EndTurn);
	}
    }
   }
  }
}

void TeachingKing ()
{
  INT X, Y;
  for(Y = InitRank - 3; Y <= InitRank + 3; Y++) {
    for(X = InitFile - 3; X <= InitFile + 3; X++) {
	if(X > 0 && X <= BoardSizeX && Y > 0 && Y <= BoardSizeY ) {
	    if(Squares[X][Y] == 0 || Sgn(Squares[X][Y]) != Sgn(Squares[InitFile][InitRank]) ) {
		if(abs(Y - InitRank) <= 1 && abs(X - InitFile) <= 1 ) {
		    Board.FillColor = 0x800080;
		    Legal[X][Y] = 6;
		 } else {
		    if(abs(Y - InitRank) == 3 || abs(X - InitFile) == 3 ) {
			Board.FillColor = 0xFFFF00;
			Legal[X][Y] = 1;
		    } else {
			Board.FillColor = 0xFF0000;
			Legal[X][Y] = 2;
		    }
		}
		if(SeeMove == 1 ) { SeeFile = X; SeeRank = Y; LookMove(); }
	    }
	}
    } // Next X;
  } // Next Y;
}

void TenjikuScore1 ()
{
  STRING TmpSTR;
  INT AB, CD;
  CD = strlen(FirstScore);
  for(AB = 1; AB <= CD; AB++) {
    if(FirstScore[AB-1] == ' ' || FirstScore[AB-1] == 10 || FirstScore[AB-1] == 13 ) {
	strcpy(TmpSTR, FirstScore); FirstScore[AB-1] = 0;
	strcat(FirstScore, TmpSTR + AB);
    }
  } // Next AB;
}

void TenjikuScore2 ()
{
  STRING TmpSTR;
  INT AB, CD;
  CD = strlen(SecondScore);
  for(AB = 1; AB <= CD; AB++) {
    if(SecondScore[AB-1] == ' ' || SecondScore[AB-1] == 10 || SecondScore[AB-1] == 13 ) {
	strcpy(TmpSTR, SecondScore); SecondScore[AB-1] = 0;
	strcat(SecondScore, TmpSTR + AB);
    }
  } // Next AB;
}

void Territory ()
{
  INT AB, CD;
  if((!strcmp(Threat, "On") && Tilde == 0) || (Tilde == 1 && LegalMoves == 0) ) {
    if(Tilde == 1 ) {
	OldSeeMove = SeeMove;
	SeeMove = 1;
    }
    if(Tilde != 1 ) GetSquare(); else InitFile = 1;
    if(InitFile > 0 ) {
	Influence = 2;
	for(AB = 1; AB <= BoardSizeY; AB++) {
	    for(CD = 1; CD <= BoardSizeX; CD++) {
		if(Squares[CD][AB] != 0 ) {
		    InitFile = CD; InitRank = AB;
		    Validate();
		}
	    } // Next CD;
	} // Next AB;
	DisplayTerritory();
	Influence = 0;
    }
    if(Tilde == 1 ) SeeMove = OldSeeMove;
  }
}


void TestAhead ()
{
  INT BB, VX, YZ, TU;
// Find Legal Moves;

  for(AA = 1; AA <= BoardSizeY; AA++) {
    for(BB = 1; BB <= BoardSizeX; BB++) {
	Squares[BB][AA] = Comp[BB][AA];
    } // Next BB;
  } // Next AA;
  CompMove = 1; LegalMoves = 0;
  for(YZ = 1; YZ <= BoardSizeY; YZ++) {
    for(VX = 1; VX <= BoardSizeX; VX++) {
      if(!strcmp(Turn, "White") ) {
	if(Squares[VX][YZ] < 0 ) {
	  InitFile = VX; InitRank = YZ;
	  I = Grafix[VX][YZ];
	  Validate();
	}
      } else {
	if(Squares[VX][YZ] > 0 ) {
	  InitFile = VX; InitRank = YZ;
	  I = Grafix[VX][YZ];
	  Validate();
	}
      }
    } // Next VX;
  DoEvents();
  } // Next YZ;

// Find Legal Drops;

  if(Drop == 1 ) {
    if((!strcmp(Turn, "Black") && Reverse == 0) || (!strcmp(Turn, "White") && Reverse == 1) ) {
      for(TU = 1; TU <= Capture; TU++) {
	ResetLegal();
	if(InHand[TU] > 0 ) {
	  I = TU; InitFile = 0; InitRank = 0;
	  HeldValid();
	  DoEvents();
	}
      } // Next TU;
    } else {
      for(TU = Capture + 1; TU <= Capture * 2; TU++) {
	if(InHand[TU] > 0 ) {
	  ResetLegal();
	  I = TU; InitFile = 0; InitRank = 0;
	  HeldValid();
	  DoEvents();
	}
      } // Next TU;
    }
  }
  ConsiderMove();
  Evaluate = 0; CompMove = 0; Influence = 0; EndMove = 0;
}

void TestDrop ()
{
  INT DD;
  for(DD = 1; DD <= Capture * 2; DD++) {
    if(CompLegal[BestMove].StartPiece == CapRef[DD] ) I = DD;
  } // Next DD;
  Legal[CompLegal[BestMove].EndFile][CompLegal[BestMove].EndRank] = 1;
}

void TestOther ()
{
  STRING ASTR;
  INT Location;
  strcpy(ASTR, Pieces[abs(Score[TurnCount].IDStart)].Name);
  if(strcmp(ASTR, "Pawn") && strcmp(ASTR, "Sparrow") && strcmp(ASTR, "Sparrow Pawn") && strcmp(ASTR, "Swallow") && strcmp(ASTR, "Dolphin") ) {
    OriginalRank = Rank; OriginalFile = File;
    OldSeeMove = SeeMove; SeeMove = 0;
    OriginalPiece = Squares[File][Rank]; Squares[File][Rank] = 0;
    Location = Score[TurnCount].PosStart;
    InitRank = Int(Location / (BoardSizeX + 1));
    InitFile = Location - (InitRank * (BoardSizeX + 1));
    Squares[InitFile][InitRank] = Score[TurnCount].IDStart;
    InitFile = FirstFile; InitRank = FirstRank;
    Validate();
    Rank = OriginalRank; File = OriginalFile;
    Location = Score[TurnCount].PosStart;
    InitRank = Int(Location / (BoardSizeX + 1));
    InitFile = Location - (InitRank * (BoardSizeX + 1));
    Squares[InitFile][InitRank] = 0; Squares[File][Rank] = OriginalPiece;
    SeeMove = OldSeeMove;
    if(Legal[File][Rank] > 0 ) Testing123 = 1; else Testing123 = 0;
  }
}

void TestStrength ()
{
  Weaker = 0;
  if(Pieces[abs(Squares[NewFile][NewRank])].special == 'G' || Pieces[abs(Squares[NewFile][NewRank])].special == 'C' ) {
    if(Pieces[abs(Squares[InitFile][InitRank])].Value <= Pieces[abs(Squares[NewFile][NewRank])].Value || !strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "Rook General") ) {
	  Weaker = 1;
    }
  }
}

void Tetrarchs ()
{
  if(InitFile < BoardSizeX ) {
    if(FileInc == 1 && RankInc == 0 && M == 3 && Legal[InitFile + 1][InitRank] == 4 ) Blocked = 1;
  }
  if(InitFile > 1 ) {
    if(FileInc == -1 && RankInc == 0 && M == 3 && Legal[InitFile - 1][InitRank] == 4 ) Blocked = 1;
  }
}

void TextScore ()
{ // Menu function.
  INT K, W;
  FILE *f3;
// TODO:  On Error Resume Next;
  strcpy(Board.CMDiagram.DialogTitle, "Create Text Score");
  Board.CMDiagram.Flags = 0x400L | 0x800L | 0x4L;
  Board.CMDiagram.Action = 2;
  if(Err == 32755 ) return; // TODO
  strcpy(SavedSTR, Board.CMDiagram.Filename);

  f3 = fopen(SavedSTR, "w");
  if(strcmp(GameName, "") ) {
    fprintf(f3, "%s Shogi", GameName);
    for(I = 1; I <= strlen(GameName) + 6; I++) {
      fprintf(f3, "=");
    } // Next I;
    fprintf(f3, "\n");
  }
  fprintf(f3, "%s\n", SaveTitleSTR);
  K = 0;
  for(W = 1; W <= TurnCount; W += 2) {
    K = K + 1;
    strcpy(FirstScore, Score[W].Caption);
    strcpy(SecondScore, Score[W + 1].Caption);
    strcpy(Board.Caption, strstr(FirstScore, " "));
    if(strchr(FirstScore, '\n') ) TenjikuScore1(); // FIXME: is this OK?
    if(strchr(SecondScore, '\n') ) TenjikuScore2();
    if(HandGame == 1 && W == 1 ) {
      fprintf(f3, "1. -  %s  ", SecondScore);
    } else {
      fprintf(f3, "%d.%s %s ", K, FirstScore, SecondScore);
    }
  } // Next W;
  fclose(f3);
  sprintf(Board.Caption, "Score text file %s created.", Board.CMDiagram.Filename); Notice = 1;
}

void TooSmall ()
{
  int Response; // write-only?
  Response = MsgBox("A screen size of at least 800x600 is required to play this variant. You should use Windows 'Setup' to change your screen size before attempting to play Tai Shogi.", 0, "Tai Shogi (Grand Shogi)");
  Start.Game[10].Enabled = False;
  Start.Game[10].Value = False;
  Start.Game[0].Value = True;
  Start.MousePointer = 0;
}

void TwoKings ()
{
  INT K, L;
  int Response; // write-only?
  for(K = Rank - 1; K <= Rank + 1; K++) {
    for(L = File - 1; L <= File + 1; L++) {
	if(L > 0 && L <= BoardSizeX && K > 0 && K <= BoardSizeY ) {
	    if((K != Rank || L != File) ) {
		if(abs(Squares[L][K]) == 1 ) {
		    if(Squares[L][K] == 1 ) strcpy(Turn2, "White"); else strcpy(Turn2, "Black");
		    Response = MsgBox((sprintf(StringTmp, "You can't leave your %s in Check! ", Pieces[abs(Squares[File][Rank])].Name), StringTmp), 0, Turn2);
		    Checked = 2;
		}
	    }
	}
    } // Next L;
  } // Next K;
}

void UnPromote ()
{ // DblClick handler on piece. I is piece number
  INT K, L, Location, DoubleProm;
  if(strcmp(Computer, Turn) && strcmp(Computer, "Both") ) {
   DoubleProm = 0;
   if(AutoPromote == 1 && ForceProm != 1 ) {
    if((Score[TurnCount - 1].IDStart != Score[TurnCount - 1].IDEnd) && (Score[TurnCount - 1].IDEnd != 0) ) {
	for(K = 1; K <= BoardSizeY; K++) {
	    for(L = 1; L <= BoardSizeX; L++) {
		if(Grafix[L][K] == I ) {
		    Location = Score[TurnCount - 1].PosEnd;
		    Rank = Int(Location / (BoardSizeX + 1));
		    File = Location - (Rank * (BoardSizeX + 1));
		    if(L == File && K == Rank ) {
			DoubleProm = 1;
			Squares[L][K] = Score[TurnCount - 1].IDStart;
			Score[TurnCount - 1].IDEnd = Score[TurnCount - 1].IDStart;
			Graphnum = Pieces[abs(Squares[L][K])].Graphic;
			if((Squares[L][K] < 0 && Reverse == 0) || (Squares[L][K] > 0 && Reverse == 1) ) Graphnum = Graphnum + (TotGraph / 2);
/* [HGM] all from file.	if(!strcmp(Choice, "Tai") ) Board.showpic[I] = TaiPieces.Pix[Graphnum - 1]; else */ Board.showpic[I] = Board.Pix[Graphnum - 1];
			Score[TurnCount].Caption[strlen(Score[TurnCount].Caption) - 1] = '=';
			strcpy(ShortScore[TurnCount], Score[TurnCount].Caption);
			sprintf(Board.LastMove.Caption, "%d. %s", MoveCount, ShortScore[TurnCount]);
		    }
		}
	    } // Next L;
	} // Next K;
    }
   }
   if(DoubleProm == 0 ) LionIgui();
  }
}

void Validate ()
{
  char SpecPowerCHR;
  if(Squares[InitFile][InitRank] != 0 ) {
    if(!strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "Dolphin") ) DolphinMove();
    SpecPowerCHR = Pieces[abs(Squares[InitFile][InitRank])].special;
    if(SpecPowerCHR == 'F' ) Demon = 1; else Demon = 0;
    if(SpecPowerCHR != '0' && SpecPowerCHR != 'L' ) SpecialMove();
    if(SpecPowerCHR != 'L' && D != 0 && LionPiece == I && strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "Teaching King") ) {
	LionPower2();
    } else {
	if(LionPiece != I ) {
	    for(N = 1; N <= 8; N++) {
		switch(N) {
		    case 1: FileInc = 0; RankInc = -1; break;
		    case 2: FileInc = 0; RankInc = 1; break;
		    case 3: FileInc = -1; RankInc = 0; break;
		    case 4: FileInc = 1; RankInc = 0; break;
		    case 5: FileInc = -1; RankInc = -1; break;
		    case 6: FileInc = 1; RankInc = -1; break;
		    case 7: FileInc = -1; RankInc = 1; break;
		    case 8: FileInc = 1; RankInc = 1; break;
		}
		if(Squares[InitFile][InitRank] < 0 ) {
		    FileInc = 0 - FileInc;
		    RankInc = 0 - RankInc;
		}
		MoveData = Pieces[abs(Squares[InitFile][InitRank])].Moves[N];
		MoveTest = ((MoveData / 128.) - Int(MoveData / 128)) * 128;
		if(MoveTest > 0 ) SingleStep();
		MoveTest = ((MoveData / 256.) - Int(MoveData / 256)) * 256;
		if(MoveTest >= 128 ) Jumping();
		MoveTest = ((MoveData / 512.) - Int(MoveData / 512)) * 512;
		if(MoveTest >= 256 ) RangeJump();
		MoveTest = ((MoveData / 1024.) - Int(MoveData / 1024)) * 1024;
		if(MoveTest >= 512 ) LionPower();
		MoveTest = ((MoveData / 2048.) - Int(MoveData / 2048)) * 2048;
		if(MoveTest >= 1024 ) KnightJump();
		if(MoveData >= 2048 ) {
		    Hook = 1;
		    MoveTest = 64;
		    SingleStep();
		}
	    } // Next N;
	}
    }
    if(GameOver == 1 || Checked > 0 ) return;
    if(!strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "Dolphin") ) {
	Pieces[abs(Squares[InitFile][InitRank])].Moves[7] = 0;
	Pieces[abs(Squares[InitFile][InitRank])].Moves[8] = 0;
    }
    if(SpecPowerCHR == 'L' ) SpecialMove();
    if(!strcmp(Pieces[abs(Squares[InitFile][InitRank])].Name, "Teaching King") && TeachVer == 2 ) {
	if(LionPiece == I ) Lion(); else TeachingKing();
    }
  }
}

void WaDrop ()
{
  int Response;
  Response = MsgBox("Play Game with Drops?", 36, "Wa Shogi");
  if(Response == 6 ) {
    Drop = 1;
    strcpy(Boardbmp, "Waboard2.bmp");
  }
}

