// data types used in backend.c

#define False 0
#define True 1

typedef int INT;
typedef char STRING[80];
typedef float SINGLE;
typedef long int LONG;

typedef struct { 
    INT number;
    STRING Name;
    char sname[5]; // As String * 4;
    INT Value;
    INT PrValue;
    INT Promotes;
    INT Graphic;
    INT PrGraphic;
    INT Moves[9];
    char special; // As String * 1;
    INT Mask;
    INT Range;
} Piece;

// form???

typedef struct {
  STRING Filename;
  STRING DialogTitle;
  int Flags;
  int Action;
} DIALOG;

typedef struct {
  STRING Caption;
  int Visible;
  int Enabled;
  int Checked;
  int dragged;
  int Interval;
  void *Picture;
  int Value;
  STRING List[10];
  int ListIndex;
  int ForeColor;
  int x, y, width, height;
  int oldX, oldY, oldWidth, oldHeight, oldVisible;
  void (*Move)(int draw, int x, int y), (*Drag)(int mode);
} OBJECT, MENU;

typedef struct {
  STRING Caption;

  OBJECT Frame;
  OBJECT Held[NTYPES];
  OBJECT Timer1;
  OBJECT Timer2;
  OBJECT showpic[NSQUARES];
  OBJECT HandPic[NTYPES];
  OBJECT WhiteClock;
  OBJECT BlackClock;
  OBJECT LastMove;
  OBJECT PieceID;
  OBJECT NextMove;
  OBJECT NotTop, NotSide;
  OBJECT Pix[NPIECES];
  OBJECT White, Black;
  OBJECT Game[10];
  OBJECT Title[18];
  OBJECT CmdPiece;

  int FillColor;
  int FillStyle;
  int ForeColor;
  int MousePointer;
  int ScaleWidth, DrawWidth;
  int CurrentX, CurrentY;
  int AutoRedraw;
  void *Picture;
  int WindowState;
  int Top, Left, Height;

  MENU MnuClockOn;
  MENU MnuClockOff;
  MENU MnuSwitch;
  MENU MnuVer1, MnuVer2;
  MENU MnuShowOn, MnuShowOff;
  MENU MnuShowLastOn, MnuShowLastOff;
  MENU MnuThreatOn, MnuThreatOff;
  MENU MnuAutoOn, MnuAutoOff;
  MENU MnuEvalOn, MnuEvalOff;
  MENU MnuLVer1, MnuLVer2;
  MENU MnuNotOn, MnuNotOff;
  MENU MnuWhitePlayer, MnuBlackPlayer;
  MENU MnuWhiteComp, MnuBlackComp;
  MENU MnuWhite, MnuBlack;
  MENU MnuHandicap;
  MENU MnuNextWhite, MnuNextBlack;
  MENU MnuWeak, MnuBest, MnuLessWeak;

  DIALOG CMDiagram;
  DIALOG CMSave;

  void (*Refresh)(), (*Hide)(), (*Show)();
  void (*Line)(int x1, int y1, int x2, int y2, int color, char *options);
  void (*Circle)(int x, int y, int r);
  void (*Print)(char *c); // prints the passed character
} FORM;

typedef struct {
  STRING Caption;
  OBJECT NewPiece;
  int Visible;
  int Enabled;
} ADDPIECES;

// backend.c functions called from menus

void ConfigSave();
void SetPieces();
void EndSetup();
void SetHandicap();
void SeeMoves();
void SetThreat();
void SetEval();
void CompMain();
void SetWhitePlayer();
void SetBlackPlayer();
void Compute();
void SetWhitePlayer();
void Notation();
void SetAutoPromote();
void ClocksOn();
void ClocksOff();
void SetLionHawk();
void LoadGame();
void SaveGame();
void ChangeGame();
void TakeBack();
void TakeAll();
void Replay();
void ReplayAll();
void SetSuggest();
void PrintScore();
void DiagramFile();
void DiagramSmall();
void Rotate();
void SwitchCompPlayer();
void SetGeneral();
void SetRules();
void SetDifficulty();
void SetLastMove();
void Clock();
void ClearBoard();
void CheckAdd();
void MovePieces();
void AddSomePieces();
void StartUp();
void ResetHand();
void NotSet();
void PicDrop();
void ShowProm();
void DropPiece();
void DropPiece2();
void ClearLegal();
void HeldDown();
void HeldProm();
void FillSquare();
void FormDrop();
void EndTilde();
void PicDown();
void Main();

// globals in backend.c altered by menus

extern STRING Direct;
extern STRING Threat;
extern STRING Cap;
extern STRING Computer;
extern STRING Turn;
extern STRING Grade;
extern STRING Choice;
extern STRING ExtraPiece;

extern INT Reverse;
extern INT Timing;
extern INT EndMove;
extern INT NewGame;
extern INT ShowLast;
extern INT SeeMove;
extern INT Eval;
extern INT Notate;
extern INT AutoPromote;
extern INT LionHawkVer;
extern INT LegalMoves;
extern INT CCC, I;
extern SINGLE NewX, NewY;
extern INT NewButton;
extern INT NewIndex;
extern INT ClickPiece;
extern INT PieceNum;
extern INT Drop;
extern INT Selection;
extern INT Reduce;
extern INT MovePiece;
extern INT MoveCount;
extern INT TurnCount;
extern INT PromDotY;
extern INT XStart;
extern INT Pixels;
extern INT BoardSizeX, BoardSizeY;

extern Piece Pieces[];

extern FORM Board;
extern FORM PieceHelp;

