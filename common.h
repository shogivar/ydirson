#define MSG_SIZ 512
#define NPIECES  1000 /* FIXME: how many, really? */
#define NSQUARES 1000 /* FIXME: how many, really? */
#define NTYPES    100 /* FIXME: how many, really? */


#define ASSIGN(X, Y) if(X) free(X); X = strdup(Y)

typedef int Boolean;
typedef char *String;
typedef void (*DelayedEventCallback)(void);
typedef int (*FileProc)(FILE *f, int n, char *title);

typedef enum { Press, Release } ClickType;

typedef enum { CheckBox, ComboBox, TextBox, Button, Spin, ResetButton, SaveButton, ListBox, Graph, PopUp,
		 FileName, PathName, Slider, Message, Fractional, Label, Icon,
		 BoxBegin, BoxEnd, BarBegin, BarEnd, DropDown, Break, EndMark, Skip } Control;

typedef struct _opt {   // [HGM] options: descriptor of UCI-style option
    int value;          // current setting, starts as default
    int min;		// Also used for flags
    int max;
    void *handle;       // for use by front end
    void *target;       // for use by front end
    char *textValue;    // points to beginning of text value in name field
    char **choice;      // points to array of combo choices in cps->combo
    Control type;
    char *name;         // holds both option name and text value (in allocated memory)
} Option;

typedef struct {
    Boolean visible;
    int x;
    int y;
    int width;
    int height;
} WindowPlacement;

/* A point in time */
typedef struct {
    long sec;  /* Assuming this is >= 32 bits */
    int ms;    /* Assuming this is >= 16 bits */
} TimeMark;

extern TimeMark programStartTime;
