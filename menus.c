/*
 * menus.c -- platform-indendent menu handling code for ShogiVar (adapted from XBoard)
 *
 * Copyright 1991 by Digital Equipment Corporation, Maynard,
 * Massachusetts.
 *
 * Enhancements Copyright 1992-2001, 2002, 2003, 2004, 2005, 2006,
 * 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014 Free Software Foundation, Inc.
 *
 * The following terms apply to Digital Equipment Corporation's copyright
 * interest in XBoard:
 * ------------------------------------------------------------------------
 * All Rights Reserved
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose and without fee is hereby granted,
 * provided that the above copyright notice appear in all copies and that
 * both that copyright notice and this permission notice appear in
 * supporting documentation, and that the name of Digital not be
 * used in advertising or publicity pertaining to distribution of the
 * software without specific, written prior permission.
 *
 * DIGITAL DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * DIGITAL BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 * ------------------------------------------------------------------------
 *
 * The following terms apply to the enhanced version of XBoard
 * distributed by the Free Software Foundation:
 * ------------------------------------------------------------------------
 *
 * GNU XBoard is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * GNU XBoard is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.  *
 *
 *------------------------------------------------------------------------
 ** See the file ChangeLog for a revision history.  */

#define HIGHDRAG 1

#include "config.h"

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>
#include <math.h>

#if STDC_HEADERS
# include <stdlib.h>
# include <string.h>
#else /* not STDC_HEADERS */
extern char *getenv();
# if HAVE_STRING_H
#  include <string.h>
# else /* not HAVE_STRING_H */
#  include <strings.h>
# endif /* not HAVE_STRING_H */
#endif /* not STDC_HEADERS */

#if HAVE_UNISTD_H
# include <unistd.h>
#endif

#if ENABLE_NLS
#include <locale.h>
#endif

// [HGM] bitmaps: put before incuding the bitmaps / pixmaps, to know how many piece types there are.

#include "common.h"
#include "backend.h"
#include "frontend.h"
#include "menus.h"
#include "dialogs.h"
#include "gettext.h"

#ifdef ENABLE_NLS
# define  _(s) gettext (s)
# define N_(s) gettext_noop (s)
#else
# define  _(s) (s)
# define N_(s)  s
#endif

/*
 * Button/menu procedures
 */


int shiftKey, controlKey;
char  *gameCopyFilename, *gamePasteFilename;

void
DoChange ()
{
    Destroy(BoardDlg);
    Main();
}

void
ChangeGameProc ()
{
    Board.Timer1.Enabled = False;
    ChangeGame();
    ScheduleDelayedEvent(&DoChange, 500);
}

void
SaveGameProc ()
{
    if(FileNamePopUpWrapper("Save Game", "", "*.gam", NULL, 0, "w", Board.CMSave.Filename, NULL))
	SaveGame();
}

void
DiagramProc ()
{
    if(FileNamePopUpWrapper("Create Text Diagram", "", "*.txt", NULL, 0, "w", Board.CMDiagram.Filename, NULL))
	DiagramFile();
}

void
SmallProc ()
{
    if(FileNamePopUpWrapper("Create Small Text Diagram", "", "*.txt", NULL, 0, "w", Board.CMDiagram.Filename, NULL))
	DiagramSmall();
}

void
LoadGameProc ()
{
    if(FileNamePopUpWrapper("Load Game", "", "*.gam", NULL, 0, "r", Board.CMSave.Filename, NULL))
	LoadGame();
    DrawBoard();
}

void
QuitProc ()
{
//    Unload Start;
    ConfigSave();
    exit(0);
}

void
ClearProc ()
{
    EndSetup();
    ClearBoard();
    DrawBoard();
}

void
RotateProc ()
{
    Rotate();
    DrawBoard();
}

void
AddBlackProc ()
{
    strcpy(ExtraPiece, "Black"); CheckAdd();
}

void
AddWhiteProc ()
{
    strcpy(ExtraPiece, "White"); CheckAdd();
}

void
RemoveProc ()
{
    EndSetup();
    Reduce = 1; SetHandicap();
}

void
TurnProc ()
{
    if(strcmp(Turn, "White") ) strcpy(Turn, "White"); else strcpy(Turn, "Black");
    MarkMenuItem("Setup.Turn", !strcpy(Turn, "White"));
}

void
MovePieceProc ()
{
    EndSetup();
    MovePieces();
}

void
ForceProc ()
{
    EndMove = 1;
}

void
NewGameProc ()
{
    if(LegalMoves == 0) {
	NewGame = 1;
	SetPieces();
    }
    DrawBoard();
}

void
PieceHelpProc ()
{
//    PieceHelp.Show();
    LoadPieceHelp();
}

void
HandicapProc ()
{
    EndSetup(); SetHandicap();
}

void
NothingProc ()
{
    return;
}

#   define MARK_MENU_ITEM(X,Y) MarkMenuItem(X, Y)

void
ShowLegalProc ()
{
  SeeMove = !SeeMove;
  SeeMoves();
  MARK_MENU_ITEM("Moves.ShowLegal", SeeMove);
}

void
ShowThreatsProc ()
{
    strcpy(Threat, strcmp(Threat, "On") ? "On" : "Off");
    SetThreat();
    MARK_MENU_ITEM("Moves.ShowTheat", !strcmp(Threat, "On"));
}

void
EvaluateProc ()
{
    Eval = !Eval;
    if (!Eval) strcpy(Board.Caption, Cap);
    strcpy(Board.PieceID.Caption, Eval ? "Evaluation On" : "Evaluation Off");
    SetEval();
    MARK_MENU_ITEM("Moves.Evaluate", Eval);
}

void
CompWhiteProc ()
{
    int w; // white is a computer player
    if(!strcmp(Computer, "White")) w = 0, strcpy( Computer, "None");  else
    if(!strcmp(Computer, "Both"))  w = 0, strcpy( Computer, "Black"); else
    if(!strcmp(Computer, "Black")) w = 1, strcpy( Computer, "Both");  else
    if(!strcmp(Computer, "None"))  w = 1, strcpy( Computer, "White");
    SetWhitePlayer();
    MARK_MENU_ITEM("Setup.CompWhite", w);
    if (w && !strcmp(Turn, "White")) CompMain();
}

void
CompBlackProc ()
{
    int b; // black is a computer player
    if(!strcmp(Computer, "Black")) b = 0, strcpy( Computer, "None");  else
    if(!strcmp(Computer, "Both"))  b = 0, strcpy( Computer, "White"); else
    if(!strcmp(Computer, "White")) b = 1, strcpy( Computer, "Both");  else
    if(!strcmp(Computer, "None"))  b = 1, strcpy( Computer, "Black");
    SetBlackPlayer();
    MARK_MENU_ITEM("Setup.CompBlack", b);
    if (b && !strcmp(Turn, "Black")) CompMain();
}

void
LevelProc ()
{   // Toggle between Weak and Less Weak
    strcpy(Grade, strcmp(Grade, "LessWeak") ? "LessWeak": "Weak");
    SetDifficulty();
    MARK_MENU_ITEM("Setup.LessWeak", !strcmp(Grade, "LessWeak"));
}

void
NotateProc ()
{
    Notate = !Notate;
    Notation();
    DrawBoard();
    MARK_MENU_ITEM("Setup.Notate", !Notate);
}

void
FlashProc ()
{
    ShowLast = !ShowLast;
    SetLastMove();
    MARK_MENU_ITEM("Moves.Flash", ShowLast);
}

void
AutoPromProc ()
{
    AutoPromote = !AutoPromote;
    SetAutoPromote();
    MARK_MENU_ITEM("Setup.AutoProm", AutoPromote);
}

void
ClocksProc ()
{
    Timing = !Timing;
    if(Timing) ClocksOff(); else ClocksOn();
    MARK_MENU_ITEM("Setup.Clocks", !Timing);
    if(Board.Timer1.Enabled) StartClockTimer(1000);
}


void
PiecesProc ()
{
    static int western;
    western = !western;
    MARK_MENU_ITEM("Setup.Japanese", !western);
}

void
LionHawkProc ()
{
    int v; char c;
    if(LionHawkVer == 2) LionHawkVer = 1, v = 42, c = 'L';
    else                 LionHawkVer = 2, v = 26, c = 'D';
    Pieces[47].Value   = Pieces[64].Value   = Pieces[18].PrValue = v;
    Pieces[47].special = Pieces[64].special = c;
    SetLionHawk();
    MARK_MENU_ITEM("Setup.LionHawk", LionHawkVer == 2);
}

/*
 *  Menu definition tables
 */

MenuItem gameMenu[] = {
  {N_("New Game"),             "<Ctrl>n",          "NewGame",    NewGameProc },
  {N_("Load Game"),            "<Ctrl>o",          "LoadGame",   LoadGameProc },
  {N_("Save Game"),            "<Ctrl>s",          "SaveGame",   SaveGameProc },
  {N_("Change Game"),          "<Ctrl>s",          "ChangeGame", ChangeGameProc },
  {"----",                      NULL,               NULL,        NothingProc },
  {N_("Quit "),                "<Ctrl>q",          "Quit",       QuitProc },
  {NULL,                        NULL,               NULL,        NULL}
};

MenuItem movesMenu[] = {
  {N_("Show Legal Moves"),       NULL,           "ShowLegal",   ShowLegalProc,   CHECK },
  {N_("Show Influence/Threats"), "<ctrl>i",      "ShowThreat",  ShowThreatsProc, CHECK },
  {N_("Flash Last Move"),        NULL,           "Flash",       FlashProc,       CHECK },
  {"----",                       NULL,           NULL,          NothingProc },
  {N_("Force Move"),             "<ctrl>f",      "ForceMove",   ForceProc },
  {"----",                       NULL,           NULL,          NothingProc },
  {N_("Take Back"),              "<ctrl>b",      "TakeBack",    TakeBack },
  {N_("Take Back All"),          "<shift>Delete", "BackAll",     TakeAll },
  {"----",                       NULL,           NULL,          NothingProc},
  {N_("Replay"),                 "<ctrl>r",      "Replay",      Replay },
  {N_("Replay All"),             "<shift>Insert", "RepAll",      ReplayAll },
  {"----",                       NULL,           NULL,          NothingProc },
  {N_("Suggest Move"),           "<ctrl>m",      "Suggest",     SetSuggest },
  {"----",                       NULL,           NULL,          NothingProc},
  {N_("Evaluate"),               "<ctrl>e",      "Evaluate",    EvaluateProc,    CHECK },
  {"----",                       NULL,           NULL,          NothingProc },
  {N_("Print Moves"),            "<ctrl>p",      "PrintMoves",  PrintScore },
  {"----",                       NULL,           NULL,          NothingProc },
  {N_("Create Text Score"),      "<ctrl>t",      "CreateLarge", DiagramProc },
  {N_("Create Text Diagram"),    "<ctrl>y" ,     "CreateSmall", SmallProc },
  {NULL,                  NULL,             NULL,           NULL}
};

MenuItem setupMenu[] = {
  {N_("Computer White"),        NULL,     "CompWhite",   CompWhiteProc, CHECK },
  {N_("Computer Black"),        NULL,     "CompBlack",   CompBlackProc, CHECK },
  {"----",                      NULL,      NULL,         NothingProc },
  {N_("Rotate Board"),         "<ctrl>w", "Rotate",      RotateProc },
  {"----",                      NULL,      NULL,         NothingProc },
  {N_("Switch Sides"),         "<ctrl>x", "SwitchSides", SwitchCompPlayer },
  {"----",                      NULL,      NULL,         NothingProc },
  {N_("Difficulty Less Weak"), "F6",      "LessWeak",    LevelProc,     CHECK },
  {"----",                      NULL,      NULL,         NothingProc },
  {N_("Notation"),              NULL,     "Notation",    NotateProc,    CHECK },
  {N_("Clocks"),                NULL,     "Clocks",      ClocksProc,    CHECK },
  {N_("Auto Promotion"),       "<ctrl>a", "AutoProm",    AutoPromProc,  CHECK },
  {"----",                      NULL,      NULL,         NothingProc },
  {N_("Clear Board"),           NULL,     "Clear",       ClearProc },
  {N_("Add Black Pieces"),      NULL,     "AddBlack",    AddBlackProc },
  {N_("Add White Pieces"),      NULL,     "AddWhite",    AddWhiteProc },
  {N_("Remove Pieces"),         NULL,     "Remove",      RemoveProc },
  {N_("Move Pieces"),           NULL,     "Setup",       MovePieceProc },
  {N_("White to Move"),         NULL,     "Turn",        TurnProc,      CHECK },
  {N_("Set Handicap"),         "<ctrl>h", "Handicap",    HandicapProc },
  {"----",                      NULL,      NULL,         NothingProc },
  {N_("Japanese Pieces"),       NULL,     "Japanese",    PiecesProc,    CHECK },
  {"----",                      NULL,      NULL,         NothingProc },
  {N_("Lion Hawk Version 2"),   NULL,     "LionHawk",    LionHawkProc,  CHECK },
  {NULL, NULL, NULL, NULL}
};

MenuItem helpMenu[] = {
  {N_("About"),	     "F1", "About",	 AboutProc },
  {"----",           NULL,  NULL,	 NothingProc},
  {N_("General"),    "F2", "General",	 SetGeneral },
  {N_("Rules"),	     "F3", "Rules",	 SetRules },
  {N_("Piece Help"), "F4", "PieceHelp",	 PieceHelpProc},
  {NULL, NULL, NULL, NULL}
};

MenuItem noMenu[] = {
  {NULL, NULL, NULL, NULL}
};

Menu menuBar[] = {
    {N_("Game"),    "Game",  gameMenu},
    {N_("Moves"),   "Moves", movesMenu},
    {N_("Set-Up"),  "Setup", setupMenu},
    {N_("Help"),    "Help",  helpMenu},
    {NULL, NULL, NULL}
};

MenuItem *
MenuNameToItem (char *menuName)
{
    int i=0;
    char buf[512], *p;
    MenuItem *menuTab;
    static MenuItem a = { NULL, NULL, NULL, NothingProc };
    extern Option mainOptions[];
    strncpy(buf, menuName, 511);
    p = strchr(buf, '.');
    if(!p) menuTab = noMenu, p = menuName; else {
	*p++ = '\0';
	for(i=0; menuBar[i].name; i++)
	    if(!strcmp(buf, menuBar[i].ref)) break;
	if(!menuBar[i].name) return NULL; // main menu not found
	menuTab = menuBar[i].mi;
    }
    if(*p == '\0') { a.handle = mainOptions[i+1].handle; return &a; } // main menu bar
    for(i=0; menuTab[i].string; i++)
	if(menuTab[i].ref && !strcmp(p, menuTab[i].ref)) return menuTab + i;
    return NULL; // item not found
}

Enables icsEnables[] = {
    { "File.MailMove", False },
    { NULL, False }
};

void
GreyRevert (Boolean grey)
{
    EnableNamedMenuItem("Edit.Revert", !grey);
    EnableNamedMenuItem("Edit.Annotate", !grey);
}

void
InitMenuMarkers()
{
    MarkMenuItem("Setup.Japanese", True);
    MarkMenuItem("Setup.Clocks", !Timing);
    MarkMenuItem("Setup.Notation", !Notate);
    MarkMenuItem("Moves.Flash", ShowLast);
    MarkMenuItem("Moves.ShowLegal", SeeMove);
    MarkMenuItem("Moves.Evaluate", Eval);
    MarkMenuItem("Moves.ShowThreat", !strcmp(Threat, "On"));
    MarkMenuItem("Setup.LessWeak", !strcmp(Grade, "LessWeak"));
    MarkMenuItem("Setup.CompWhite", !strcmp(Computer, "White") ||  !strcmp(Computer, "Both"));
    MarkMenuItem("Setup.CompBlack", !strcmp(Computer, "Black") ||  !strcmp(Computer, "Both"));
    if(strcmp(Choice, "Tenjiku")) {
	EnableNamedMenuItem("Setup.LionHawk", False);
    } else {
	EnableNamedMenuItem("Setup.LionHawk", True);
	MarkMenuItem("Setup.LionHawk", LionHawkVer == 2);
    }
}
