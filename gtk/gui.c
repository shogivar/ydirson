/*
 * xboard.c -- universal GTK front-end
 *
 * Copyright 1991 by Digital Equipment Corporation, Maynard,
 * Massachusetts.
 *
 * Enhancements Copyright 1992-2001, 2002, 2003, 2004, 2005, 2006,
 * 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014 Free Software Foundation, Inc.
 *
 * The following terms apply to Digital Equipment Corporation's copyright
 * interest in XBoard:
 * ------------------------------------------------------------------------
 * All Rights Reserved
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose and without fee is hereby granted,
 * provided that the above copyright notice appear in all copies and that
 * both that copyright notice and this permission notice appear in
 * supporting documentation, and that the name of Digital not be
 * used in advertising or publicity pertaining to distribution of the
 * software without specific, written prior permission.
 *
 * DIGITAL DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * DIGITAL BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 * ------------------------------------------------------------------------
 *
 * The following terms apply to the enhanced version of XBoard
 * distributed by the Free Software Foundation:
 * ------------------------------------------------------------------------
 *
 * GNU XBoard is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * GNU XBoard is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.  *
 *
 *------------------------------------------------------------------------
 ** See the file ChangeLog for a revision history.  */

#define HIGHDRAG 1

#include "config.h"

#include <stdio.h>
#include <ctype.h>
#include <signal.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>
#include <math.h>
#include <cairo/cairo.h>
#include <cairo/cairo-xlib.h>
#include <gtk/gtk.h>

#if !OMIT_SOCKETS
# if HAVE_SYS_SOCKET_H
#  include <sys/socket.h>
#  include <netinet/in.h>
#  include <netdb.h>
# else /* not HAVE_SYS_SOCKET_H */
#  if HAVE_LAN_SOCKET_H
#   include <lan/socket.h>
#   include <lan/in.h>
#   include <lan/netdb.h>
#  else /* not HAVE_LAN_SOCKET_H */
#   define OMIT_SOCKETS 1
#  endif /* not HAVE_LAN_SOCKET_H */
# endif /* not HAVE_SYS_SOCKET_H */
#endif /* !OMIT_SOCKETS */

#if STDC_HEADERS
# include <stdlib.h>
# include <string.h>
#else /* not STDC_HEADERS */
extern char *getenv();
# if HAVE_STRING_H
#  include <string.h>
# else /* not HAVE_STRING_H */
#  include <strings.h>
# endif /* not HAVE_STRING_H */
#endif /* not STDC_HEADERS */

#if HAVE_SYS_FCNTL_H
# include <sys/fcntl.h>
#else /* not HAVE_SYS_FCNTL_H */
# if HAVE_FCNTL_H
#  include <fcntl.h>
# endif /* HAVE_FCNTL_H */
#endif /* not HAVE_SYS_FCNTL_H */

#if HAVE_SYS_SYSTEMINFO_H
# include <sys/systeminfo.h>
#endif /* HAVE_SYS_SYSTEMINFO_H */

#if TIME_WITH_SYS_TIME
# include <sys/time.h>
# include <time.h>
#else
# if HAVE_SYS_TIME_H
#  include <sys/time.h>
# else
#  include <time.h>
# endif
#endif

#if HAVE_UNISTD_H
# include <unistd.h>
#endif

#if HAVE_SYS_WAIT_H
# include <sys/wait.h>
#endif

#if HAVE_DIRENT_H
# include <dirent.h>
# define NAMLEN(dirent) strlen((dirent)->d_name)
# define HAVE_DIR_STRUCT
#else
# define dirent direct
# define NAMLEN(dirent) (dirent)->d_namlen
# if HAVE_SYS_NDIR_H
#  include <sys/ndir.h>
#  define HAVE_DIR_STRUCT
# endif
# if HAVE_SYS_DIR_H
#  include <sys/dir.h>
#  define HAVE_DIR_STRUCT
# endif
# if HAVE_NDIR_H
#  include <ndir.h>
#  define HAVE_DIR_STRUCT
# endif
#endif

#if ENABLE_NLS
#include <locale.h>
#endif

// [HGM] bitmaps: put before incuding the bitmaps / pixmaps, to know how many piece types there are.
#include "common.h"
#include "backend.h"
#include "frontend.h"
#include "gui.h"
#include "menus.h"
#include "dialogs.h"
#include "gettext.h"
#include "draw.h"

#ifdef __APPLE__
#  include <gtkmacintegration/gtkosxapplication.h>
   // prevent pathname of positional file argument provided by OS X being be mistaken for option name
   // (price is that we won't recognize Windows option format anymore).
#  define SLASH '-'
   // redefine some defaults
#  undef ICS_LOGON
#  undef DATADIR
#  undef SETTINGS_FILE
#  define ICS_LOGON "Library/Preferences/XboardICS.conf"
#  define DATADIR dataDir
#  define SETTINGS_FILE masterSettings
   char dataDir[MSG_SIZ]; // for expanding ~~
   char masterSettings[MSG_SIZ];
#else
#  define SLASH '/'
#endif

#ifdef __EMX__
#ifndef HAVE_USLEEP
#define HAVE_USLEEP
#endif
#define usleep(t)   _sleep2(((t)+500)/1000)
#endif

#ifdef ENABLE_NLS
# define  _(s) gettext (s)
# define N_(s) gettext_noop (s)
#else
# define  _(s) (s)
# define N_(s)  s
#endif

int main(int argc, char **argv);
RETSIGTYPE CmailSigHandler(int sig);
RETSIGTYPE IntSigHandler(int sig);
RETSIGTYPE TermSizeSigHandler(int sig);
char *InsertPxlSize(char *pattern, int targetPxlSize);
#if ENABLE_NLS
XFontSet CreateFontSet(char *base_fnt_lst);
#else
char *FindFont(char *pattern, int targetPxlSize);
#endif
void DelayedDrag(void);
void ICSInputBoxPopUp(void);
void MoveTypeInProc(GdkEventKey *eventkey);
gboolean KeyPressProc(GtkWindow *window, GdkEventKey *eventkey, gpointer data);
Boolean TempBackwardActive = False;
void DisplayMove(int moveNumber);
void update_ics_width();
int CopyMemoProc();
static gboolean EventProc(GtkWidget *widget, GdkEvent *event, gpointer g);

#ifdef TODO_GTK
#if ENABLE_NLS
XFontSet fontSet, clockFontSet;
#else
Font clockFontID;
XFontStruct *clockFontStruct;
#endif
Font coordFontID, countFontID;
XFontStruct *coordFontStruct, *countFontStruct;
#else
void *shellWidget, *formWidget, *boardWidget, *titleWidget, *dropMenu, *menuBarWidget;
GtkWidget       *mainwindow;
#endif
Option *optList; // contains all widgets of main window
char *layoutName;
char *clockFont = "Sans Bold 9";

char installDir[] = "."; // [HGM] UCI: needed for UCI; probably needs run-time initializtion

/* pixbufs */
static GdkPixbuf       *mainwindowIcon=NULL;
static GdkPixbuf       *WhiteIcon=NULL;
static GdkPixbuf       *BlackIcon=NULL;

/* key board accelerators */
GtkAccelGroup *GtkAccelerators;

typedef unsigned int BoardSize;
BoardSize boardSize;
Boolean chessProgram;

int  minX, minY; // [HGM] placement: volatile limits on upper-left corner
int smallLayout = 0, tinyLayout = 0,
  marginW, marginH, // [HGM] for run-time resizing
  fromX = -1, fromY = -1, toX, toY, commentUp = False,
  errorExitStatus = -1, defaultLineGap;

void *currBoard;
char *chessDir, *programName, *programVersion;
Boolean alwaysOnTop = False;

TimeMark programStartTime;

WindowPlacement wpMain;

/* This magic number is the number of intermediate frames used
   in each half of the animation. For short moves it's reduced
   by 1. The total number of frames will be factor * 2 + 1.  */
#define kFactor	   4

void
BoardToTop ()
{
  gtk_window_present(GTK_WINDOW(shells[RootWindow]));
}

//---------------------------------------------------------------------------------------------------------
// some symbol definitions to provide the proper (= XBoard) context for the code in args.h
#define CW_USEDEFAULT (1<<31)
#define ICS_TEXT_MENU_SIZE 90

// front-end part of option handling

int frameX, frameY;

void
GetActualPlacement (GtkWidget *shell, WindowPlacement *wp)
{
  GtkAllocation a;
  if(!shell) return;
  gtk_widget_get_allocation(shell, &a);
  gtk_window_get_position(GTK_WINDOW(shell), &a.x, &a.y);
  wp->x = a.x;
  wp->y = a.y;
  wp->width = a.width;
  wp->height = a.height;
//printf("placement: (%d,%d) %dx%d\n", a.x, a.y, a.width, a.height);
  frameX = 3; frameY = 3; // remember to decide if windows touch
}

void
GetPlacement (DialogClass dlg, WindowPlacement *wp)
{ // wrapper to shield back-end from widget type
  if(shellUp[dlg]) GetActualPlacement(shells[dlg], wp);
}

void
GetWindowCoords ()
{ // wrapper to shield use of window handles from back-end (make addressible by number?)
  // In XBoard this will have to wait until awareness of window parameters is implemented
  GetActualPlacement(shellWidget, &wpMain);
}

void
EnsureOnScreen (int *x, int *y, int minX, int minY)
{
  return;
}

int
MainWindowUp ()
{ // [HGM] args: allows testing if main window is realized from back-end
  return DialogExists(RootWindow);
}

int clockKludge;

void
ResizeBoardWindow (int w, int h, int inhibit)
{
    GtkAllocation a;
//    if(clockKludge) return; // ignore as long as clock does not have final height
    gtk_widget_get_allocation(optList[W_WHITE].handle, &a);
    w += marginW + 1; // [HGM] not sure why the +1 is (sometimes) needed...
    h += marginH + a.height + 1;
    gtk_window_resize(GTK_WINDOW(shellWidget), w, h);
}

void
SlaveResize (Option *opt)
{
    static int slaveW, slaveH, w, h;
    GtkAllocation a;
    if(!slaveH) {
	gtk_widget_get_allocation(shells[DummyDlg], &a);
	w = a.width; h = a.height;
	gtk_widget_get_allocation(opt->handle, &a);
	slaveW =  w - opt->max; // [HGM] needed to set new shellWidget size when we resize board
	slaveH =  h - a.height + 13;
   }
  gtk_window_resize(GTK_WINDOW(shells[DummyDlg]), slaveW + opt->max, slaveH + opt->value);
}

#ifdef __APPLE__
static char clickedFile[MSG_SIZ];
static int suppress;

static gboolean
StartNewXBoard(GtkosxApplication *app, gchar *path, gpointer user_data)
{ // handler of OSX OpenFile signal, which sends us the filename of clicked file or first argument
  if(suppress) { // we just started XBoard without arguments
    strncpy(clickedFile, path, MSG_SIZ); // remember file name, but otherwise ignore
  } else {       // we are running something presumably useful
    char buf[MSG_SIZ];
    snprintf(buf, MSG_SIZ, "open -n -a \"xboard\" --args \"%s\"", path);
    system(buf); // start new instance on this file
  }
  return TRUE;
}
#endif

/* Get the current time as a TimeMark */
void
GetTimeMark (TimeMark *tm)
{
#if HAVE_GETTIMEOFDAY

    struct timeval timeVal;
    struct timezone timeZone;

    gettimeofday(&timeVal, &timeZone);
    tm->sec = (long) timeVal.tv_sec;
    tm->ms = (int) (timeVal.tv_usec / 1000L);

#else /*!HAVE_GETTIMEOFDAY*/
#if HAVE_FTIME

// include <sys/timeb.h> / moved to just above start of function
    struct timeb timeB;

    ftime(&timeB);
    tm->sec = (long) timeB.time;
    tm->ms = (int) timeB.millitm;

#else /*!HAVE_FTIME && !HAVE_GETTIMEOFDAY*/
    tm->sec = (long) time(NULL);
    tm->ms = 0;
#endif
#endif
}

/* Return the difference in milliseconds between two
   time marks.  We assume the difference will fit in a long!
*/
long
SubtractTimeMarks (TimeMark *tm2, TimeMark *tm1)
{
    return 1000L*(tm2->sec - tm1->sec) +
           (long) (tm2->ms - tm1->ms);
}

int
main (int argc, char **argv)
{
    int i;
//    int boardWidth, w, h; //, boardHeight;
    char *p;

    srandom(time(0)); // [HGM] book: make random truly random
    GetTimeMark(&programStartTime);

    setbuf(stdout, NULL);
    setbuf(stderr, NULL);

#if 0
    if(argc > 1 && (!strcmp(argv[1], "-v" ) || !strcmp(argv[1], "--version" ))) {
        printf("%s version %s\n\n  configure options: %s\n", PACKAGE_NAME, PACKAGE_VERSION, CONFIGURE_OPTIONS);
	exit(0);
    }
#endif

    /* set up GTK */
    gtk_init (&argc, &argv);
#ifdef __APPLE__
    {   // prepare to catch OX OpenFile signal, which will tell us the clicked file
	GtkosxApplication *theApp = g_object_new(GTKOSX_TYPE_APPLICATION, NULL);
	char *path = gtkosx_application_get_bundle_path();
	strncpy(dataDir, path, MSG_SIZ);
	snprintf(masterSettings, MSG_SIZ, "%s/Contents/Resources/etc/xboard.conf", path);
	g_signal_connect(theApp, "NSApplicationOpenFile", G_CALLBACK(StartNewXBoard), NULL);
	// we must call application ready before we can get the signal,
	// and supply a (dummy) menu bar before that, to avoid problems with dual apples in it
	gtkosx_application_set_menu_bar(theApp, GTK_MENU_SHELL(gtk_menu_bar_new()));
	gtkosx_application_ready(theApp);
	suppress = (argc == 1 || argc > 1 && argv[1][00] != '-'); // OSX sends signal even if name was already argv[1]!
	if(argc == 1) {                  // called without args: OSX open-file signal might follow
	    static char *fakeArgv[3] = {NULL, clickedFile, NULL};
	    usleep(10000);               // wait 10 msec (and hope this is long enough).
	    while(gtk_events_pending())
		gtk_main_iteration();    // process all events that came in upto now
	    suppress = 0;                // future open-file signals should start new instance
	    if(clickedFile[0]) {         // we were sent an open-file signal with filename!
	      fakeArgv[0] = argv[0];
	      argc = 2; argv = fakeArgv; // fake that we were called as "xboard filename"
	    }
	}
    }
#endif

    /* set up keyboard accelerators group */
    GtkAccelerators = gtk_accel_group_new();

    programName = strrchr(argv[0], '/');
    if (programName == NULL)
      programName = argv[0];
    else
      programName++;

#ifdef ENABLE_NLS
    bindtextdomain(PACKAGE, LOCALEDIR);
    bind_textdomain_codeset(PACKAGE, "UTF-8"); // needed when creating markup for the clocks
    textdomain(PACKAGE);
#endif

    p = getenv("HOME");
    if (p == NULL) p = "/tmp";
    i = strlen(p) + strlen("/.xboardXXXXXx.pgn") + 1;
    gameCopyFilename = (char*) malloc(i);
    gamePasteFilename = (char*) malloc(i);
    snprintf(gameCopyFilename,i, "%s/.xboard%05uc.pgn", p, getpid());
    snprintf(gamePasteFilename,i, "%s/.xboard%05up.pgn", p, getpid());

#if ENABLE_NLS
    if (appData.debugMode) {
      fprintf(debugFP, "locale = %s\n", setlocale(LC_ALL, NULL));
    }
#endif

//    InitBackEnd1();

    wpMain.width = -1; // prevent popup sizes window
    LoadStart();

    /* check for GTK events and process them */
    while(1) {
	gtk_main_iteration();
    }

    return 0;
}

void
LoadBoard(char *name)
{
//    int boardWidth, w, h; //, boardHeight;

    InitBoard(name);

    optList = BoardPopUp(squareSize, lineGap, (void*)
#ifdef TODO_GTK
#if ENABLE_NLS
						&clockFontSet);
#else
						clockFontStruct);
#endif
#else
0);
#endif
    InitDrawingHandle(optList + W_BOARD);
    shellWidget      = shells[BoardDlg];
    currBoard        = &optList[W_BOARD];
    boardWidget      = optList[W_BOARD].handle;
    menuBarWidget    = optList[W_MENU].handle;

    InitMenuMarkers();

    // add accelerators to main shell
    gtk_window_add_accel_group(GTK_WINDOW(shellWidget), GtkAccelerators);

#if 0
    /*
     * Create an icon. (Use two icons, to indicate whther it is white's or black's turn.)
     */
    WhiteIcon  = gdk_pixbuf_new_from_file(SVGDIR "/icon_white.svg", NULL);
    BlackIcon  = gdk_pixbuf_new_from_file(SVGDIR "/icon_black.svg", NULL);
    mainwindowIcon = WhiteIcon;
    gtk_window_set_icon(GTK_WINDOW(shellWidget), mainwindowIcon);
#endif

#if 0
    /*
     * Inhibit shell resizing.
     */
    {
	// Note: We cannot do sensible sizing here, because the height of the clock widget is not yet known
	// It wil only become known asynchronously, when we first write a string into it.
	// This will then change the clock widget height, which triggers resizing the top-level window
	// and a configure event. Only then can we know the total height of the top-level window,
	// and calculate the height we need. The clockKludge flag suppresses all resizing until
	// that moment comes, after which the configure event-handler handles it through a (delayed) DragProg.
	int hc;
	GtkAllocation a;
	gtk_widget_get_allocation(shells[BoardDlg], &a);
	w = a.width; h = a.height;
	gtk_widget_get_allocation(optList[W_WHITE].handle, &a);
	clockKludge = hc = a.height;
	gtk_widget_get_allocation(boardWidget, &a);
	marginW =  w - boardWidth; // [HGM] needed to set new shellWidget size when we resize board
	marginH =  h - a.height - hc; // subtract current clock height, so it can be added back dynamically
    }
#endif

//    CreateAnyPieces(); FIXME: next 3
//    CreateGrid();

//    CreateAnimVars();

    g_signal_connect(shells[BoardDlg], "key-press-event", G_CALLBACK(KeyPressProc), NULL);
    g_signal_connect(shells[BoardDlg], "configure-event", G_CALLBACK(EventProc), NULL);

//    InitBackEnd2();

//    InitDrawingSizes();
//    InitPosition(TRUE);
}

RETSIGTYPE
IntSigHandler (int sig)
{
    return;
//    ExitEvent(sig);
}

#define Abs(n) ((n)<0 ? -(n) : (n))

void
EnableNamedMenuItem (char *menuRef, int state)
{
    MenuItem *item = MenuNameToItem(menuRef);
    if(item && item->handle) gtk_widget_set_sensitive(item->handle, state);
}

void
SetMenuEnables (Enables *enab)
{
  while (enab->name != NULL) {
    EnableNamedMenuItem(enab->name, enab->value);
    enab++;
  }
}

gboolean KeyPressProc(window, eventkey, data)
     GtkWindow *window;
     GdkEventKey  *eventkey;
     gpointer data;
{

//    MoveTypeInProc(eventkey); // pop up for typed in moves

#ifdef TODO_GTK
    /* check for other key values */
    switch(eventkey->keyval) {
        case GDK_question:
	  AboutGameEvent();
	  break;
        default:
	  break;
    }
#endif
    return False;
}
#ifdef TODO_GTK
void
KeyBindingProc (Widget w, XEvent *event, String *prms, Cardinal *nprms)
{   // [HGM] new method of key binding: specify MenuItem(FlipView) in stead of FlipViewProc in translation string
    MenuItem *item;
    if(*nprms == 0) return;
    item = MenuNameToItem(prms[0]);
    if(item) ((MenuProc *) item->proc) ();
}
#endif

/* Pause for `ms' milliseconds */
/* !! Ugh, this is a kludge. Fix it sometime. --tpm */
void
TimeDelay (long ms)
{
    TimeMark m1, m2;

    GetTimeMark(&m1);
    do {
	GetTimeMark(&m2);
    } while (SubtractTimeMarks(&m2, &m1) < ms);
}

static void
do_flash_delay (unsigned long msec)
{
    TimeDelay(msec);
}

void
FlashDelay (int flash_delay)
{
	if(flash_delay) do_flash_delay(flash_delay);
}

double
Fraction (int x, int start, int stop)
{
   double f = ((double) x - start)/(stop - start);
   if(f > 1.) f = 1.; else if(f < 0.) f = 0.;
   return f;
}

static WindowPlacement wpNew;

void
CoDrag (GtkWidget *sh, WindowPlacement *wp)
{
    int touch=0, fudge = 2, f = 2;
    GetActualPlacement(sh, wp);
    if(abs(wpMain.x + wpMain.width + 2*frameX - f - wp->x)         < fudge) touch = 1; else // right touch
    if(abs(wp->x + wp->width + 2*frameX + f - wpMain.x)            < fudge) touch = 2; else // left touch
    if(abs(wpMain.y + wpMain.height + frameX - f + frameY - wp->y) < fudge) touch = 3; else // bottom touch
    if(abs(wp->y + wp->height + frameX + frameY + f - wpMain.y)    < fudge) touch = 4;      // top touch
//printf("CoDrag: touch = %d x=%d w=%d x2=%d w2=%d fx=%d\n", touch, wpMain.x, wpMain.width, wp->x, wp->width, frameX);
    if(!touch ) return; // only windows that touch co-move
    if(touch < 3 && wpNew.height != wpMain.height) { // left or right and height changed
	int heightInc = wpNew.height - wpMain.height;
	double fracTop = Fraction(wp->y, wpMain.y, wpMain.y + wpMain.height + frameX + frameY);
	double fracBot = Fraction(wp->y + wp->height + frameX + frameY + 1, wpMain.y, wpMain.y + wpMain.height + frameX + frameY);
	wp->y += fracTop * heightInc;
	heightInc = (int) (fracBot * heightInc) - (int) (fracTop * heightInc);
#ifdef TODO_GTK
	if(heightInc) XtSetArg(args[j], XtNheight, wp->height + heightInc), j++;
#endif
	wp->height += heightInc;
    } else if(touch > 2 && wpNew.width != wpMain.width) { // top or bottom and width changed
	int widthInc = wpNew.width - wpMain.width;
	double fracLeft = Fraction(wp->x, wpMain.x, wpMain.x + wpMain.width + 2*frameX);
	double fracRght = Fraction(wp->x + wp->width + 2*frameX + 1, wpMain.x, wpMain.x + wpMain.width + 2*frameX);
	wp->y += fracLeft * widthInc;
	widthInc = (int) (fracRght * widthInc) - (int) (fracLeft * widthInc);
#ifdef TODO_GTK
	if(widthInc) XtSetArg(args[j], XtNwidth, wp->width + widthInc), j++;
#endif
	wp->width += widthInc;
    }
    wp->x += wpNew.x - wpMain.x;
    wp->y += wpNew.y - wpMain.y;
    if(touch == 1) wp->x += wpNew.width - wpMain.width; else
    if(touch == 3) wp->y += wpNew.height - wpMain.height;
#ifdef TODO_GTK
    XtSetArg(args[j], XtNx, wp->x); j++;
    XtSetArg(args[j], XtNy, wp->y); j++;
    XtSetValues(sh, args, j);
#endif
	gtk_window_move(GTK_WINDOW(sh), wp->x, wp->y);
//printf("moved to (%d,%d)\n", wp->x, wp->y);
	gtk_window_resize(GTK_WINDOW(sh), wp->width, wp->height);
}

void
ReSize (WindowPlacement *wp)
{
#if 0
	GtkAllocation a;
	int sqx, sqy, w, h, hc, lg = lineGap;
	gtk_widget_get_allocation(optList[W_WHITE].handle, &a);
	hc = a.height; // clock height can depend on single / double line clock text!
        if(clockKludge && hc != clockKludge) wp->height += hc - clockKludge, clockKludge = 0;
	wpMain.height = BOARD_HEIGHT * (squareSize + lineGap) + lineGap + marginH + hc;
	if(wp->width == wpMain.width && wp->height == wpMain.height) return; // not sized
	sqx = (wp->width  - lg - marginW) / BOARD_WIDTH - lg;
	sqy = (wp->height - lg - marginH - hc) / BOARD_HEIGHT - lg;
	if(sqy < sqx) sqx = sqy;
        if(sqx < 20) return;
	if(appData.overrideLineGap < 0) { // do second iteration with adjusted lineGap
	    lg = lineGap = sqx < 37 ? 1 : sqx < 59 ? 2 : sqx < 116 ? 3 : 4;
	    sqx = (wp->width  - lg - marginW) / BOARD_WIDTH - lg;
	    sqy = (wp->height - lg - marginH - hc) / BOARD_HEIGHT - lg;
	    if(sqy < sqx) sqx = sqy;
	}
	if(sqx != squareSize) {
	    squareSize = sqx; // adopt new square size
	    CreatePNGPieces(); // make newly scaled pieces
	    InitDrawingSizes(0, 0); // creates grid etc.
	} else ResizeBoardWindow(BOARD_WIDTH * (squareSize + lineGap) + lineGap, BOARD_HEIGHT * (squareSize + lineGap) + lineGap, 0);
	w = BOARD_WIDTH * (squareSize + lineGap) + lineGap;
	h = BOARD_HEIGHT * (squareSize + lineGap) + lineGap;
	if(optList[W_BOARD].max   > w) optList[W_BOARD].max = w;
	if(optList[W_BOARD].value > h) optList[W_BOARD].value = h;
#endif
}

static guint delayedDragTag = 0;

void
DragProc ()
{
	static int busy;
	if(busy) return;

	busy = 1;
	GetActualPlacement(shellWidget, &wpNew);
	if(wpNew.x == wpMain.x && wpNew.y == wpMain.y && // not moved
	   wpNew.width == wpMain.width && wpNew.height == wpMain.height) { // not sized
	    busy = 0; return; // false alarm
	}
	ReSize(&wpNew);
//	    if(shellUp[EngOutDlg]) CoDrag(shells[EngOutDlg], &wpEngineOutput);
	wpMain = wpNew;
	DrawBoard();
	if(delayedDragTag) g_source_remove(delayedDragTag);
	delayedDragTag = 0; // now drag executed, make sure next DelayedDrag will not cancel timer event (which could now be used by other)
	busy = 0;
}

void
DelayedDrag ()
{
//printf("old timr = %d\n", delayedDragTag);
    if(delayedDragTag) g_source_remove(delayedDragTag);
    delayedDragTag = g_timeout_add( 200, (GSourceFunc) DragProc, NULL);
//printf("new timr = %d\n", delayedDragTag);
}

static gboolean
EventProc (GtkWidget *widget, GdkEvent *event, gpointer g)
{
//printf("event proc (%d,%d) %dx%d\n", event->configure.x, event->configure.y, event->configure.width, event->configure.height);
    // immediately
    wpNew.x = event->configure.x;
    wpNew.y = event->configure.y;
    wpNew.width  = event->configure.width;
    wpNew.height = event->configure.height;
    DelayedDrag(); // as long as events keep coming in faster than 50 msec, they destroy each other
    return FALSE;
}



/* Disable all user input other than deleting the window */
static int frozen = 0;

void
FreezeUI ()
{
  if (frozen) return;
  /* Grab by a widget that doesn't accept input */
  gtk_grab_add(optList[W_PIECE].handle);
  frozen = 1;
}

/* Undo a FreezeUI */
void
ThawUI ()
{
  if (!frozen) return;
  gtk_grab_remove(optList[W_PIECE].handle);
  frozen = 0;
}


/*
 * Button/menu procedures
 */

void CopyFileToClipboard(gchar *filename)
{
    gchar *selection_tmp;
    GtkClipboard *cb;

    // read the file
    FILE* f = fopen(filename, "r");
    long len;
    size_t count;
    if (f == NULL) return;
    fseek(f, 0, 2);
    len = ftell(f);
    rewind(f);
    selection_tmp = g_try_malloc(len + 1);
    if (selection_tmp == NULL) {
        printf("Malloc failed in CopyFileToClipboard\n");
        return;
    }
    count = fread(selection_tmp, 1, len, f);
    fclose(f);
    if (len != count) {
      g_free(selection_tmp);
      return;
    }
    selection_tmp[len] = '\0'; // file is now in selection_tmp

    // copy selection_tmp to clipboard
    GdkDisplay *gdisp = gdk_display_get_default();
    if (!gdisp) {
        g_free(selection_tmp);
        return;
    }
    cb = gtk_clipboard_get_for_display(gdisp, GDK_SELECTION_CLIPBOARD);
    gtk_clipboard_set_text(cb, selection_tmp, -1);
    g_free(selection_tmp);
}

void
CopySomething (char *src)
{
    GdkDisplay *gdisp = gdk_display_get_default();
    GtkClipboard *cb;
    if(!src) { CopyFileToClipboard(gameCopyFilename); return; }
    if (gdisp == NULL) return;
    cb = gtk_clipboard_get_for_display(gdisp, GDK_SELECTION_CLIPBOARD);
    gtk_clipboard_set_text(cb, src, -1);
}


void
SetWindowTitle (DialogClass dlg, char *title, char *icon)
{
    gtk_window_set_title (GTK_WINDOW(shells[dlg]), title);
}


char *
TimeString (long ms)
{
    long second, minute, hour, day;
    char *sign = "";
    static char buf[32];

    if (ms > 0 && ms <= 9900) {
      /* convert milliseconds to tenths, rounding up */
      double tenths = floor( ((double)(ms + 99L)) / 100.00 );

      snprintf(buf,sizeof(buf)/sizeof(buf[0]), " %03.1f ", tenths/10.0);
      return buf;
    }

    /* convert milliseconds to seconds, rounding up */
    /* use floating point to avoid strangeness of integer division
       with negative dividends on many machines */
    second = (long) floor(((double) (ms + 999L)) / 1000.0);

    if (second < 0) {
	sign = "-";
	second = -second;
    }

    day = second / (60 * 60 * 24);
    second = second % (60 * 60 * 24);
    hour = second / (60 * 60);
    second = second % (60 * 60);
    minute = second / 60;
    second = second % 60;

    if (day > 0)
      snprintf(buf, sizeof(buf)/sizeof(buf[0]), " %s%ld:%02ld:%02ld:%02ld ",
	      sign, day, hour, minute, second);
    else if (hour > 0)
      snprintf(buf, sizeof(buf)/sizeof(buf[0]), " %s%ld:%02ld:%02ld ", sign, hour, minute, second);
    else
      snprintf(buf, sizeof(buf)/sizeof(buf[0]), " %s%2ld:%02ld ", sign, minute, second);

    return buf;
}

void
SetColoredLabel (Option *opt, char *timer, char *fgcolor, char *bgcolor, char *clockFont)
{
    GtkWidget *w = (GtkWidget *) opt->handle;
    GdkColor col;
    char *markup;

    gdk_color_parse( bgcolor, &col );
    gtk_widget_modify_bg(gtk_widget_get_parent(opt->handle), GTK_STATE_NORMAL, &col);

    if (1) { // clock mode
        markup = g_markup_printf_escaped("<span font=\"%s\" background=\"%s\" foreground=\"%s\">  %s</span>", clockFont,
					 bgcolor, fgcolor, timer);
//        markup = g_markup_printf_escaped("<span size=\"xx-large\" weight=\"heavy\" background=\"%s\" foreground=\"%s\">%s:%s%s</span>",
//					 bgcolor, fgcolor, color, appData.logoSize && !partnerUp ? "\n" : " ", TimeString(timer));
    }

    gtk_label_set_markup(GTK_LABEL(w), markup);
    g_free(markup);
}

static GdkPixbuf **clockIcons[] = { &WhiteIcon, &BlackIcon };

void
SetClockIcon (int color)
{
    GdkPixbuf *pm = *clockIcons[color];
    if (mainwindowIcon != pm) {
        mainwindowIcon = pm;
	gtk_window_set_icon(GTK_WINDOW(shellWidget), mainwindowIcon);
    }
}

#define INPUT_SOURCE_BUF_SIZE 8192

#ifndef HAVE_USLEEP

static Boolean frameWaiting;

static RETSIGTYPE
FrameAlarm (int sig)
{
  frameWaiting = False;
  /* In case System-V style signals.  Needed?? */
  signal(SIGALRM, FrameAlarm);
}

void
FrameDelay (int time)
{
  struct itimerval delay;

  if (time > 0) {
    frameWaiting = True;
    signal(SIGALRM, FrameAlarm);
    delay.it_interval.tv_sec =
      delay.it_value.tv_sec = time / 1000;
    delay.it_interval.tv_usec =
      delay.it_value.tv_usec = (time % 1000) * 1000;
    setitimer(ITIMER_REAL, &delay, NULL);
    while (frameWaiting) pause();
    delay.it_interval.tv_sec = delay.it_value.tv_sec = 0;
    delay.it_interval.tv_usec = delay.it_value.tv_usec = 0;
    setitimer(ITIMER_REAL, &delay, NULL);
  }
}

#else

void
FrameDelay (int time)
{
//  gtk_main_iteration_do(False);

  if (time > 0)
    usleep(time * 1000);
}

#endif

int
FileNamePopUpWrapper (label, def, filter, proc, pathFlag, openMode, name, fp)
     char *label;
     char *def;
     char *filter;
     void (*proc)();
     char *openMode;
     Boolean pathFlag;
     char *name;
     FILE **fp;
{
  GtkWidget     *dialog;
  GtkFileFilter *gtkfilter;
  GtkFileFilter *gtkfilter_all;
  char space[]     = " ";
  char fileext[10] = "";
  char *result     = NULL;
  char *cp;
  int res = 0;

  /* make a copy of the filter string, so that strtok can work with it*/
  cp = strdup(filter);

  /* add filters for file extensions */
  gtkfilter     = gtk_file_filter_new();
  gtkfilter_all = gtk_file_filter_new();

  /* one filter to show everything */
  gtk_file_filter_add_pattern(gtkfilter_all, "*.*");
  gtk_file_filter_set_name   (gtkfilter_all, "All Files");

  /* add filter if present */
  result = strtok(cp, space);
  while( result != NULL  ) {
    snprintf(fileext,10,"*%s",result);
    result = strtok( NULL, space );
    gtk_file_filter_add_pattern(gtkfilter, fileext);
  };

  /* second filter to only show what's useful */
  gtk_file_filter_set_name (gtkfilter,filter);

  if (openMode[0] == 'r')
    {
      dialog = gtk_file_chooser_dialog_new (label,
					    NULL,
					    GTK_FILE_CHOOSER_ACTION_OPEN,
					    GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					    GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
					    NULL);
    }
  else
    {
      dialog = gtk_file_chooser_dialog_new (label,
					    NULL,
					    GTK_FILE_CHOOSER_ACTION_SAVE,
					    GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					    GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
					    NULL);
      /* add filename suggestions */
      if (strlen(def) > 0 )
	gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), def);

      //gtk_file_chooser_set_create_folders(GTK_FILE_CHOOSER (dialog),TRUE);
    }

  /* add filters */
  gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(dialog),gtkfilter_all);
  gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(dialog),gtkfilter);
  /* activate filter */
  gtk_file_chooser_set_filter (GTK_FILE_CHOOSER(dialog),gtkfilter);

  if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT && name)
    {
      char *filename;

	filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));

      //see loadgamepopup
	strcpy(name, filename);
	g_free (filename);
	res = 1;
    };

  gtk_widget_destroy (dialog);

  free(cp);
  return res;

}

void
DoEvents()
{
    while(gtk_events_pending()) {
	gtk_main_iteration();
    }
}

char *
CommonBox (char *msg, int n, char *title, char *def)
{
    GtkWidget *dialog, *box, *label, *shell = shells[BoardDlg];
    int resp, content = n & 15, resp1, resp2;
    static char buf[MSG_SIZ];
    char *textRep, *text1, *text2;

    switch(content) {
	case 1: text1 = GTK_STOCK_OK; text2 = GTK_STOCK_CANCEL; resp1 = 1; resp2 = 2; break;
	case 4: text1 = "Yes"; text2 = "No"; resp1 = 6; resp2 = 7; break;
	default: text1 = GTK_STOCK_OK; text2 = NULL; resp1 = 1; break;
    }

    if(shell == NULL) shell = shells[RootWindow];

    dialog = gtk_dialog_new_with_buttons( title,
					  GTK_WINDOW(shell),
					  GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_NO_SEPARATOR |
					  GTK_DIALOG_MODAL,
					  text1, resp1,
					  text2, resp2,
					  NULL );
    box = gtk_dialog_get_content_area( GTK_DIALOG( dialog ) );
    if(def) {
	label = gtk_entry_new();
	gtk_entry_set_text (GTK_ENTRY (label), def);
    } else label = gtk_label_new(msg);
    gtk_box_pack_start(GTK_BOX (box), label, TRUE, TRUE, 0);
    gtk_widget_show(label);
    resp = gtk_dialog_run( GTK_DIALOG(dialog) );
    if(def) {
	textRep = (char *) gtk_entry_get_text(GTK_ENTRY (label));
        strncpy(buf, textRep, MSG_SIZ);
//	g_free(textRep); // this makes it segfault!?
    }
    gtk_widget_destroy(dialog);
    return def ? buf : (char*) (intptr_t) resp;
}

int
MsgBox (char *msg, int n, char *title)
{
    return (int) (intptr_t) CommonBox(msg, n, title, NULL);
}

char *
InputBoxSTR (char *prompt, char *title, char *def)
{
    return CommonBox(prompt, 0, title, def);
}
