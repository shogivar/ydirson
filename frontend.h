
INT MsgBox(char *msg, int buttons, char *title);
void DoEvents();
char *InputBoxSTR(STRING prompt, STRING title, STRING def);
void *LoadPicture(STRING filename);
int StartTime();
void DisplayError(char *message, int error);
void UpdateCaptions(void);
void DrawBoard();
void InitBoard();

void LoadStart();
void UnloadStart();
void LoadAddPieces();
void UnloadAddPieces();
void LoadBoard();
void UnloadBoard();
void LoadPieceHelp();
void UnloadPieceHelp();

void MoveObject(int redraw, OBJECT *piece, int x, int y);
void DrawCircle(int x, int y, int r, int fg, int fill, int style);
void DrawLine(int x1, int y1, int x2, int y2, int color, char *options);
void StartClockTimer(long millisec);
void ScheduleDelayedEvent(DelayedEventCallback cb, long millisec);

void MarkMenuItem(char *menuRef, int mark);
void EnableNamedMenuItem(char *menuRef, int enable);
void TagsPopUp(char *text, char *title);
int FileNamePopUpWrapper(
     char *label,
     char *def,
     char *filter,
     void (*proc)(),
     Boolean pathFlag,
     char *openMode,
     char *name,
     FILE **fp
);
