/*
 * draw.c -- drawing routines for XBoard
 *
 * Copyright 1991 by Digital Equipment Corporation, Maynard,
 * Massachusetts.
 *
 * Enhancements Copyright 1992-2001, 2002, 2003, 2004, 2005, 2006,
 * 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014 Free Software Foundation, Inc.
 *
 * The following terms apply to Digital Equipment Corporation's copyright
 * interest in XBoard:
 * ------------------------------------------------------------------------
 * All Rights Reserved
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose and without fee is hereby granted,
 * provided that the above copyright notice appear in all copies and that
 * both that copyright notice and this permission notice appear in
 * supporting documentation, and that the name of Digital not be
 * used in advertising or publicity pertaining to distribution of the
 * software without specific, written prior permission.
 *
 * DIGITAL DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * DIGITAL BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 * ------------------------------------------------------------------------
 *
 * The following terms apply to the enhanced version of XBoard
 * distributed by the Free Software Foundation:
 * ------------------------------------------------------------------------
 *
 * GNU XBoard is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * GNU XBoard is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.  *
 *
 *------------------------------------------------------------------------
 ** See the file ChangeLog for a revision history.  */

#include "config.h"

#include <stdio.h>
#include <math.h>
#include <cairo/cairo.h>
#include <cairo/cairo-xlib.h>
#include <librsvg/rsvg.h>
#include <librsvg/rsvg-cairo.h>

#if STDC_HEADERS
# include <stdlib.h>
# include <string.h>
#else /* not STDC_HEADERS */
extern char *getenv();
# if HAVE_STRING_H
#  include <string.h>
# else /* not HAVE_STRING_H */
#  include <strings.h>
# endif /* not HAVE_STRING_H */
#endif /* not STDC_HEADERS */

#if ENABLE_NLS
#include <locale.h>
#endif

#include "common.h"

#include "backend.h"
#include "menus.h"
#include "dialogs.h"
#include "gettext.h"
#include "draw.h"


#ifdef __EMX__
#ifndef HAVE_USLEEP
#define HAVE_USLEEP
#endif
#define usleep(t)   _sleep2(((t)+500)/1000)
#endif

#ifdef ENABLE_NLS
# define  _(s) gettext (s)
# define N_(s) gettext_noop (s)
#else
# define  _(s) (s)
# define N_(s)  s
#endif

#define SOLID 0
#define OUTLINE 1
Boolean cairoAnimate;
Option *currBoard;
cairo_surface_t *csBoardWindow;
int useTexture, textureW[2], textureH[2];

char *crWhite = "#FFFFB0";
char *crBlack = "#AD5D3D";

void
ExposeRedraw (Option *graph, int x, int y, int w, int h)
{   // copy a selected part of the buffer bitmap to the display
    cairo_t *cr = cairo_create((cairo_surface_t *) graph->textValue);
    cairo_set_source_surface(cr, (cairo_surface_t *) graph->choice, 0, 0);
    cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
    cairo_rectangle(cr, x, y, w, h);
    cairo_fill(cr);
    cairo_destroy(cr);
}

// [HGM] seekgraph: some low-level drawing routines (by JC, mostly)

float
Color (char *col, int n)
{
  int c;
  sscanf(col, "#%x", &c);
  c = c >> 4*n & 255;
  return c/255.;
}

void
SetPen (cairo_t *cr, float w, char *col, int dash)
{
  static const double dotted[] = {4.0, 4.0};
  static int len  = sizeof(dotted) / sizeof(dotted[0]);
  cairo_set_line_width (cr, w);
  cairo_set_source_rgba (cr, Color(col, 4), Color(col, 2), Color(col, 0), 1.0);
  if(dash) cairo_set_dash (cr, dotted, len, 0.0);
}

void DrawSeekAxis( int x, int y, int xTo, int yTo )
{
    cairo_t *cr;

    /* get a cairo_t */
    cr = cairo_create (csBoardWindow);

    cairo_move_to (cr, x, y);
    cairo_line_to(cr, xTo, yTo );

    SetPen(cr, 2, "#000000", 0);
    cairo_stroke(cr);

    /* free memory */
    cairo_destroy (cr);
    GraphExpose(currBoard, x-1, yTo-1, xTo-x+2, y-yTo+2);
}

void DrawSeekBackground( int left, int top, int right, int bottom )
{
    cairo_t *cr = cairo_create (csBoardWindow);

    cairo_rectangle (cr, left, top, right-left, bottom-top);

    cairo_set_source_rgba(cr, 0.8, 0.8, 0.4,1.0);
    cairo_fill(cr);

    /* free memory */
    cairo_destroy (cr);
    GraphExpose(currBoard, left, top, right-left, bottom-top);
}

void
DrawSeekText (char *buf, int x, int y)
{
    cairo_t *cr = cairo_create (csBoardWindow);

    cairo_select_font_face (cr, "Sans",
			    CAIRO_FONT_SLANT_NORMAL,
			    CAIRO_FONT_WEIGHT_BOLD);

    cairo_set_font_size (cr, 11.0);

    cairo_move_to (cr, x, y+11);
    cairo_set_source_rgba(cr, 0, 0, 0,1.0);
    cairo_show_text( cr, buf);

    /* free memory */
    cairo_destroy (cr);
    GraphExpose(currBoard, x-5, y-3, 60, 15);
}

void DrawSeekDot(int x, int y, int colorNr)
{
    cairo_t *cr = cairo_create (csBoardWindow);
    int square = colorNr & 0x80;
    colorNr &= 0x7F;

    if(square)
	cairo_rectangle (cr, x-squareSize/9, y-squareSize/9, 2*(squareSize/9), 2*(squareSize/9));
    else
	cairo_arc(cr, x, y, squareSize/9, 0.0, 2*M_PI);

    SetPen(cr, 2, "#000000", 0);
    cairo_stroke_preserve(cr);
    switch (colorNr) {
      case 0: cairo_set_source_rgba(cr, 1.0, 0, 0,1.0);	break;
      case 1: cairo_set_source_rgba (cr, 0.0, 0.7, 0.2, 1.0); break;
      default: cairo_set_source_rgba (cr, 1.0, 1.0, 0.0, 1.0); break;
    }
    cairo_fill(cr);

    /* free memory */
    cairo_destroy (cr);
    GraphExpose(currBoard, x-squareSize/8, y-squareSize/8, 2*(squareSize/8), 2*(squareSize/8));
}

void
InitDrawingHandle (Option *opt)
{
    csBoardWindow = DRAWABLE(opt);
}

void
CopyRectangle (void *src, int srcX, int srcY, int width, int height, int destX, int destY)
{
	cairo_t *cr;
	cr = cairo_create (csBoardWindow);
	cairo_set_source_surface (cr, (cairo_surface_t*) src, destX - srcX, destY - srcY);
	cairo_rectangle (cr, destX, destY, width, height);
	cairo_fill (cr);
	cairo_destroy (cr);
}

void
DrawLogo (Option *opt, void *logo)
{
    cairo_surface_t *img;
    cairo_t *cr;
    int w, h;

    if(!logo || !opt) return;
    img = cairo_image_surface_create_from_png (logo);
    w = cairo_image_surface_get_width (img);
    h = cairo_image_surface_get_height (img);
    cr = cairo_create(DRAWABLE(opt));
//    cairo_scale(cr, (float)appData.logoSize/w, appData.logoSize/(2.*h));
    cairo_scale(cr, (float)opt->max/w, (float)opt->value/h);
    cairo_set_source_surface (cr, img, 0, 0);
    cairo_paint (cr);
    cairo_destroy (cr);
    cairo_surface_destroy (img);
    GraphExpose(opt, 0, 0, opt->max, opt->value);
}

void *
LoadPicture(char *src)
{
    cairo_surface_t *img;
    char *p;
    if((p = strstr(src, ".bmp"))) strcpy(p, ".png");
    img = cairo_image_surface_create_from_png (src); // try if there are png pieces there
    if(cairo_surface_status(img) != CAIRO_STATUS_SUCCESS) return NULL;
    return img;
}

void
LoadPieces (char *game)
{
    int i, ok = 1, skip = 0;
    char buf[MSG_SIZ], *suffix = "";
    if(!strcmp(game, "Sho")) suffix = "gi";
    if(!strcmp(game, "HShogi")) skip = 1;
    for(i=0; i<NPIECES; i++) Board.Pix[i].Visible = False;
    for(i=0; i<NPIECES; i++) {
	snprintf(buf, MSG_SIZ, DATADIR "/pieces/%s%s%d.png", game+skip, suffix, i+1);
	ok = ((Board.Pix[i].Picture = LoadPicture(buf)) != NULL);
	if(!ok && !strcmp(game, "Tai") ) { // some Tai pieces must be taken from Maka Dai Dai
 	    snprintf(buf, MSG_SIZ, DATADIR "/pieces/Maka%d.png", i < 115 ? i+1 : i - 36);
	    ok = ((Board.Pix[i].Picture = LoadPicture(buf)) != NULL);
	}
	if(!ok) break;
        Board.Pix[i].width = cairo_image_surface_get_width(Board.Pix[i].Picture);
        Board.Pix[i].height = cairo_image_surface_get_height(Board.Pix[i].Picture);
	Board.Pix[i].Visible = True;
    }
    dotSmall = LoadPicture(DATADIR "/titles/MakaDot.png");
    dotLarge = LoadPicture(DATADIR "/titles/MiniDot.png");
}

void
DrawCircle (int x, int y, int r, int fg, int bg, int fill)
{
    cairo_t *cr = cairo_create (csBoardWindow);

    cairo_set_line_width (cr, 2.0);
    cairo_set_source_rgba(cr, (fg&255)/255., (fg>>8&255)/255., (fg>>16&255)/255., 1.0);
    cairo_arc(cr, x, y, r, 0.0, 2*M_PI);

    if(fill) {
	cairo_stroke_preserve(cr);
	cairo_set_source_rgba(cr, (bg&255)/255., (bg>>8&255)/255., (bg>>16&255)/255., 1.0);
	cairo_fill(cr);
    } else {
	cairo_stroke(cr);
    }

    /* free memory */
    cairo_destroy (cr);
    GraphExpose(currBoard, x-r-2, y-r-2, 2*r+4, 2*r+4);
}

void
DrawLine (int x1, int y1, int x2, int y2, int color, char *options)
{
    cairo_t *cr = cairo_create (csBoardWindow);
    int fg = color; // black

    cairo_set_line_width (cr, 1.0);
    cairo_set_source_rgba(cr, (fg&255)/255., (fg>>8&255)/255., (fg>>16&255)/255., 1.0);

    if(strchr(options, 'B')) {
	int h;
	if(x1 > x2) h = x1, x1 = x2, x2 = h;
	if(y1 > y2) h = y1, y1 = y2, y2 = h;
	cairo_rectangle(cr, x1+0.5, y1+0.5, x2-x1, y2-y1);
	if(strchr(options, 'F')) {
	    cairo_stroke_preserve(cr);
	    cairo_fill(cr);
	} else cairo_stroke(cr);
    } else {
	int h;
	if(x1 == x2 && y1 > y2) h = y1, y1 = y2, y2 = h;
	cairo_move_to (cr, x1-0.5, y1);
	cairo_line_to(cr, x2-0.5, y2);
	cairo_stroke(cr);
    }


    /* free memory */
    cairo_destroy (cr);
    GraphExpose(currBoard, x1-1, y1-1, x2-x1+2, y2-y1+2);
}

