"                                                     INTRODUCTION
                                                     ==============


  It is thought that the original Japanese chess dates from about the 8th
  or 9th century, and was introduced either from China or from Burma and
  Thailand. 
    
  While there are references to the game dating from the 10th century, the
  earliest account of the rules are given in a history text entitled 
  NICHUREKI dated between 1126 and 1130. The description gives the moves
  of the pieces, says that all pieces promote to Gold General on reaching
  the 3rd rank, and that baring the opponents King wins the game. There is, 
  however, no mention of the number of pieces, size of the board, or the 
  initial set-up.

  Versions of the game have been reconstructed using either an 8x9 or 8x8 
  board (and it is believed possible that both versions may have existed).
  The 8x8 reconstruction has only one Gold General in the array, with the
  8x9 having an extra Gold on the back rank as in Modern Shogi.  The 8x9
  square version is the one that has been included in this program.

  There are no drops in Heian Shogi,and the game does not have Rooks or 
  Bishops. The pieces in Heian Shogi therefore have very limited powers of 
  movement and the game is much slower and less interesting than later 
  variants.  Heian Shogi is therefore primarily of historical interest only.	    


                                                         THE GAME
                                                         =========

  The reconstruction of Heian Shogi included in this program is played on a 
  board of 8 x 9 squares and each player has 18 pieces (including 9 pawns).

  The pieces are flat and wedge-shaped and are not distinguished by colour.
  Although the pieces are of uniform colour the first player is still
  conventionally referred to as 'Black' and the second player as 'White'.
  Ownership of the pieces is indicated by the direction in which they face,
  with a player's pieces always pointing towards the opponent.     
  
  The players make alternate moves, with the object being to capture the
  opposing 'King'.
 
  As in Western Chess, when a 'King' is about to be captured next move and
  no legal move can be made to prevent the capture, the piece is said to be
  'Checkmated'.

  In common with other ancient chess games, the game can also be won by 
  capturing all pieces except the 'King' (the 'bare king' rule). A bare 
  King may secure a draw if it can also bare the opposing 'King' on the
  following move. 
 
  On each turn a player can move one piece according to its power of 
  movement to a vacant square on the board, or to a square occupied by an
  enemy piece (in which case the enemy piece is captured and removed from
  the game).  


                                                            THE PIECES
                                                            ===========

  The Piece Help screen provides the names, notation symbols, promotion
  details, and powers of movement for all of the pieces in the game.

  [The Piece Help screen can be reached by clicking on the 'Pieces' button]


                                                          JUMPING PIECES
                                                          ===============

  The 'Knight' is the only piece in Heian Shogi that has the power to 
  jump over occupied squares. The Heian 'Knight' has the same move as
  the equivalent piece in the Western game (ie: it may move one square 
  orthogonally then one square diagonally), except that its move is limited
  to the forward direction only.
                                 
                                                            PROMOTION
                                                            ==========

  Each player has a Promotion Zone consisting of the three ranks (rows of
  squares) furthest away from him. All pieces except the 'King' can promote
  to 'Gold General' on entering, moving within, or leaving the Promotion
  Zone.

  Promotion is not compulsory unless the piece would be unable to make a
  further legal move in its unpromoted state. The 'Pawn' and 'Lance' must 
  therefore promote on reaching the last rank (that furthest from the
  player) and the 'Knight' must promote if it reaches the 2nd last rank. 
  There can be advantages with some pieces of not promoting immediately on
  entering the Promotion Zone.

  As in all the games in the Shogi family, the promoted rank is shown on the
  reverse side of the piece, and the piece is turned over on promotion to
  reveal the new rank.  

  
                                                           CAPTURES
                                                           =========

  Unlike in the modern game of Shogi, captured pieces in Heian Shogi can
  not be 'dropped' back into play. A captured piece is removed from play and
  takes no further part in the game.  

 
                                                        HANDICAP PLAY
                                                        ===============

  Handicaps are often given when players of unequal strength play Shogi in
  Japan. The reason that handicap play is common is that the handicap system
  in Shogi works far better than that used in Western Chess.

  In a handicap game a player offers a handicap of one or more pieces to an
  opponent of less strength. While Heian Shogi does not lend itself to 
  handicaps as well as Shogi, provision for handicap play has nevertheless
  been included in this program.

  The same rules for handicaps as in Shogi have been adopted.  Under these
  rules, the player offering the handicap plays 'White'and his opponent (as
  'Black') removes the handicap pieces as the first move of the game. In
  handicap play 'White' therefore makes the first move on the board.



                                                         NOTATION
                                                         =========

  The following notation system is used for recording Heian Shogi games in
  this program.

  The files are designated by numbers (1 to 9), and the ranks by letters
  (a to h). The files are numbered from right to left (in the Japanese
  fashion), and the ranks from top to bottom (from Black's point of view).
  The top right square is therefore 1a.

  A move is described by giving:

     a) the designation of the piece (eg: L for Lance); this designation is
        preceded by a '+' if it is at its promoted rank,

     b) followed by the method of moving; 
	
          '-' for a simple move on the board,
          'x' for a capture,
          
     c) then the destination square is recorded,

     d) finally if the piece promoted on that turn, this is recorded by
        adding '+' after the move, or if promotion was possible but was
        refused, the symbol '=' is added. 

  If more than one piece of the same type can reach the destination square
  then the starting square is also given after the piece designation to 
  avoid confusion.  

  A 'Lance' moving from 5d to 5c (which was occupied by an enemy piece) and 
  promoting is therefore recorded as Lx5c+.

  NOTE:   This system is the official notation system of 'The Shogi
          Association' for Shogi variants.


                                                  DISPLAYING LEGAL MOVES
                                                  =========================

  If the 'Show Move' option is selected from the Moves Menu, clicking the 
  left mouse button on a piece during play will show allthe legal moves of
  that piece:

   -  All legal moves to vacant squares are represented as White Circles,

   -  Legal captures are shown as Red Circles. "
