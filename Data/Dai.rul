"                                                     INTRODUCTION
                                                     ==============

  There are references to a game called Dai Shogi (Great Shogi) dating from
  the twelfth century, but these are thought to relate to an earlier game 
  with a 13x13 board (included in this program as Heian Dai Shogi). The
  earliest mention of Dai Shogi played on a 15x15 board are from the fifteenth
  century.
 
  While the pieces in Dai Shogi are for the most part the same as those in
  Chu Shogi, the game has a different feel as a result of the much larger 
  board. The power of the Lion is greatly diminished in Dai Shogi, and there
  are no special rules restricting the capture of the Lion.

  As with Chu and the other larger variants, there is no provision for 
  returning captured pieces into play in Dai Shogi. This makes Dai Shogi 
  more similar to Western Chess than modern Shogi.
 

                                                         THE GAME
                                                         =========

  Dai Shogi is played on a board of 15 x 15 squares and each player has 65
  pieces (including 15 pawns).

  As in all Shogi games, the pieces are flat and wedge-shaped and are not
  distinguished by colour. Although the pieces are of uniform colour the
  first player is still conventionally referred to as 'Black' and the second
  player as 'White'. Ownership of the pieces is indicated by the direction
  in which they face, with a player's pieces always pointing towards the
  opponent.     
  
  The players make alternate moves, with the object being to capture the
  opposing 'King'.  If the opposing player has obtained a 'Crown Prince' by
  promotion, that piece must also be captured in order to win the game.
 
  On each turn a player can move one piece according to its power of 
  movement to a vacant square on the board, or to a square occupied by an
  enemy piece (in which case the enemy piece is captured and removed from
  the game).

  In the case of the 'Lion' and pieces with 'Lion' power a second move can 
  sometimes be made in the same turn.
	

                                                            THE PIECES
                                                            ===========

  The Piece Help screen provides the names, notation symbols, promotion
  details, and powers of movement for all of the pieces in the game.

  [The Piece Help screen can be reached by clicking on the 'Pieces' button]


                                                          JUMPING PIECES
                                                          ===============

  The 'Kylin' and 'Phoenix' (and those pieces with 'Lion' powers) are the
  only pieces in Dai Shogi that have the power to jump over occupied squares.
  As indicated by red circles on the Piece Help screens, the 'Kylin' can jump
  to the second square in any orthogonal direction, and the 'Phoenix' may
  jump to the second square when moving diagonally.  The 'Flying Dragon' and
  'Violent Ox' can only move to the 2nd square in the directions indicated on
  the Piece Help screens (diagonally and orthogonally, respectively) if the
  intervening square is unoccupied (ie: they can not jump).  


                                                            THE LION
                                                            =========

  The 'Lion' has a very unusual and powerful move.

  If the 8 squares immediately adjacent to the 'Lion' are called the 'A'
  squares (shown as Dark Blue Circles on the Piece Help screen), and the 16
  squares two away from the piece are called the 'B' squares (represented as
  Light Blue Circles), then the 'Lion' may do anyone of the following things
  in a single turn:

    -  Move directly to any 'A' or 'B' square, jumping an intervening square
       if necessary;

    -  Capture a piece on an 'A' square and continue moving one more square
       in any direction from the point of capture, making another capture if 
       the 2nd square is also occupied by an enemy piece.

    -  Capture a piece on any 'A' square without moving (this is known as
       'igui' and counts as a turn).

    -  Move to an adjacent square and return to the starting square
       (effectively passing the turn). This move can be made by double-
       clicking the left mouse button on the Lion. 

  In Dai Shogi there are no restrictions on the capture of Lions (as there
  are in Chu Shogi).
   
  The 'Horned Falcon' and 'Soaring Eagle' also have 'Lion' power, but only 
  in the directions indicated by Blue Circles on the Piece Help Screens.  
  

                                                        THE CROWN PRINCE
                                                        ==================

  The 'Drunk Elephant' is potentially a very important piece, as it promotes
  to a 'Crown Prince'.  

  A player who gains a 'Crown Prince' effectively acquires a second 'King' 
  as the 'Crown Prince' must also be captured before the opponent can win the
  game.

                                 
                                                            PROMOTION
                                                            ==========

  Each player has a Promotion Zone consisting of the five ranks (rows of
  squares) furthest away from him. All pieces except the 'King', 'Lion'
  and 'Free King' have a promoted rank and can promote on entering, moving
  within, or leaving the Promotion Zone.

  Promotion is not compulsory, but a piece must promote if it would have no
  further legal moves in its unpromoted form (ie: a 'Pawn','Lance','Iron
  General','Stone General' or 'Knight' must promote on reaching the last
  rank).
  
  As in all the games in the Shogi family, in Dai Shogi sets the promoted
  rank is shown on the reverse side of the piece, and the piece is turned
  over on promotion to reveal the new rank.  

  
                                                           CAPTURES
                                                           =========

  Unlike in Shogi, captured pieces in Dai can not be 'dropped' back into
  play. A captured piece is removed from play and takes no further part in
  the game.  

 
                                                        HANDICAP PLAY
                                                        ===============

  Handicaps are often given when players of unequal strength play Shogi in
  Japan. The reason that handicap play is common is that the handicap system
  in Shogi works far better than that used in Western Chess.

  In a handicap game a player offers a handicap of one or more pieces to an
  opponent of less strength. While Dai does not lend itself to handicaps as
  well as Shogi (as there are no 'drops' in Dai), provision for handicap play
  has nevertheless been included in this program.

  The same rules for handicaps as in Shogi have been adopted. Under these
  rules, the player offering the handicap plays 'White'and his opponent (as
  'Black') removes the handicap pieces as the first move of the game. In
  handicap play 'White' therefore makes the first move on the board.


                                                         NOTATION
                                                         ==========

  The following notation system is used for recording Dai Shogi games in this
  program.

  The files are designated by numbers (1 to 15), and the ranks by letters
  (a to o). The files are numbered from right to left (in the Japanese
  fashion), and the ranks from top to bottom (from Black's point of view).
  The top right square is therefore 1a.

  A move is described by giving:

     a) the designation of the piece (eg: Ln for Lion); this designation is
        preceded by a '+' if it is at its promoted rank,

     b) followed by the method of moving; 
	
          '-' for a simple move on the board,
          'x' for a capture,
          
     c) then the destination square is recorded,

     d) finally if the piece promoted on that turn, this is recorded by
        adding '+' after the move, or if promotion was possible but was
        refused, the symbol '=' is added. 

  If more than one piece of the same type can reach the destination square
  then the starting square is also given after the piece designation to 
  avoid confusion.  

  When a 'Lion', 'Horned Falcon' or 'Soaring Eagle' captures by 'igui' 
  (ie: without moving) the square of the piece being captured is used
  instead of the destination square, and this is preceded by the symbol '!'.
  (eg: a Lion on 8c capturing a piece on 9d would be shown as Lnx!9d).

  When a piece makes a double capture with 'Lion' powers both captures are
  shown in the order that they were made. (eg: a Lion on 3g capturing a 
  piece on 3h and then capturing another on 2i, would be represented by
  Lnx3hx2i).        
  
  NOTE:   This system is the official notation system of 'The Shogi
          Association'.


                                                  DISPLAYING LEGAL MOVES
                                                  =========================

  If the 'Show Move' option is selected from the Moves Menu, clicking the left
  mouse button on a piece during play will show all the legal moves of that
  piece:

   -  Legal moves to vacant squares are represented as White Circles,

   -  Legal captures are shown as Red Circles. 

   -  Legal moves by a piece with 'Lion' powers to an 'A' square (including
      captures) are shown as a Dark Blue Circle.

   -  Legal moves by a piece with 'Lion' powers to a 'B' square (including
      captures) are indicated by a Light Blue Circle."
